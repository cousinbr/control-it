"""Zdlra module Admin"""
from django.contrib import admin

from zdlra.models import ZdlraProtectionPolicy, Zdlra, ZdlraDatabase, ZdlraDatabaseSpaceMetric, ZdlraSpaceMetric

admin.site.register(Zdlra)
admin.site.register(ZdlraSpaceMetric)
admin.site.register(ZdlraProtectionPolicy)
admin.site.register(ZdlraDatabase)
admin.site.register(ZdlraDatabaseSpaceMetric)
