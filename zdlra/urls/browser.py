"""Zdlra browser urls"""

from django.urls import path

from zdlra.views.browser import ZdlraDatabaseDetailView, ZdlraDatabaseExportView, ZdlraDatabaseListView, \
    ZdlraDetailView, ZdlraExportView, ZdlraListView, ZdlraProtectionPolicyDetailView, ZdlraProtectionPolicyExportView, \
    ZdlraProtectionPolicyListView

app_name = 'zdlra'
urlpatterns = [
    # Zdlra
    path('', ZdlraListView.as_view(), name='zdlra-list'),
    path('export/', ZdlraExportView.as_view(), name='zdlra-export'),
    path('<int:pk>/', ZdlraDetailView.as_view(), name='zdlra-detail'),
    # Protection policy
    path('protection-policies/', ZdlraProtectionPolicyListView.as_view(), name='zdlraprotectionpolicy-list'),
    path('protection-policies/export/', ZdlraProtectionPolicyExportView.as_view(), name='zdlraprotectionpolicy-export'),
    path('protection-policie/<int:pk>/', ZdlraProtectionPolicyDetailView.as_view(),
         name='zdlraprotectionpolicy-detail'),
    # ZDLRA databases
    path('databases/', ZdlraDatabaseListView.as_view(), name='zdlradatabase-list'),
    path('databases/export/', ZdlraDatabaseExportView.as_view(), name='zdlradatabase-export'),
    path('database/<int:pk>/', ZdlraDatabaseDetailView.as_view(), name='zdlradatabase-detail'),
]
