from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('core/', include('core.urls.api')),
    path('zdlra/', include('zdlra.urls.api')),
]
