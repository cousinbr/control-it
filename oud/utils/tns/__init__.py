"""Abstraction layer for OUD tns"""

import importlib

from controlit import settings
from oud.utils.tns.backend import TnsBackend


class TnsEngine:
    """OudTns engine class"""

    def __new__(cls):
        raise TypeError('Static classes cannot be instantiated')

    @staticmethod
    def get_engine(options) -> TnsBackend:
        """Return a secret engine"""

        oud_tns_engine = settings.OUD_TNS_ENGINE['backend']
        module_name, class_name = oud_tns_engine.rsplit(".", 1)
        engine_class = getattr(
            importlib.import_module(module_name), class_name)

        assert issubclass(engine_class, TnsBackend),\
               f"{settings.OUD_TNS_ENGINE['backend']} must derive from oud.utils.tns.backend.TnsBackend"

        # Return engine instance passing configured options
        return engine_class(options)
