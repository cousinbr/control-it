"""Class to manage TNS aliases in OUD using API REST..

Created on 2023.06.12

The goal is to support the creation and manipulation of TNSNAMEs entries.
There are 2 types of entry :
- The standard entries that carry the description, named service.
- Aliases that point to entries of type service.
"""
import json
import logging

import requests

logger = logging.getLogger(__name__)


class OudTnsApi():
    """Class to manage TNSNAME entries from OUD.

    On calling the class, a connection will be established with
    the OUD platform. Then the functions will use this connection
    to perform the expected operations.
    """

    def __init__(
        self, server: str, port: int, user: str, password: str, base_dn: str
    ) -> None:
        """init.

        Setup the URL and access.
        """
        self.server = server
        self.port = port
        self.user = user
        self.password = password
        self.base_dn = base_dn
        self.uri = f"https://{self.server}:{self.port}/rest/v1/directory/"
        self.session = requests.Session()
        self.session.verify = "/etc/pki/tls/certs/ca-bundle.crt"
        self.session.auth = (self.user, self.password)
        self.session.headers.update({"Content-type": "application/json"})

    def _resul2dict(self, ldapentries: dict) -> dict:
        """Convert the result to a neested dictionary adding the dn and list of attibutes.

        Args:
            ldapentries: result search.

        Returns:
            entries (dict):
                id (str): Entry TNSNAME.
                dn (str): DN entry from OUD.
                attributes (list): all entry attributs.
                tuple (attr name, value).
        """
        ldapentriesdict: dict = {}
        if ldapentries.get("searchResultEntries") is None:
            return ldapentriesdict
        for entry in ldapentries["searchResultEntries"]:
            entrydict: dict = {"dn": entry["dn"], "attributes": []}
            cn = entry["attributes"]["cn"]
            for key, value in entry["attributes"].items():
                entrydict["attributes"].append(key)
                entrydict[key] = value
            ldapentriesdict[cn] = entrydict
        return ldapentriesdict

    def _get(self, url: str, query: str) -> dict:
        """Retreive entries.

        Args:
            url (str): to specify entry.
            query (str): LDAP expression syntax.

        Returns:
            result (bool): True if ok else False.
        """
        result = {}
        try:
            request = self.session.get(url=url, params=query, timeout=5)
            request.raise_for_status()
        except requests.exceptions.HTTPError as error:
            logger.error(error)
            return result
        except requests.exceptions.ConnectionError as error:
            logger.error(error)
            raise RuntimeError("Connection Error") from error
        except requests.exceptions.Timeout as error:
            logger.error(error)
            raise RuntimeError("Timeout") from error
        except requests.exceptions.RequestException as error:
            logger.error(error)
            raise RuntimeError("Request error") from error
        result = request.json()
        return result

    def _post(self, data: str) -> bool:
        """Add an entry.

        Args:
            data (str): JSON description.

        Returns:
            result (bool): True if ok else False.
        """
        try:
            request = self.session.post(url=self.uri, data=data, timeout=5)
            request.raise_for_status()
        except requests.exceptions.HTTPError as error:
            logger.error(error)
            return False
        except requests.exceptions.ConnectionError as error:
            logger.error(error)
            raise RuntimeError("Connection Error") from error
        except requests.exceptions.Timeout as error:
            logger.error(error)
            raise RuntimeError("Timeout") from error
        except requests.exceptions.RequestException as error:
            logger.error(error)
            raise RuntimeError("Request error") from error
        logger.info("Added entry, data: %s", data)
        return True

    def _delete(self, distinguishedname: str) -> bool:
        """Remove an entry using the DN.

        Args:
            distinguishedname (str): wanted DN.

        Returns:
            result (bool): True if ok else False.
        """
        url = self.uri + distinguishedname
        try:
            request = self.session.delete(url=url, timeout=5)
            request.raise_for_status()
        except requests.exceptions.HTTPError as error:
            logger.error(error)
            return False
        except requests.exceptions.ConnectionError as error:
            logger.error(error)
            raise RuntimeError("Connection Error") from error
        except requests.exceptions.Timeout as error:
            logger.error(error)
            raise RuntimeError("Timeout") from error
        except requests.exceptions.RequestException as error:
            logger.error(error)
            raise RuntimeError("Request error") from error
        logger.info("Deleted entry DN: %s", distinguishedname)
        return True

    def _patch(self, distinguishedname: str, data: str) -> bool:
        """Modify an entry using the DN.

        Args:
            distinguishedname (str): wanted DN.
            data (str): JSON specification.

        Returns:
            result (bool): True if ok else False.
        """
        url = self.uri + distinguishedname
        try:
            request = self.session.patch(url=url, data=data, timeout=5)
            request.raise_for_status()
        except requests.exceptions.HTTPError as error:
            logger.error(error)
            return False
        except requests.exceptions.ConnectionError as error:
            logger.error(error)
            raise RuntimeError("Connection Error") from error
        except requests.exceptions.Timeout as error:
            logger.error(error)
            raise RuntimeError("Timeout") from error
        except requests.exceptions.RequestException as error:
            logger.error(error)
            raise RuntimeError("Request error") from error
        logger.info("Modify entry DN: %s", distinguishedname)
        return True

    def close(self) -> None:
        """Do nothing (backport)."""
        return True

    def getservices(self, tnsname: str = None) -> dict:
        """Get services matching a tnsname or all if set to None.

        Tne tnsname is use in search LDAP on cn attribut
        using LDAP expression syntax.
        Args:
            tnsname (str): wanted lettering.

        Returns:
            entries (dict): list of matching entries, empty if nothing foud.
        """
        data: dict = {}
        if tnsname is not None:
            query = {
                "scope": "one",
                "filter": "(&(objectclass=orclNetService)(cn=" + tnsname + "))",
                "attributes": "*",
            }
        else:
            query = {
                "scope": "one",
                "filter": "(objectclass=orclNetService)",
                "attributes": "*",
            }
        url = self.uri + self.base_dn
        result = self._get(url=url, query=query)
        data = self._resul2dict(ldapentries=result)
        return data

    def getservice(self, tnsname: str = None, distinguishedname: str = None) -> dict:
        """Get service using tnsname or DN.

        Search for a service by TNSNAME or DN.
        If TNSMANE, convert to DN in first.
        Args:
            tnsname (str): wanted TNSNAME.
            distinguishedname (str): wanted DN.

        Returns:
            entry (dict): entry found, empty if not.
        """
        data: dict = {}
        if tnsname is None and distinguishedname is None:
            return data
        if tnsname is not None and distinguishedname is not None:
            return data
        if tnsname is not None:
            distinguishedname = "cn=" + tnsname + "," + self.base_dn
        url = self.uri + distinguishedname
        query = {
            "scope": "base",
            "filter": "(objectclass=orclNetService)",
            "attributes": "*",
        }
        result = self._get(url=url, query=query)
        data = self._resul2dict(ldapentries=result)
        return data

    def getaliases(self, tnsname: str = None) -> dict:
        """Get aliases matching tnsname or all if set to None.

        Tne tnsname is use in search LDAP on cn attribut
        using LDAP expression syntax.
        Args:
            tnsname (str): wanted lettering.

        Returns:
            entries (dict): list of matching entries, empty if nothing foud.
        """
        data: dict = {}
        if tnsname is not None:
            query = {
                "scope": "one",
                "filter": "(&(objectclass=orclNetServiceAlias)(cn=" + tnsname + "))",
                "attributes": "*",
            }
        else:
            query = {
                "scope": "one",
                "filter": "(objectclass=orclNetServiceAlias)",
                "attributes": "*",
            }
        url = self.uri + self.base_dn
        result = self._get(url=url, query=query)
        data = self._resul2dict(ldapentries=result)
        return data

    def getalias(self, tnsname: str = None, distinguishedname: str = None) -> dict:
        """Get alias using tnsname or DN.

        Search for an alias by TNSNAME or DN.
        If TNSMANE, convert to DN in first.
        Args:
            tnsname (str): wanted TNSNAME.
            distinguishedname (str): wanted DN.

        Returns:
            entry (dict): entry found, empty if not.
        """
        data: dict = {}
        if tnsname is None and distinguishedname is None:
            return data
        if tnsname is not None and distinguishedname is not None:
            return data
        if tnsname is not None:
            distinguishedname = "cn=" + tnsname + "," + self.base_dn
        url = self.uri + distinguishedname
        query = {
            "scope": "base",
            "filter": "(objectclass=orclNetServiceAlias)",
            "attributes": "*",
        }
        result = self._get(url=url, query=query)
        data = self._resul2dict(ldapentries=result)
        return data

    def getservicealiases(self, tnsname: str) -> dict:
        """Get all aliases pointing to a given service.

        Generate the DN from the TNSNAME
        then do a search on the aliasedObjectName attribute.
        Args:
            tnsname (str): wanted TNSNAME.

        Returns:
            entries (dict): list of aliases, empty if nothing foud.
        """
        data: dict = {}
        service = self.getservice(tnsname=tnsname)
        if not service:
            return data
        url = self.uri + self.base_dn
        query = {
            "scope": "one",
            "filter": "(aliasedObjectName=" + service[tnsname]["dn"] + ")",
            "attributes": "*",
        }
        result = self._get(url=url, query=query)
        data = self._resul2dict(ldapentries=result)
        return data

    def getdefunctaliases(self) -> dict:
        """Gat all orphan aliases (ie pointing anywhere).

        Args:
            None.

        Returns:
            entries (dict): list of aliases, empty if nothing foud.
        """
        defunctaliases: dict = {}
        allaliases = self.getaliases()
        for tns, alias in allaliases.items():
            servicednused = alias["aliasedObjectName"]
            service = self.getservice(distinguishedname=servicednused)
            if not service:
                defunctaliases[tns] = alias
        return defunctaliases

    def addservice(
        self, tnsname: str, descstring: str, description: str = None
    ) -> bool:
        """Add a new service entry (ie TNS entry).

        Generate the DN from the tnsname of the service
        then create the entry.
        Args:
            tnsname (str): TNSNAME to be created.
            descstring (str): Definition of the TNSNAME.
            description (str): Comment if need.

        Returns:
            result (bool): True if ok else False.
        """
        tns: dict = {
            "msgType": "urn:ietf:params:rest:schemas:oracle:oud:1.0:AddRequest",
            "dn": "cn=" + tnsname + "," + self.base_dn,
            "attributes": {
                "cn": tnsname,
                "orclNetDescString": descstring,
                "description": description,
                "objectClass": ["top", "orclNetService"],
            },
        }
        tns = json.dumps(tns)
        result = self._post(data=tns)
        return result

    def modifyservice(
        self, tnsname: str, description: str = None, descstring: str = None
    ) -> bool:
        """Modify a service entry.

        Only attribute orclNetDescString and/or Description.
        First check is entry exist as service (ie not an alias).
        Args:
            tnsname (str): TNSNAME to be modified.
            descstring (str): Definition of the TNSNAME.
            description (str): Comment.

        Returns:
            result (bool): True if ok else False.
        """
        if description is None and descstring is None:
            logger.warning("Missing parameter description or descstring")
            return False
        service = self.getservice(tnsname=tnsname)
        if not service:
            logger.warning("Service %s does not exist", tnsname)
            return False
        if descstring is not None:
            tns: dict = {
                "msgType": "urn:ietf:params:rest:schemas:oracle:oud:1.0:ModifyRequest",
                "operations": [
                    {
                        "opType": "replace",
                        "attribute": "orclNetDescString",
                        "values": [descstring],
                    }
                ],
            }
            tns = json.dumps(tns)
            result = self._patch(distinguishedname=service[tnsname]["dn"], data=tns)
            if result is False:
                return False
        if description is not None:
            tns: dict = {
                "msgType": "urn:ietf:params:rest:schemas:oracle:oud:1.0:ModifyRequest",
                "operations": [
                    {
                        "opType": "replace",
                        "attribute": "description",
                        "values": [description],
                    }
                ],
            }
            tns = json.dumps(tns)
            result = self._patch(distinguishedname=service[tnsname]["dn"], data=tns)
        return result

    def removeservice(self, tnsname: str, removealiases: bool = False) -> bool:
        """Remove a service entry (ie TNS entry).

        Check if it's a service then remove.
        Args:
            tnsname (str): TNSNAME to be deleted.
            removealiases (bool): True to delete all aliases pointing
            to this service.

        Returns:
            result (bool): True if ok else False.
        """
        service = self.getservice(tnsname=tnsname)
        if not service:
            return False
        if removealiases:
            logger.info("Remove aliases requested")
            aliases = self.getservicealiases(tnsname=tnsname)
            for dummy, alias in aliases.items():
                result = self._delete(distinguishedname=alias["dn"])
                if not result:
                    return False
        result = self._delete(distinguishedname=service[tnsname]["dn"])
        return result

    def addalias(self, tnsname: str, aliasedservice: str) -> bool:
        """Add a new alias on a specified service.

        First check if the service entry exists.
        Args:
            tnsname (str): TNSNAME to be created.
            aliasedservice (str): TNSNAME of the service to alias.

        Returns:
            result (bool): True if ok else False.
        """
        service = self.getservice(tnsname=aliasedservice)
        if not service:
            logger.error("Service %s not found", aliasedservice)
            return False
        tns: dict = {
            "msgType": "urn:ietf:params:rest:schemas:oracle:oud:1.0:AddRequest",
            "dn": "cn=" + tnsname + "," + self.base_dn,
            "attributes": {
                "cn": tnsname,
                "aliasedObjectName": service[aliasedservice]["dn"],
                "objectClass": ["top", "alias", "orclNetServiceAlias"],
            },
        }
        tns = json.dumps(tns)
        result = self._post(data=tns)
        return result

    def removealias(self, tnsname: str) -> bool:
        """Remove an alias entry.

        Check if it's a alias then remove.
        Args:
            tnsname (str): TNSNAME to be deleted.

        Returns:
            result (bool): True if ok else False.
        """
        alias = self.getalias(tnsname=tnsname)
        if not alias:
            return False
        result = self._delete(distinguishedname=alias[tnsname]["dn"])
        return result
