"""Abstract Class to manage TNS aliases in OUD.

Created on 2023.06.12

The goal is to support the creation and manipulation of TNSNAMEs entries.
There are 2 types of entry :
- The standard entries that carry the description, named service.
- Aliases that point to entries of type service.
"""
import abc


class TnsBackend(metaclass=abc.ABCMeta):
    """Class to manage TNSNAME entries from OUD.
    """
    @classmethod
    def __subclasshook__(cls, subclass):
        return (
            hasattr(subclass, 'close') and callable(subclass.close) and
            hasattr(subclass, 'getservices') and callable(subclass.getservices) and
            hasattr(subclass, 'getservice') and callable(subclass.getservice) and
            hasattr(subclass, 'getaliases') and callable(subclass.getaliases) and
            hasattr(subclass, 'getalias') and callable(subclass.getalias) and
            hasattr(subclass, 'getservicealiases') and callable(subclass.getservicealiases) and
            hasattr(subclass, 'getdefunctaliases') and callable(subclass.getdefunctaliases) and
            hasattr(subclass, 'addservice') and callable(subclass.addservice) and
            hasattr(subclass, 'modifyservice') and callable(subclass.modifyservice) and
            hasattr(subclass, 'removeservice') and callable(subclass.removeservice) and
            hasattr(subclass, 'addalias') and callable(subclass.addalias) and
            hasattr(subclass, 'removealias') and callable(subclass.removealias)
        )

class DummyTnsBackend():
    """Dummy TnsBackend class"""

    def __init__(self, options):
        """Init"""
        self.options = options

    def close(self):
        """Dummy method"""

    def getservices(self):
        """Dummy method"""

    def getservice(self):
        """Dummy method"""

    def getaliases(self):
        """Dummy method"""

    def getalias(self):
        """Dummy method"""

    def getservicealiases(self):
        """Dummy method"""

    def getdefunctaliases(self):
        """Dummy method"""

    def addservice(self):
        """Dummy method"""

    def modifyservice(self):
        """Dummy method"""

    def removeservice(self):
        """Dummy method"""

    def addalias(self):
        """Dummy method"""

    def removealias(self):
        """Dummy method"""
