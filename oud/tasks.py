"""Oud app tasks"""

import logging
import re

from celery import shared_task

from controlit import settings
from core.utils.secret import SecretEngine
from database.models.tns import (
    FailoverNetworkAlias,
    FailoverService,
    StandaloneNetworkAlias,
    Service,
)
from oud.utils.tns import TnsEngine

logger = logging.getLogger(__name__)


@shared_task
def sync_oud_tns():
    """Sync OUD TNS"""

    def dict_compare(dict_1, dict_2):
        dict_1_keys = set(dict_1.keys())
        dict_2_keys = set(dict_2.keys())
        shared_keys = dict_1_keys.intersection(dict_2_keys)
        added = dict_1_keys - dict_2_keys
        removed = dict_2_keys - dict_1_keys
        modified = {
            o: (dict_1[o], dict_2[o]) for o in shared_keys if dict_1[o] != dict_2[o]
        }
        same = set(o for o in shared_keys if dict_1[o] == dict_2[o])
        return added, removed, modified, same

    #############################################################
    # Retreive password for OUD user
    #############################################################
    secret_engine = SecretEngine.get_engine(settings.OUD_PASSWORD_OPTIONS)
    oud_password = secret_engine.get_secret(
        key=settings.OUD_TNS_ENGINE["options"]["user"],
        options=settings.OUD_PASSWORD_OPTIONS,
    )

    #############################################################
    # Connect to OUD
    #############################################################
    logger.info("Retreive data from OUD")
    oud_engine_options = settings.OUD_TNS_ENGINE["options"]
    oud_engine_options["password"] = oud_password
    oud = TnsEngine.get_engine(oud_engine_options)
    result = oud.getservices()

    data_oud = {}

    for dummy, value in result.items():
        data_oud[value["cn"]] = value["orclNetDescString"]

    # Retreive data from inventory
    #############################################################
    logger.info("Retreive data from inventory")
    data_inventory = {}

    # Iter over database service
    for service in Service.objects.all().exclude(name__contains="$"):
        data_inventory[service.tns_name] = service.tns_string

    # Iter over database failover service
    for service in FailoverService.objects.exclude(name__contains="$"):
        data_inventory[service.tns_name] = service.tns_string

    added, removed, modified, same = dict_compare(data_inventory, data_oud)

    logger.info(
        "Added : %d, Removed : %d, Modified : %d, Same : %d",
        len(added),
        len(removed),
        len(modified),
        len(same),
    )

    # Add new entry
    logger.info("Add new entry to OUD")
    for entry in added:
        tnsname = entry
        descstring = data_inventory[entry]
        oud.addservice(tnsname=tnsname, descstring=descstring)

    # Remove deleted entry
    logger.info("Remove deleted entry from OUD")
    for entry in removed:
        tnsname = entry
        oud.removeservice(tnsname=tnsname, removealiases=True)

    # Update modified entry
    logger.info("Update modified entry from OUD")
    for entry in modified:
        tnsname = entry
        descstring = data_inventory[entry]
        oud.modifyservice(tnsname=tnsname, descstring=descstring)

    # Alias cleanup
    logger.info("Cleanup orphan alias in OUD")
    defunct_alias = oud.getdefunctaliases()
    for entry in defunct_alias:
        oud.removealias(tnsname=entry)

    #############################################################
    # Sync tns alias
    #############################################################
    logger.info("Sync tns aliases")

    # Retreive aliases from oud
    #############################################################
    logger.info("Retreive aliases from oud")
    result = oud.getaliases()

    data_oud = {}

    for dummy, value in result.items():
        alias_object_cn = re.findall(
            r"^cn=([a-zA-Z0-9_\-]+)", value["aliasedObjectName"]
        )
        data_oud[value["cn"]] = alias_object_cn[0]

    # Retreive data from inventory
    #############################################################
    logger.info("Retreive data from inventory")
    data_inventory = {}

    # Iter over aliases
    for alias in StandaloneNetworkAlias.objects.exclude(name__contains="$"):
        data_inventory[alias.name] = alias.service.tns_name

    # Iter over failover aliases
    for alias in FailoverNetworkAlias.objects.exclude(name__contains="$"):
        data_inventory[alias.name] = alias.service.tns_name

    added, removed, modified, same = dict_compare(data_inventory, data_oud)

    logger.info(
        "Added : %d, Removed : %d, Modified : %d, Same : %d",
        len(added),
        len(removed),
        len(modified),
        len(same),
    )

    # Add new entry
    logger.info("Add new alias entry to OUD")
    for entry in added:
        tnsname = entry
        aliasedservice = data_inventory[entry]
        oud.addalias(tnsname=tnsname, aliasedservice=aliasedservice)

    # Remove deleted entry
    logger.info("Remove deleted alias entry from OUD")
    for entry in removed:
        tnsname = entry
        oud.removealias(tnsname=tnsname)

    # Update modified entry
    logger.info("Update modified alias entry from OUD")
    for entry in modified:
        tnsname = entry
        aliasedservice = data_inventory[entry]
        oud.removealias(tnsname=tnsname)
        oud.addalias(tnsname=tnsname, aliasedservice=aliasedservice)
