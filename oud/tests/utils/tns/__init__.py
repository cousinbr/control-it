"""Test oud.utils.tns module"""

from django.test import TestCase

from oud.utils.tns import TnsEngine
from oud.utils.tns.backend import TnsBackend


class TnsEngineTestCase(TestCase):
    """Test TnsEngine"""

    def test_create_object(self):
        """Test object initialisation"""

        with self.assertRaises(TypeError):
            TnsEngine()

    def test_get_engine(self):
        """Test get_engine"""

        engine = TnsEngine.get_engine(options={})

        self.assertIsInstance(engine, TnsBackend)
