"""Module OUDTestCase to test OudTnsApi class."""
import os
from unittest.mock import patch

import requests
import responses
from django import setup
from django.test import TestCase

from oud.utils.tns.backend.api import OudTnsApi

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "controlit.settings")
setup()


class OUDTestCase(TestCase):
    """Test the management of TNSNAMEs.

    Tests using Responses library.
    https://github.com/getsentry/responses
    """

    server: str = "server"
    port: int = 8080
    url: str = "https://" + server + ":" + str(port) + "/rest/v1/directory/"
    user: str = "cn=admin"
    password: str = "adminpassword"
    base_dn: str = "cn=OracleContext,dc=domain"
    descstring: str = "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=server)(PORT=port))(CONNECT_DATA=(SERVICE_NAME=service)))"

    return_empty = {
        "msgType": "urn:ietf:params:rest:schemas:oracle:oud:1.0:SearchResponse",
        "totalResults": 0,
        "controls": [None],
    }
    return_service = {
        "msgType": "urn:ietf:params:rest:schemas:oracle:oud:1.0:SearchResponse",
        "totalResults": 1,
        "searchResultEntries": [
            {
                "dn": "cn=TEST001,cn=OracleContext,dc=domain",
                "attributes": {
                    "orclNetDescString": "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=server1)(PORT=port1))(CONNECT_DATA=(SERVICE_NAME=service1)))",
                    "cn": "TEST001",
                    "objectClass": ["top", "orclNetService"],
                },
            }
        ],
        "controls": [None],
    }
    return_newservice = {
        "msgType": "urn:ietf:params:rest:schemas:oracle:oud:1.0:SearchResponse",
        "totalResults": 1,
        "searchResultEntries": [
            {
                "dn": "cn=TEST010,cn=OracleContext,dc=domain",
                "attributes": {
                    "orclNetDescString": "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=server10)(PORT=port10))(CONNECT_DATA=(SERVICE_NAME=service10)))",
                    "cn": "TEST010",
                    "objectClass": ["top", "orclNetService"],
                },
            }
        ],
        "controls": [None],
    }
    return_listservices = {
        "msgType": "urn:ietf:params:rest:schemas:oracle:oud:1.0:SearchResponse",
        "totalResults": 3,
        "searchResultEntries": [
            {
                "dn": "cn=TEST001,cn=OracleContext,dc=domain",
                "attributes": {
                    "orclNetDescString": "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=server1)(PORT=port1))(CONNECT_DATA=(SERVICE_NAME=service1)))",
                    "cn": "TEST001",
                    "objectClass": ["top", "orclNetService"],
                },
            },
            {
                "dn": "cn=TEST002,cn=OracleContext,dc=domain",
                "attributes": {
                    "orclNetDescString": "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=server2)(PORT=port2))(CONNECT_DATA=(SERVICE_NAME=service2)))",
                    "cn": "TEST002",
                    "objectClass": ["top", "orclNetService"],
                },
            },
            {
                "dn": "cn=TEST003,cn=OracleContext,dc=domain",
                "attributes": {
                    "orclNetDescString": "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=server3)(PORT=port13)(CONNECT_DATA=(SERVICE_NAME=service3)))",
                    "cn": "TEST003",
                    "objectClass": ["top", "orclNetService"],
                },
            },
        ],
        "controls": [None],
    }
    return_alias = {
        "msgType": "urn:ietf:params:rest:schemas:oracle:oud:1.0:SearchResponse",
        "totalResults": 1,
        "searchResultEntries": [
            {
                "dn": "cn=AL001,cn=OracleContext,dc=domain",
                "attributes": {
                    "aliasedObjectName": "cn=TEST001,cn=OracleContext,dc=domain",
                    "cn": "AL001",
                    "objectClass": ["alias", "top", "orclNetServiceAlias"],
                },
            }
        ],
        "controls": [None],
    }
    return_badalias = {
        "msgType": "urn:ietf:params:rest:schemas:oracle:oud:1.0:SearchResponse",
        "totalResults": 2,
        "searchResultEntries": [
            {
                "dn": "cn=AL007,cn=OracleContext,dc=domain",
                "attributes": {
                    "aliasedObjectName": "cn=TEST007,cn=OracleContext,dc=domain",
                    "cn": "AL007",
                    "objectClass": ["alias", "top", "orclNetServiceAlias"],
                },
            },
            {
                "dn": "cn=AL001,cn=OracleContext,dc=domain",
                "attributes": {
                    "aliasedObjectName": "cn=TEST001,cn=OracleContext,dc=domain",
                    "cn": "AL001",
                    "objectClass": ["alias", "top", "orclNetServiceAlias"],
                },
            },
        ],
        "controls": [None],
    }
    return_newalias = {
        "msgType": "urn:ietf:params:rest:schemas:oracle:oud:1.0:SearchResponse",
        "totalResults": 1,
        "searchResultEntries": [
            {
                "dn": "cn=AL011,cn=OracleContext,dc=domain",
                "attributes": {
                    "aliasedObjectName": "cn=TEST001,cn=OracleContext,dc=domain",
                    "cn": "AL011",
                    "objectClass": ["alias", "top", "orclNetServiceAlias"],
                },
            }
        ],
        "controls": [None],
    }
    return_listaliases = {
        "msgType": "urn:ietf:params:rest:schemas:oracle:oud:1.0:SearchResponse",
        "totalResults": 3,
        "searchResultEntries": [
            {
                "dn": "cn=AL001,cn=OracleContext,dc=domain",
                "attributes": {
                    "aliasedObjectName": "cn=TEST001,cn=OracleContext,dc=domain",
                    "cn": "AL001",
                    "objectClass": ["alias", "top", "orclNetServiceAlias"],
                },
            },
            {
                "dn": "cn=AL002,cn=OracleContext,dc=domain",
                "attributes": {
                    "aliasedObjectName": "cn=TEST002,cn=OracleContext,dc=domain",
                    "cn": "AL002",
                    "objectClass": ["alias", "top", "orclNetServiceAlias"],
                },
            },
            {
                "dn": "cn=AL003,cn=OracleContext,dc=domain",
                "attributes": {
                    "aliasedObjectName": "cn=TEST003,cn=OracleContext,dc=domain",
                    "cn": "AL003",
                    "objectClass": ["alias", "top", "orclNetServiceAlias"],
                },
            },
        ],
        "controls": [None],
    }

    def test_close(self):
        """Close connection."""
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = myoud.close()
        self.assertEqual(result, True)

    @responses.activate
    def test_get_services(self):
        """Query services using LDAP expression syntax."""
        responses.add(
            responses.GET,
            url=self.url + self.base_dn,
            json=self.return_listservices,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservices(tnsname="TEST00*")
        resultkeys = list(result.keys())
        resultkeys.sort()
        listentries = ["TEST001", "TEST002", "TEST003"]
        self.assertEqual(resultkeys, listentries)

    @responses.activate
    def test_get_all_services(self):
        """Request all services."""
        responses.add(
            responses.GET,
            url=self.url + self.base_dn,
            json=self.return_listservices,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservices()
        resultkeys = list(result.keys())
        resultkeys.sort()
        listentries = ["TEST001", "TEST002", "TEST003"]
        self.assertEqual(resultkeys, listentries)

    @responses.activate
    def test_get_service(self):
        """Get a service by its TNSNAME."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservice(tnsname="TEST001")
        self.assertEqual(result["TEST001"]["cn"], "TEST001")

    @responses.activate
    def test_get_service_return_none(self):
        """Query a service no answer."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST010," + self.base_dn,
            json=self.return_empty,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservice(tnsname="TEST010")
        resultkeys = list(result.keys())
        self.assertEqual(resultkeys, [])

    @responses.activate
    def test_get_service_errors(self):
        """Query a service with errors."""
        for error in [400, 401, 402, 403, 404, 405, 500, 501, 502, 503, 504, 505]:
            responses.add(
                responses.GET,
                url=self.url + "cn=TEST001," + self.base_dn,
                json={"message": "error message"},
                status=error,
                content_type="text/plain",
            )
            myoud = OudTnsApi(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            result = myoud.getservice(tnsname="TEST001")
            resultkeys = list(result.keys())
            self.assertEqual(resultkeys, [])

    @patch("oud.utils.tns.backend.api.requests.Session.get")
    def test_get_service_exceptions(self, mock_get):
        """Query a service with server issues."""
        for raiserror in [
            requests.exceptions.ConnectionError,
            requests.exceptions.Timeout,
            requests.exceptions.RequestException,
        ]:
            mock_get.side_effect = raiserror
            myoud = OudTnsApi(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            with self.assertRaises(RuntimeError):
                myoud.getservice(tnsname="TEST001")

    def test_get_service_error_param1(self):
        """Try to get a service with the TNSNAME and DN parameters defined."""
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        tnsname = "TEST001"
        dn = "cn=TEST001,cn=OracleContext,dc=domain"
        result = myoud.getservice(tnsname=tnsname, distinguishedname=dn)
        self.assertEqual(result, {})

    def test_get_service_error_param2(self):
        """Try to get a service without parameter."""
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservice()
        self.assertEqual(result, {})

    @responses.activate
    def test_get_aliases(self):
        """Query aliases using LDAP expression syntax."""
        responses.add(
            responses.GET,
            url=self.url + self.base_dn,
            json=self.return_listaliases,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getaliases(tnsname="AL00*")
        resultkeys = list(result.keys())
        resultkeys.sort()
        listentries = ["AL001", "AL002", "AL003"]
        self.assertEqual(resultkeys, listentries)

    @responses.activate
    def test_get_all_aliases(self):
        """Request all aliases."""
        responses.add(
            responses.GET,
            url=self.url + self.base_dn,
            json=self.return_listaliases,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getaliases()
        resultkeys = list(result.keys())
        resultkeys.sort()
        listentries = ["AL001", "AL002", "AL003"]
        self.assertEqual(resultkeys, listentries)

    @responses.activate
    def test_get_alias(self):
        """Get an alias by its TNSNAME."""
        responses.add(
            responses.GET,
            url=self.url + "cn=AL001," + self.base_dn,
            json=self.return_alias,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getalias(tnsname="AL001")
        self.assertEqual(result["AL001"]["cn"], "AL001")

    @responses.activate
    def test_get_alias_by_dn(self):
        """Get an alias by its TNSNAME."""
        responses.add(
            responses.GET,
            url=self.url + "cn=AL001," + self.base_dn,
            json=self.return_alias,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getalias(distinguishedname="cn=AL001,cn=OracleContext,dc=domain")
        self.assertEqual(result["AL001"]["cn"], "AL001")

    def test_get_alias_error_param1(self):
        """Try to get an alias with the TNSNAME and DN parameters defined."""
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        tnsname = "AL001"
        dn = "cn=AL001,cn=OracleContext,dc=domain"
        result = myoud.getalias(tnsname=tnsname, distinguishedname=dn)
        self.assertEqual(result, {})

    def test_get_alias_error_param2(self):
        """Try to get an alias without parameter."""
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getalias()
        self.assertEqual(result, {})

    @responses.activate
    def test_get_aliases_of_service(self):
        """Request all aliases of a service."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        responses.add(
            responses.GET,
            url=self.url + self.base_dn,
            json=self.return_alias,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservicealiases(tnsname="TEST001")
        resultkeys = list(result.keys())
        resultkeys.sort()
        listentries = ["AL001"]
        self.assertEqual(resultkeys, listentries)

    @responses.activate
    def test_get_aliases_of_service_notfound(self):
        """Request all aliases of a service that does not exist."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST007," + self.base_dn,
            json=self.return_empty,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservicealiases(tnsname="TEST007")
        self.assertEqual(result, {})

    @responses.activate
    def test_get_defunct_aliases(self):
        """Get all aliases no longer pointing anyhere."""
        responses.add(
            responses.GET,
            url=self.url + self.base_dn,
            json=self.return_badalias,
            status=200,
            content_type="text/plain",
        )
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST007," + self.base_dn,
            json=self.return_empty,
            status=200,
            content_type="text/plain",
        )
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getdefunctaliases()
        resultkeys = list(result.keys())
        resultkeys.sort()
        listentries = ["AL007"]
        self.assertEqual(resultkeys, listentries)

    @responses.activate
    def test_create_service(self):
        """Create a new entry of type service."""
        responses.add(
            responses.POST,
            url=self.url,
            json=self.return_newservice,
            status=201,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST010"
        descstring = self.descstring
        description = "Service for business"
        result = myoud.addservice(
            tnsname=tnsname, descstring=descstring, description=description
        )
        self.assertEqual(result, True)

    @responses.activate
    def test_create_service_errors(self):
        """Error when creating a new entry of type service."""
        for error in [400, 401, 402, 403, 404, 405, 500, 501, 502, 503, 504, 505]:
            responses.add(
                responses.POST,
                url=self.url,
                json={"message": "error message"},
                status=error,
                content_type="text/plain",
            )
            myoud = OudTnsApi(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            tnsname = "TEST010"
            descstring = self.descstring
            description = "Service for business"
            result = myoud.addservice(
                tnsname=tnsname, descstring=descstring, description=description
            )
            self.assertEqual(result, False)

    @patch("oud.utils.tns.backend.api.requests.Session.post")
    def test_create_service_exceptions(self, mock_post):
        """Creating an entry with server exceptions."""
        for raiserror in [
            requests.exceptions.ConnectionError,
            requests.exceptions.Timeout,
            requests.exceptions.RequestException,
        ]:
            mock_post.side_effect = raiserror
            myoud = OudTnsApi(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            tnsname = "TEST010"
            descstring = self.descstring
            description = "Service for business"
            with self.assertRaises(RuntimeError):
                myoud.addservice(
                    tnsname=tnsname, descstring=descstring, description=description
                )

    @responses.activate
    def test_modify_service_descstring(self):
        """Modify an entry of type service."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        responses.add(
            responses.PATCH,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST001"
        descstring = self.descstring
        result = myoud.modifyservice(tnsname=tnsname, descstring=descstring)
        self.assertEqual(result, True)

    @responses.activate
    def test_modify_service_description(self):
        """Modify an entry of type service."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        responses.add(
            responses.PATCH,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST001"
        description = "Service for business"
        result = myoud.modifyservice(tnsname=tnsname, description=description)
        self.assertEqual(result, True)

    @responses.activate
    def test_modify_service_errors(self):
        """Error when modifying an entry of type service."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        for error in [400, 401, 402, 403, 404, 405, 500, 501, 502, 503, 504, 505]:
            responses.add(
                responses.PATCH,
                url=self.url + "cn=TEST001," + self.base_dn,
                json={"message": "error message"},
                status=error,
                content_type="text/plain",
            )
            myoud = OudTnsApi(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            tnsname = "TEST001"
            descstring = self.descstring
            description = "Service for business"
            result = myoud.modifyservice(
                tnsname=tnsname, descstring=descstring, description=description
            )
            self.assertEqual(result, False)

    @responses.activate
    @patch("oud.utils.tns.backend.api.requests.Session.patch")
    def test_modify_service_execptions(self, mock_patch):
        """Modifying an entry with server exceptions."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        for raiserror in [
            requests.exceptions.ConnectionError,
            requests.exceptions.Timeout,
            requests.exceptions.RequestException,
        ]:
            mock_patch.side_effect = raiserror
            myoud = OudTnsApi(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            tnsname = "TEST001"
            descstring = self.descstring
            description = "Service for business"
            with self.assertRaises(RuntimeError):
                myoud.modifyservice(
                    tnsname=tnsname, descstring=descstring, description=description
                )

    @responses.activate
    def test_modify_service_not_exists(self):
        """Error modifying an entry that does not exist."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST010," + self.base_dn,
            json=self.return_empty,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST010"
        descstring = self.descstring
        description = "Service for business"
        result = myoud.modifyservice(
            tnsname=tnsname, descstring=descstring, description=description
        )
        self.assertEqual(result, False)

    def test_modify_service_without_param(self):
        """Missing parameters when modifying an entry."""
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST001"
        result = myoud.modifyservice(tnsname=tnsname)
        self.assertEqual(result, False)

    @responses.activate
    def test_remove_service(self):
        """Remove an entry of type service."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        responses.add(
            responses.DELETE,
            url=self.url + "cn=TEST001," + self.base_dn,
            json={"message": "No Content"},
            status=204,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST001"
        result = myoud.removeservice(tnsname=tnsname)
        self.assertEqual(result, True)

    @responses.activate
    def test_remove_service_errors(self):
        """Errors when removing an entry of type service."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        for error in [400, 401, 402, 403, 404, 405, 500, 501, 502, 503, 504, 505]:
            responses.add(
                responses.DELETE,
                url=self.url + "cn=TEST001," + self.base_dn,
                json={"message": "error message"},
                status=error,
                content_type="text/plain",
            )
            myoud = OudTnsApi(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            tnsname = "TEST001"
            result = myoud.removeservice(tnsname=tnsname)
            self.assertEqual(result, False)

    @responses.activate
    @patch("oud.utils.tns.backend.api.requests.Session.delete")
    def test_remove_service_exceptions(self, mock_delete):
        """Removing an entry with server exceptions."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        for raiserror in [
            requests.exceptions.ConnectionError,
            requests.exceptions.Timeout,
            requests.exceptions.RequestException,
        ]:
            mock_delete.side_effect = raiserror
            myoud = OudTnsApi(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            tnsname = "TEST001"
            with self.assertRaises(RuntimeError):
                myoud.removeservice(tnsname=tnsname)

    @responses.activate
    def test_remove_service_notexist(self):
        """Remove an entry that does not exist."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST010," + self.base_dn,
            json=self.return_empty,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST010"
        result = myoud.removeservice(tnsname=tnsname)
        self.assertEqual(result, False)

    @responses.activate
    def test_remove_service_and_alliases(self):
        """Remove an entry of type service with all its aliases."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        responses.add(
            responses.GET,
            url=self.url + self.base_dn,
            json=self.return_alias,
            status=200,
            content_type="text/plain",
        )
        responses.add(
            responses.DELETE,
            url=self.url + "cn=AL001," + self.base_dn,
            json={"message": "No Content"},
            status=204,
            content_type="text/plain",
        )
        responses.add(
            responses.DELETE,
            url=self.url + "cn=TEST001," + self.base_dn,
            json={"message": "No Content"},
            status=204,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST001"
        result = myoud.removeservice(tnsname=tnsname, removealiases=True)
        self.assertEqual(result, True)

    @responses.activate
    def test_remove_service_and_alliases_ko(self):
        """Error when deleting an alias of a servive entry."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        responses.add(
            responses.GET,
            url=self.url + self.base_dn,
            json=self.return_alias,
            status=200,
            content_type="text/plain",
        )
        responses.add(
            responses.DELETE,
            url=self.url + "cn=AL001," + self.base_dn,
            json={"message": "Error message"},
            status=401,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST001"
        result = myoud.removeservice(tnsname=tnsname, removealiases=True)
        self.assertEqual(result, False)

    @responses.activate
    def test_add_alias(self):
        """Create an entry of type alias."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST001," + self.base_dn,
            json=self.return_service,
            status=200,
            content_type="text/plain",
        )
        responses.add(
            responses.POST,
            url=self.url,
            json=self.return_newalias,
            status=201,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        service = "TEST001"
        tnsname = "AL011"
        result = myoud.addalias(tnsname=tnsname, aliasedservice=service)
        self.assertEqual(result, True)

    @responses.activate
    def test_add_alias_service_not_exist(self):
        """Try to add an alias on service that does not exist."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST010," + self.base_dn,
            json=self.return_empty,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        service = "TEST010"
        tnsname = "AL011"
        result = myoud.addalias(tnsname=tnsname, aliasedservice=service)
        self.assertEqual(result, False)

    @responses.activate
    def test_add_alias_errors(self):
        """Error when creating an entry of type alias."""
        responses.add(
            responses.GET,
            url=self.url + "cn=TEST010," + self.base_dn,
            json=self.return_empty,
            status=200,
            content_type="text/plain",
        )
        for error in [400, 401, 402, 403, 404, 405, 500, 501, 502, 503, 504, 505]:
            responses.add(
                responses.POST,
                url=self.url + self.base_dn,
                json={"message": "error message"},
                status=error,
                content_type="text/plain",
            )
            myoud = OudTnsApi(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            service = "TEST010"
            tnsname = "AL011"
            result = myoud.addalias(tnsname=tnsname, aliasedservice=service)
            self.assertEqual(result, False)

    @responses.activate
    def test_remove_alias(self):
        """Remove an entry of type alias."""
        responses.add(
            responses.GET,
            url=self.url + "cn=AL001," + self.base_dn,
            json=self.return_alias,
            status=200,
            content_type="text/plain",
        )
        responses.add(
            responses.DELETE,
            url=self.url + "cn=AL001," + self.base_dn,
            json={"message": "No Content"},
            status=204,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "AL001"
        result = myoud.removealias(tnsname=tnsname)
        self.assertEqual(result, True)

    @responses.activate
    def test_remove_alias_notexist(self):
        """Remove an alias that does not exist."""
        responses.add(
            responses.GET,
            url=self.url + "cn=AL001," + self.base_dn,
            json=self.return_empty,
            status=200,
            content_type="text/plain",
        )
        myoud = OudTnsApi(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "AL001"
        result = myoud.removealias(tnsname=tnsname)
        self.assertEqual(result, False)
