"""Module OUDTestCase to test OudTnsLdap class."""
import os
from unittest.mock import patch

from django import setup
from django.test import TestCase
from ldap3 import MOCK_SYNC, Connection, Server
from ldap3.core.exceptions import (
    LDAPBindError,
    LDAPCursorError,
    LDAPExceptionError,
    LDAPInvalidDnError,
    LDAPSocketOpenError,
)

from controlit import settings
from oud.utils.tns.backend.ldap import OudTnsLdap

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "controlit.settings")
setup()


class OUDTestCase(TestCase):
    """Test the management of TNSNAMEs.

    For the implementation of the tests we created a fake OUD
    server based on the Mocking of the LDAP3 library.
    Once connected to your OUD server it's possible to extract
    the data in JSON format to create your test server.

    First connect:
    server = Server(REAL_SERVER, get_info=ALL)
    connection = Connection(server, REAL_USER, REAL_PASSWORD, auto_bind=True)

    Get server info and schema :
    server.info.to_file('oud_server_info.json')
    server.schema.to_file('oud_schema.json')

    You can retrieve data using query:
    connection.search('ou=test,o=lab', '(objectclass=*)')
    connection.response_to_file('oud_entries.json', raw=True)
    Otherwise you can create your own dataset.
    """

    # Fake OUD server config
    server: str = "server"
    port: int = 389
    user: str = "cn=admin"
    password: str = "adminpassword"
    base_dn: str = "cn=OracleContext,dc=my_domain"
    descstring: str = "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=my_server)(PORT=my_port))(CONNECT_DATA=(SERVICE_NAME=my_service)))"

    def setUp(self):
        """Init the fake OUD server using your files."""
        self.fake_server = Server.from_definition(
            "my_fake_oud",
            os.path.join(
                settings.BASE_DIR, "oud", "tests", "utils", "oud_server_info.json"
            ),
            os.path.join(settings.BASE_DIR, "oud", "tests", "utils", "oud_schema.json"),
        )
        self.fake_connection = Connection(
            self.fake_server,
            user=self.user,
            password=self.password,
            client_strategy=MOCK_SYNC,
        )
        self.fake_connection.strategy.entries_from_json(
            os.path.join(settings.BASE_DIR, "oud", "tests", "utils", "oud_entries.json")
        )
        self.fake_connection.strategy.add_entry(
            self.user, {"userPassword": self.password, "sn": "ADMIN TNS"}
        )

    def tearDown(self):
        """Stop fake OUD server."""
        self.fake_connection.unbind()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_connect_ko(self, mock_server, mock_connection):
        """Raise an error when connecting."""
        mock_server.return_value = self.fake_server
        for raiserror in [LDAPSocketOpenError, LDAPBindError, LDAPExceptionError]:
            mock_connection.side_effect = raiserror
            with self.assertRaises(RuntimeError):
                OudTnsLdap(
                    server=self.server,
                    port=self.port,
                    user=self.user,
                    password=self.password,
                    base_dn=self.base_dn,
                )

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_close(self, mock_server, mock_connection):
        """Close connection."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = myoud.close()
        self.assertEqual(result, True)

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_services(self, mock_server, mock_connection):
        """Query services using LDAP expression syntax."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservices(tnsname="*TEST00*")
        resultkeys = list(result.keys())
        resultkeys.sort()
        listentries = ["TEST001", "TEST002", "TEST003", "TEST004", "TEST005"]
        self.assertEqual(resultkeys, listentries)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_all_services(self, mock_server, mock_connection):
        """Request all services."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservices()
        resultkeys = list(result.keys())
        resultkeys.sort()
        listentries = ["TEST001", "TEST002", "TEST003", "TEST004", "TEST005"]
        self.assertEqual(resultkeys, listentries)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_service(self, mock_server, mock_connection):
        """Get a service by its TNSNAME."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservice(tnsname="TEST001")
        self.assertEqual(result["TEST001"]["cn"], "TEST001")
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_service_error_param1(self, mock_server, mock_connection):
        """Try to get a service with the TNSNAME and DN parameters defined."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        tnsname = "TEST001"
        dn = "cn=TEST001,cn=OracleContext,dc=mydomain"
        result = myoud.getservice(tnsname=tnsname, distinguishedname=dn)
        self.assertEqual(result, {})
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_service_error_param2(self, mock_server, mock_connection):
        """Try to get a service without parameter."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservice()
        self.assertEqual(result, {})
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_aliases(self, mock_server, mock_connection):
        """Query aliases using LDAP expression syntax."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getaliases(tnsname="*AL03*")
        resultkeys = list(result.keys())
        resultkeys.sort()
        listentries = ["AL031", "AL032"]
        self.assertEqual(resultkeys, listentries)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_all_aliases(self, mock_server, mock_connection):
        """Request all aliases."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getaliases()
        resultkeys = list(result.keys())
        resultkeys.sort()
        listentries = [
            "AL011",
            "AL012",
            "AL021",
            "AL022",
            "AL031",
            "AL032",
            "AL061",
            "AL071",
        ]
        self.assertEqual(resultkeys, listentries)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_alias(self, mock_server, mock_connection):
        """Get an alias by its TNSNAME."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getalias(tnsname="AL021")
        self.assertEqual(result["AL021"]["cn"], "AL021")
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_alias_by_dn(self, mock_server, mock_connection):
        """Get an alias by its TNSNAME."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getalias(
            distinguishedname="cn=AL021,cn=OracleContext,dc=my_domain"
        )
        self.assertEqual(result["AL021"]["cn"], "AL021")
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_alias_error_param1(self, mock_server, mock_connection):
        """Try to get an alias with the TNSNAME and DN parameters defined."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        tnsname = "AL021"
        dn = "cn=AL021,cn=OracleContext,dc=mydomain"
        result = myoud.getalias(tnsname=tnsname, distinguishedname=dn)
        self.assertEqual(result, {})
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_alias_error_param2(self, mock_server, mock_connection):
        """Try to get an alias without parameter."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getalias()
        self.assertEqual(result, {})
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_aliases_of_service(self, mock_server, mock_connection):
        """Request all aliases of a service."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservicealiases(tnsname="TEST003")
        resultkeys = list(result.keys())
        resultkeys.sort()
        listentries = ["AL031", "AL032"]
        self.assertEqual(resultkeys, listentries)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_aliases_of_service_notfound(self, mock_server, mock_connection):
        """Request all aliases of a service that does not exist."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getservicealiases(tnsname="TEST007")
        self.assertEqual(result, {})
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_get_defunct_aliases(self, mock_server, mock_connection):
        """Get all aliases no longer pointing anyhere."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = {}
        result = myoud.getdefunctaliases()
        resultkeys = list(result.keys())
        resultkeys.sort()
        listentries = ["AL061", "AL071"]
        self.assertEqual(resultkeys, listentries)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Writer.commit")
    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_create_service_description(
        self, mock_server, mock_connection, moke_commit
    ):
        """Create a new entry of type service."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        moke_commit.return_value = True
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST100"
        descstring = self.descstring
        description = "Service for business"
        result = myoud.addservice(
            tnsname=tnsname, descstring=descstring, description=description
        )
        self.assertEqual(result, True)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Writer.commit")
    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_create_service(self, mock_server, mock_connection, moke_commit):
        """Create a new entry of type service."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        moke_commit.return_value = True
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST100"
        descstring = self.descstring
        result = myoud.addservice(tnsname=tnsname, descstring=descstring)
        self.assertEqual(result, True)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Writer.commit")
    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_create_service_ko(self, mock_server, mock_connection, moke_commit):
        """Error when creating a new entry of type service."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        moke_commit.return_value = False
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST100"
        descstring = self.descstring
        description = "Service for business"
        result = myoud.addservice(
            tnsname=tnsname, descstring=descstring, description=description
        )
        self.assertEqual(result, False)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Writer.new")
    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_create_service_already_exist(self, mock_server, mock_connection, moke_new):
        """Error when creating an entry that exists."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        moke_new.side_effect = LDAPCursorError
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST100"
        descstring = self.descstring
        description = "Service for business"
        result = myoud.addservice(
            tnsname=tnsname, descstring=descstring, description=description
        )
        self.assertEqual(result, False)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Writer.commit")
    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_modify_service_descstring(self, mock_server, mock_connection, moke_commit):
        """Modify an entry of type service."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        moke_commit.return_value = True
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST002"
        descstring = self.descstring
        result = myoud.modifyservice(tnsname=tnsname, descstring=descstring)
        self.assertEqual(result, True)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Writer.commit")
    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_modify_service_description(
        self, mock_server, mock_connection, moke_commit
    ):
        """Modify an entry of type service."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        moke_commit.return_value = True
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST002"
        description = "Service for business"
        result = myoud.modifyservice(tnsname=tnsname, description=description)
        self.assertEqual(result, True)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Writer.commit")
    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_modify_service_ko(self, mock_server, mock_connection, moke_commit):
        """Error modifying an entry of type service."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        moke_commit.return_value = False
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST002"
        descstring = self.descstring
        description = "Service for business"
        result = myoud.modifyservice(
            tnsname=tnsname, descstring=descstring, description=description
        )
        self.assertEqual(result, False)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_modify_service_not_exists(self, mock_server, mock_connection):
        """Error modifying an entry that does not exist."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST100"
        descstring = self.descstring
        description = "Service for business"
        result = myoud.modifyservice(
            tnsname=tnsname, descstring=descstring, description=description
        )
        self.assertEqual(result, False)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_modify_service_without_param(self, mock_server, mock_connection):
        """Missing parameters when modifying an entry."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        tnsname = "TEST001"
        result = myoud.modifyservice(tnsname=tnsname)
        self.assertEqual(result, False)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_remove_service(self, mock_server, mock_connection):
        """Remove an entry of type service."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        tnsname = "TEST001"
        with patch.object(Connection, "delete") as mock_delete:
            mock_delete.return_value = True
            myoud = OudTnsLdap(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            result = myoud.removeservice(tnsname=tnsname)
        self.assertEqual(result, True)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_remove_service_ko(self, mock_server, mock_connection):
        """Error removing an entry of type service."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        tnsname = "TEST001"
        with patch.object(Connection, "delete") as mock_delete:
            mock_delete.side_effect = LDAPInvalidDnError
            myoud = OudTnsLdap(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            result = myoud.removeservice(tnsname=tnsname)
        self.assertEqual(result, False)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_remove_service_notexist(self, mock_server, mock_connection):
        """Remove an entry that does not exist."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        tnsname = "TEST007"
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = myoud.removeservice(tnsname=tnsname)
        self.assertEqual(result, False)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_remove_service_and_alliases(self, mock_server, mock_connection):
        """Remove an entry of type service with all its aliases."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        tnsname = "TEST001"
        with patch.object(Connection, "delete") as mock_delete:
            mock_delete.return_value = True
            myoud = OudTnsLdap(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            result = myoud.removeservice(tnsname=tnsname, removealiases=True)
        self.assertEqual(result, True)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_remove_service_and_alliases_ko(self, mock_server, mock_connection):
        """Error deleting an alias of a servive entry."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        tnsname = "TEST001"
        with patch.object(Connection, "delete") as mock_delete:
            mock_delete.side_effect = LDAPInvalidDnError
            myoud = OudTnsLdap(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            result = myoud.removeservice(tnsname=tnsname, removealiases=True)
        self.assertEqual(result, False)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Writer.commit")
    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_add_alias(self, mock_server, mock_connection, moke_commit):
        """Create an entry of type alias."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        moke_commit.return_value = True
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        service = "TEST005"
        tnsname = "AL051"
        result = myoud.addalias(tnsname=tnsname, aliasedservice=service)
        self.assertEqual(result, True)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Writer.new")
    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_add_alias_allready_exists(self, mock_server, mock_connection, moke_new):
        """Try to add an entry that already exists."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        moke_new.side_effect = LDAPCursorError
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        service = "TEST005"
        tnsname = "AL051"
        result = myoud.addalias(tnsname=tnsname, aliasedservice=service)
        self.assertEqual(result, False)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_add_alias_service_not_exist(self, mock_server, mock_connection):
        """Try to add an alias on service that does not exist."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        service = "TEST100"
        tnsname = "AL101"
        result = myoud.addalias(tnsname=tnsname, aliasedservice=service)
        self.assertEqual(result, False)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Writer.commit")
    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_add_alias_ko(self, mock_server, mock_connection, moke_commit):
        """Error creating an entry of type alias."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        moke_commit.return_value = False
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        service = "TEST005"
        tnsname = "AL051"
        result = myoud.addalias(tnsname=tnsname, aliasedservice=service)
        self.assertEqual(result, False)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_remove_alias(self, mock_server, mock_connection):
        """Remove an entry of type alias."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        tnsname = "AL061"
        with patch.object(Connection, "delete") as mock_delete:
            mock_delete.return_value = True
            myoud = OudTnsLdap(
                server=self.server,
                port=self.port,
                user=self.user,
                password=self.password,
                base_dn=self.base_dn,
            )
            result = myoud.removealias(tnsname=tnsname)
        self.assertEqual(result, True)
        myoud.close()

    @patch("oud.utils.tns.backend.ldap.Connection")
    @patch("oud.utils.tns.backend.ldap.Server")
    def test_remove_alias_notexist(self, mock_server, mock_connection):
        """Remove an alias that does not exist."""
        mock_server.return_value = self.fake_server
        mock_connection.return_value = self.fake_connection
        tnsname = "AL100"
        myoud = OudTnsLdap(
            server=self.server,
            port=self.port,
            user=self.user,
            password=self.password,
            base_dn=self.base_dn,
        )
        result = myoud.removealias(tnsname=tnsname)
        self.assertEqual(result, False)
        myoud.close()
