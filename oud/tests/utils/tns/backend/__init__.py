"""Test TnsBackend"""

from unittest.mock import patch
from unittest import TestCase

from oud.utils.tns.backend import TnsBackend



class FalseTnsBackend():
    """False TnsBackend"""

class TnsBackendTestCase(TestCase):
    """Test TnsBackend abstract class"""

    def test_class(self):
        """Test issubclass"""
        result = issubclass(FalseTnsBackend, TnsBackend)
        self.assertFalse(result)
