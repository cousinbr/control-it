"""Oud app tasks Test"""

# from unittest.mock import patch

# from django.test import TestCase

# from database.tests.models import ModelBuilder
# from oud.tasks import sync_oud_tns


# class SyncOudTnsTestCase(TestCase):
#     """Load facts function test"""

#     def setUp(self):
#         """Setup test"""
#         self.model_builder = ModelBuilder()

#     @patch("oud.tasks.SecretEngine")
#     @patch("oud.utils.tns.TnsEngine")
#     def test_sync_oud_tns(self, mock_oudtns, unused_mock_secretengine):
#         """Sync OUD data"""

#         mocked_oudtns = mock_oudtns.return_value

#         mocked_oudtns.getservices.return_value = {
#             "DBUNIQUENAME-myservice-1": {
#                 "dn": "cn=DBUNIQUENAME-myservice-1,cn=OracleContext,dc=tns,dc=oracle,dc=domain,dc=fr",
#                 "attributes": [
#                     "objectClass",
#                     "cn",
#                     "orclNetDescName",
#                     "orclNetDescString",
#                     "orclVersion",
#                     "Description",
#                 ],
#                 "objectClass": ["top", "orclNetService"],
#                 "cn": "DBUNIQUENAME-myservice-1",
#                 "orclNetDescName": None,
#                 "orclNetDescString": "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=hostname)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=myservice)))",
#                 "orclVersion": None,
#                 "Description": None,
#             },
#             "DBNAME-myservice-1": {
#                 "dn": "cn=DBNAME-myservice-1,cn=OracleContext,dc=tns,dc=oracle,dc=domain,dc=fr",
#                 "attributes": [
#                     "objectClass",
#                     "cn",
#                     "orclNetDescName",
#                     "orclNetDescString",
#                     "orclVersion",
#                     "Description",
#                 ],
#                 "objectClass": ["top", "orclNetService"],
#                 "cn": "DBNAME-myservice-1",
#                 "orclNetDescName": None,
#                 "orclNetDescString": "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=hostname)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=myservice)))",
#                 "orclVersion": None,
#                 "Description": None,
#             },
#         }
#         mocked_oudtns.getaliases.return_value = {
#             "ALIAS_FAILOVER": {
#                 "dn": "cn=ALIAS_FAILOVER,cn=OracleContext,dc=tns,dc=oracle,dc=domain,dc=fr",
#                 "attributes": ["objectClass", "aliasedObjectName", "cn"],
#                 "objectClass": ["alias", "top", "orclNetServiceAlias"],
#                 "aliasedObjectName": "cn=DBNAME-myservice-1,cn=OracleContext,dc=tns,dc=oracle,dc=domain,dc=fr",
#                 "cn": "ALIAS_FAILOVER",
#             },
#             "ALIAS": {
#                 "dn": "cn=ALIAS,cn=OracleContext,dc=tns,dc=oracle,dc=domain,dc=fr",
#                 "attributes": ["objectClass", "aliasedObjectName", "cn"],
#                 "objectClass": ["alias", "top", "orclNetServiceAlias"],
#                 "aliasedObjectName": "cn=DBUNIQUENAME-myservice-1,cn=OracleContext,dc=tns,dc=oracle,dc=domain,dc=fr",
#                 "cn": "ALIAS",
#             },
#         }

#         sync_oud_tns()
#         self.assertEqual(1, 1)
