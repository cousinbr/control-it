"""Search app class_name template tag"""
from django import template

register = template.Library()


@register.filter()
def class_name(value):
    """Return object class name"""

    return value.__class__.__name__
