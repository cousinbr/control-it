"""Search app views"""
from itertools import chain
from django.views.generic import ListView
from database.models.database import Database
from database.models.instance import Instance
from database.models.tablespace import Tablespace
from database.models.tns import FailoverNetworkAlias, FailoverService, Service, StandaloneNetworkAlias

from system.models import Cluster, Server


class SearchView(ListView):
    """Search view"""

    template_name = 'search/views.html'
    count = 0

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['count'] = self.count or 0
        context['query'] = self.request.GET.get('q')
        return context

    def get_queryset(self):
        request = self.request
        query = request.GET.get('q', None).strip()

        if query is not None:
            cluster_results = Cluster.objects.search(query)
            server_results = Server.objects.search(query)
            instance_results = Instance.objects.search(query)
            database_results = Database.objects.search(query)
            tablespace_results = Tablespace.objects.search(query)
            service_results = Service.objects.search(query)
            failover_service_results = FailoverService.objects.search(query)
            standalone_net_alias_results = StandaloneNetworkAlias.objects.search(query)
            failover_net_alias_results = FailoverNetworkAlias.objects.search(query)

            # combine querysets
            qs = list(chain(
                cluster_results,
                server_results,
                instance_results,
                database_results,
                tablespace_results,
                service_results,
                failover_service_results,
                standalone_net_alias_results,
                failover_net_alias_results
            ))
            # qs = sorted(queryset_chain,
            #             key=lambda instance: instance.pk,
            #             reverse=True
            #             )
            self.count = len(qs)  # since qs is actually a list
            return qs
        return Server.objects.none()  # just an empty queryset as default
