"""System browser urls"""

from django.urls import path

from system.views.browser import ClusterDetailView, ClusterExportView, ClusterListView, FilesystemDetailView, \
    ServerCreate, ServerDetailView, ServerExportView, ServerListView

app_name = 'system'
urlpatterns = [
    # Cluster
    path('clusters/', ClusterListView.as_view(), name='cluster-list'),
    path('clusters/export/', ClusterExportView.as_view(), name='cluster-export'),
    path('cluster/<int:pk>/', ClusterDetailView.as_view(), name='cluster-detail'),
    # Server
    path('servers/', ServerListView.as_view(), name='server-list'),
    path('servers/export/', ServerExportView.as_view(), name='server-export'),
    path('servers/add/', ServerCreate.as_view(), name='server-add'),
    path('server/<int:pk>/', ServerDetailView.as_view(), name='server-detail'),
    # Filesystem
    path('filesystem/<int:pk>/', FilesystemDetailView.as_view(), name='filesystem-detail'),
]
