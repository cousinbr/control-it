"""System module Admin"""
from django.contrib import admin

from system.models import Server

admin.site.register(Server)
