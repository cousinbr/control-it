"""System browser views"""

from csv_export.views import CSVExportView

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import CreateView, ListView, DetailView
from system.forms import ServerForm

from system.models import Cluster, Server, Filesystem


class ClusterListView(LoginRequiredMixin, ListView):
    """Cluster list view"""

    model = Cluster
    paginate_by = 10


class ClusterExportView(CSVExportView):
    """Cluster export view"""

    model = Cluster
    fields = "__all__"


class ClusterDetailView(LoginRequiredMixin, DetailView):
    """Cluster detail view"""

    model = Cluster


class ServerListView(LoginRequiredMixin, ListView):
    """Server list view"""

    model = Server
    paginate_by = 10


class ServerExportView(CSVExportView):
    """Server export view"""

    model = Server
    fields = "__all__"


class ServerDetailView(LoginRequiredMixin, DetailView):
    """Server detail view"""

    model = Server


class ServerCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    """Server create view"""

    model = Server
    form_class = ServerForm
    success_message = "%(name)s successfully added"


class FilesystemDetailView(LoginRequiredMixin, DetailView):
    """Filesystem detail view"""

    model = Filesystem
