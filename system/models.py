"""System app models"""
from django.db import models
from django.urls import reverse

from core.models import Environment, Metric, Snapshot, Vendor


# pylint: disable=R0903
class ClusterManager(models.Manager):
    """Cluster manager"""

    def search(self, query=None):
        """Search for cluster"""

        qs = self.get_queryset()
        if query is not None:
            or_lookup = models.Q(name__icontains=query)
            qs = qs.filter(or_lookup).order_by('name')
        return qs


class Cluster(models.Model):
    """Cluster model"""

    name = models.CharField('Name', max_length=255)

    objects = ClusterManager()

    class Meta:
        ordering = ['name']
        constraints = [
            models.UniqueConstraint(fields=['name'], name='cluster_set_unique')
        ]

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""

        return reverse('system:cluster-detail', kwargs={'pk': self.pk})


# pylint: disable=R0903
class ServerManager(models.Manager):
    """Server manager"""

    def search(self, query=None):
        """Search for server"""

        qs = self.get_queryset()
        if query is not None:
            or_lookup = models.Q(name__icontains=query)
            qs = qs.filter(or_lookup).order_by('name')
        return qs


class Server(models.Model):
    """Server model"""

    name = models.CharField('Name', max_length=255)
    nb_cores = models.PositiveIntegerField('Number of core on server', blank=True, null=True)
    memtotal_mb = models.PositiveIntegerField('Total memory of server', blank=True, null=True)
    site = models.CharField('Server site', max_length=16, blank=True, null=True)

    snapshot = models.ForeignKey(Snapshot, on_delete=models.CASCADE, blank=True, null=True)
    environment = models.ForeignKey(Environment, on_delete=models.CASCADE, blank=True, null=True)
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, blank=True, null=True)

    objects = ServerManager()

    class Meta:
        ordering = ['name']
        constraints = [
            models.UniqueConstraint(fields=['name'], name='server_set_unique')
        ]

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""

        return reverse('system:server-detail', kwargs={'pk': self.pk})


class Filesystem(models.Model):
    """Filesystem model"""

    mount_point = models.CharField('Mount point', max_length=255)
    type = models.CharField('Filesystem type', max_length=8)

    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    snapshot = models.ForeignKey(Snapshot, on_delete=models.CASCADE)

    class Meta:
        ordering = ['mount_point']
        constraints = [
            models.UniqueConstraint(fields=['mount_point', 'server'], name='filesystem_set_unique')
        ]

    def __str__(self):
        return f"{self.mount_point} ({self.type})"

    def get_absolute_url(self):
        """Return object absolute url"""

        return reverse('system:filesystem-detail', kwargs={'pk': self.pk})


class FilesystemSizeMetric(Metric):
    """Filesystem size metric model"""

    filesystem = models.ForeignKey(Filesystem, on_delete=models.CASCADE)
    size = models.PositiveBigIntegerField('Size')
    used = models.PositiveBigIntegerField('Used')

    class Meta(Metric.Meta):
        db_table = 'system_filesystem_size_metric'


class Devices(models.Model):
    """Devices model"""

    name = models.CharField('Name', max_length=255)
    size_gb = models.PositiveBigIntegerField('Size Gb')

    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name} ({self.size_gb})"


class NetworkInterface(models.Model):
    """NetworkInterface model"""

    PHYSICAL = 'PHYSICAL'
    BONDED = 'BONDED'

    NETIF_TYPE = [
        (PHYSICAL, 'Physical'),
        (BONDED, 'Bonded'),
    ]

    name = models.CharField('Name', max_length=16)
    type = models.CharField(max_length=8, choices=NETIF_TYPE)
    speed = models.PositiveIntegerField('Speed in Mb', null=True, blank=True)
    mtu = models.PositiveIntegerField('MTU', null=True, blank=True)
    active = models.BooleanField("Is it interface active")

    lacp_rate = models.CharField('LACP rate', max_length=16, null=True, blank=True)
    mode = models.CharField('LACP mode', max_length=16, null=True, blank=True)

    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    slave_interfaces = models.ManyToManyField("self", symmetrical=False)

    class Meta:
        ordering = ['name']
        constraints = [
            models.UniqueConstraint(fields=['name', 'server'], name='networkinterface_set_unique')
        ]

    def __str__(self):
        return f"{self.name}"


class IpAddress(models.Model):
    """IpAddress model"""

    address = models.GenericIPAddressField('IP address')
    netmask = models.GenericIPAddressField('IP netmask')
    network = models.GenericIPAddressField('IP network')
    prefix = models.PositiveIntegerField('IP network prefix')
    is_primary = models.BooleanField("Is it the primary interface IP", default=True)

    interface = models.ForeignKey(NetworkInterface, on_delete=models.CASCADE)

    class Meta:
        ordering = ['address']
        constraints = [
            models.UniqueConstraint(fields=['address', 'interface'], name='ipaddress_set_unique')
        ]

    def __str__(self):
        ip_type = ""
        if not self.is_primary:
            ip_type = " (VIP)"
        return f"{self.address}{ip_type}"
