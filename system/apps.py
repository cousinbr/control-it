"""System module apps"""
from django.apps import AppConfig


class SystemConfig(AppConfig):
    """System app config"""

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'system'
