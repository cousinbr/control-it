"""Oject helper for tests"""

from core.tests import ModelBuilder as CoreModelBuilder
from system.models import Cluster, Devices, Filesystem, IpAddress, NetworkInterface, Server


class ModelBuilder:
    """Helper class used in tests to build model objects"""

    # Cluster
    cluster_name = "Cluster"

    # Server
    server_name = "Server"
    server_nb_cores = 8
    server_memtotal_mb = 8192

    # Filesystem
    filesystem_mount_point = "/mount_point"
    filesystem_type = "ext4"

    # Device
    device_name = "sda"
    device_size_gb = 10

    # Network interface
    netif_name = "eth0"
    netif_type = NetworkInterface.PHYSICAL
    netif_active = True

    # IP Address
    ipaddress_address_primary = "192.168.0.10"
    ipaddress_address_secondary = "192.168.0.11"
    ipaddress_netmask = "255.255.255.0"
    ipaddress_network = "192.168.0.1"
    ipaddress_prefix = "25"
    ipaddress_primary = True
    ipaddress_secondary = False

    def __init__(self):

        self.core_model_builder = CoreModelBuilder()

        # Cluster
        self.cluster_obj = Cluster.objects.create(
            name=self.cluster_name,
        )

        # Server
        self.server_obj = Server.objects.create(
            name = self.server_name,
            nb_cores = self.server_nb_cores,
            memtotal_mb = self.server_memtotal_mb,
            snapshot = self.core_model_builder.snapshot_obj
        )

        # Filesystem
        self.filesystem_obj = Filesystem.objects.create(
            mount_point = self.filesystem_mount_point,
            server = self.server_obj,
            type = self.filesystem_type,
            snapshot = self.core_model_builder.snapshot_obj
        )

        # Device
        self.device_obj = Devices.objects.create(
            name = self.device_name,
            server = self.server_obj,
            vendor = self.core_model_builder.vendor_obj,
            size_gb = self.device_size_gb
        )

        # Netif
        self.netif_obj = NetworkInterface.objects.create(
            name = self.netif_name,
            server = self.server_obj,
            type = self.netif_type,
            active = self.netif_active
        )

        # IP Address
        self.ipaddress_primary_obj = IpAddress.objects.create(
            address = self.ipaddress_address_primary,
            interface = self.netif_obj,
            netmask = self.ipaddress_netmask,
            network = self.ipaddress_network,
            prefix = self.ipaddress_prefix,
            is_primary = self.ipaddress_primary
        )
        self.ipaddress_secondary_obj = IpAddress.objects.create(
            address = self.ipaddress_address_secondary,
            interface = self.netif_obj,
            netmask = self.ipaddress_netmask,
            network = self.ipaddress_network,
            prefix = self.ipaddress_prefix,
            is_primary = self.ipaddress_secondary
        )
