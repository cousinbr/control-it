"""Core app tasks"""
import logging
import traceback
from datetime import datetime
from itertools import islice
from typing import Mapping

from celery import shared_task
from django.utils import timezone
from django.utils.timezone import make_aware

from core.models import Snapshot, Vendor
from database.models import DatabaseType
from database.models.database import (
    Database,
    DatabaseReplicationSystem,
    DatabaseSizeMetric,
)
from database.models.database.oracle import (
    ContainerOracleDatabase,
    ContainerOracleFraMetric,
    OracleDatabase,
    PluggableOracleDatabase,
)
from database.models.diskgroup.oracle import OracleDiskgroup, OracleDiskgroupSizeMetric
from database.models.engine import Engine
from database.models.instance import Instance, InstanceParameterMetric
from database.models.instance.oracle import AsmOracleInstance, DatabaseOracleInstance
from database.models.tablespace import TablespaceSpaceMetric
from database.models.tablespace.oracle import OracleTablespace
from database.models.tns import (
    Endpoint,
    FailoverService,
    LocalListener,
    ScanListener,
    Service,
)
from database.models.user import UserSpaceMetric
from database.models.user.oracle import OracleUser
from system.models import (
    Cluster,
    Devices,
    Filesystem,
    FilesystemSizeMetric,
    IpAddress,
    NetworkInterface,
    Server,
)

logger = logging.getLogger(__name__)

# Bulk load batch size
BATCH_SIZE = 100

# Datetime format from facts
DATETIME_FORMAT = "%Y/%m/%d %H:%M:%S"


def load_cluster(data: dict) -> Cluster:
    """Load cluster data"""
    cluster_obj = None
    if data["databases"]["oracle"]["is_cluster"]:
        logger.info("Load cluster : %s", data["databases"]["oracle"]["cluster_name"])
        cluster_obj, unused_cluster_created = Cluster.objects.update_or_create(
            name=data["databases"]["oracle"]["cluster_name"],
        )

    return cluster_obj


def load_server(
    data: dict, server_fqdn: str, snapshot_obj: Snapshot, cluster_obj: Cluster
) -> Server:
    """Load server data"""
    logger.info("Load server : %s", server_fqdn)

    defaults = {
        "nb_cores": data["processor_cores"],
        "memtotal_mb": data["memtotal_mb"],
        "site": data["server_site"],
        "snapshot": snapshot_obj,
    }
    if cluster_obj:
        defaults["cluster"] = cluster_obj

    server_obj, unused_server_created = Server.objects.update_or_create(
        name=server_fqdn,
        defaults=defaults,
    )

    # Update snapshot
    #############################################################
    snapshot_obj.target_server = server_obj
    snapshot_obj.save()

    return server_obj


def load_filesystems(data: dict, snapshot_obj: Snapshot, server_obj: Server):
    """Load filesystems data"""
    logger.info("Load filesystem's")
    for filesystem in data["mounts"]:
        # Don't register NFS filesystem
        if filesystem["fstype"] in ("nfs"):
            continue

        logger.debug("Load filesystem : %s", filesystem["mount"])
        (
            filesystem_obj,
            unused_filesystem_created,
        ) = Filesystem.objects.update_or_create(
            mount_point=filesystem["mount"],
            server=server_obj,
            defaults={"type": filesystem["fstype"], "snapshot": snapshot_obj},
        )

        # Filesystem size metric
        #############################################################
        logger.debug("Load filesystem metric : %s", filesystem["mount"])
        FilesystemSizeMetric.objects.create(
            filesystem=filesystem_obj,
            size=filesystem["size_total"],
            used=filesystem["size_total"] - filesystem["size_available"],
            snapshot=snapshot_obj,
            day=snapshot_obj.day,
        )


def load_devices(data: dict, server_obj: Server):
    """Load disk devices data"""
    logger.info("Load device's")
    for device in data["devices"]:
        # Register only sd* device
        if device.startswith("sd"):
            logger.debug("Load device : %s", device)
            vendor_obj, unused_vendor_created = Vendor.objects.update_or_create(
                name=data["devices"][device]["vendor"]
            )

            # Find device size
            if "TB" in data["devices"][device]["size"]:
                size = int(float(data["devices"][device]["size"].split()[0]) * 1024)
            elif "GB" in data["devices"][device]["size"]:
                size = int(float(data["devices"][device]["size"].split()[0]))
            else:
                size = 0

            Devices.objects.update_or_create(
                name=device,
                server=server_obj,
                defaults={"vendor": vendor_obj, "size_gb": size},
            )


def load_network_interfaces_physical(
    data: dict, server_obj: Server
) -> Mapping[str, NetworkInterface]:
    """Load physical network interfaces"""
    logger.info("Load physical network interface's")
    physical_interfaces = {}
    for interface in data["interfaces"]:
        logger.debug("Current network interface : %s", interface)
        if "type" in data[interface] and data[interface]["type"] == "ether":
            logger.debug("Load physical network interface : %s", interface)

            # Parse data and fill defaults
            defaults = {
                "type": NetworkInterface.PHYSICAL,
                "active": data[interface]["active"],
            }
            if "speed" in data[interface] and not data[interface]["speed"] == "":
                defaults["speed"] = int(data[interface]["speed"])
            if "mtu" in data[interface] and not data[interface]["mtu"] == "":
                defaults["mtu"] = int(data[interface]["mtu"])

            (
                netif_obj,
                unused_netif_created,
            ) = NetworkInterface.objects.update_or_create(
                name=interface,
                server=server_obj,
                defaults=defaults,
            )
            # Store object for reuse
            physical_interfaces[interface] = netif_obj

            # Add IP address
            if "ipv4" in data[interface]:
                logger.debug("Load primary IP : %s", data[interface]["ipv4"]["address"])
                IpAddress.objects.update_or_create(
                    address=data[interface]["ipv4"]["address"],
                    interface=netif_obj,
                    defaults={
                        "netmask": data[interface]["ipv4"]["netmask"],
                        "network": data[interface]["ipv4"]["network"],
                        "prefix": data[interface]["ipv4"]["prefix"],
                        "is_primary": True,
                    },
                )

            # Search for secondary IP on this interface
            secondary_ips = filter(
                lambda ip: ip.startswith(f"{interface}_"), data["interfaces"]
            )
            for secondary_ip in secondary_ips:
                logger.debug("Load secondary IP : %s", secondary_ip)
                IpAddress.objects.update_or_create(
                    address=data[secondary_ip]["ipv4"]["address"],
                    interface=netif_obj,
                    defaults={
                        "netmask": data[secondary_ip]["ipv4"]["netmask"],
                        "network": data[secondary_ip]["ipv4"]["network"],
                        "prefix": data[secondary_ip]["ipv4"]["prefix"],
                        "is_primary": False,
                    },
                )
    return physical_interfaces


def load_network_interfaces_bonded(
    data: dict, server_obj: Server, physical_interfaces: Mapping[str, NetworkInterface]
):
    """Load bonded network interfaces"""
    logger.info("Load bonded network interface's")
    for interface in data["interfaces"]:
        logger.debug("Current network interface : %s", interface)
        if "type" in data[interface] and data[interface]["type"] == "bonding":
            logger.debug("Load bonded network interface : %s", interface)

            # Parse data and fill defaults
            defaults = {
                "type": NetworkInterface.BONDED,
                "active": data[interface]["active"],
            }
            if "speed" in data[interface] and not data[interface]["speed"] == "":
                defaults["speed"] = int(data[interface]["speed"])
            if "mtu" in data[interface] and not data[interface]["mtu"] == "":
                defaults["mtu"] = int(data[interface]["mtu"])
            if (
                "lacp_rate" in data[interface]
                and not data[interface]["lacp_rate"] == ""
            ):
                defaults["lacp_rate"] = data[interface]["lacp_rate"]
            if "mode" in data[interface] and not data[interface]["mode"] == "":
                defaults["mode"] = data[interface]["mode"]

            (
                netif_obj,
                unused_netif_created,
            ) = NetworkInterface.objects.update_or_create(
                name=interface,
                server=server_obj,
                defaults=defaults,
            )

            for slave in data[interface]["slaves"]:
                netif_obj.slave_interfaces.add(physical_interfaces[slave])

            # Add IP address
            if "ipv4" in data[interface]:
                logger.debug("Load primary IP : %s", data[interface]["ipv4"]["address"])
                IpAddress.objects.update_or_create(
                    address=data[interface]["ipv4"]["address"],
                    interface=netif_obj,
                    defaults={
                        "netmask": data[interface]["ipv4"]["netmask"],
                        "network": data[interface]["ipv4"]["network"],
                        "prefix": data[interface]["ipv4"]["prefix"],
                        "is_primary": True,
                    },
                )

            # Search for secondary IP on this interface
            secondary_ips = filter(
                lambda ip: ip.startswith(f"{interface}_"), data["interfaces"]
            )
            for secondary_ip in secondary_ips:
                logger.debug("Load secondary IP : %s", secondary_ip)
                for secondary_ip_conf in data[secondary_ip]["ipv4_secondaries"]:
                    IpAddress.objects.update_or_create(
                        address=secondary_ip_conf["address"],
                        interface=netif_obj,
                        defaults={
                            "netmask": secondary_ip_conf["netmask"],
                            "network": secondary_ip_conf["network"],
                            "prefix": secondary_ip_conf["prefix"],
                            "is_primary": False,
                        },
                    )


def load_network_interfaces(data: dict, server_obj: Server):
    """Load network interface data"""
    physical_interfaces = load_network_interfaces_physical(
        data=data, server_obj=server_obj
    )

    load_network_interfaces_bonded(
        data=data, server_obj=server_obj, physical_interfaces=physical_interfaces
    )


def load_vendor(name: str) -> Vendor:
    """Load vendor"""
    logger.info("Load vendor : %s", name)
    vendor_obj, unused_vendor_created = Vendor.objects.update_or_create(name=name)
    return vendor_obj


def load_tablespace(
    database_obj: OracleDatabase, snapshot_obj: Snapshot, tablespace: dict
):
    """Load tablepsace"""

    #############################################################
    # Tablespace
    #############################################################
    logger.debug("Load tablespace : %s", tablespace["tablespace_name"])
    (
        tablespace_obj,
        unused_tablespace_created,
    ) = OracleTablespace.objects.update_or_create(
        name=tablespace["tablespace_name"],
        database=database_obj,
        defaults={
            "bigfile": tablespace["bigfile"],
            "block_size": tablespace["block_size"],
            "extent_management": tablespace["extent_management"],
            "force_logging": tablespace["force_logging"],
            "segment_space_management": tablespace["segment_space_management"],
            "contents": tablespace["contents"],
            "snapshot": snapshot_obj,
        },
    )

    #############################################################
    # Tablespace metrics
    #############################################################
    if (
        "space_size" in tablespace
        and tablespace["space_size"] != ""
        and "space_used" in tablespace
        and tablespace["space_used"] != ""
        and "space_max_size" in tablespace
        and tablespace["space_max_size"] != ""
    ):
        logger.debug("Load tablespace metric : %s", tablespace["tablespace_name"])
        TablespaceSpaceMetric.objects.create(
            size=tablespace["space_size"],
            used=tablespace["space_used"],
            max_size=tablespace["space_max_size"],
            tablespace=tablespace_obj,
            snapshot=snapshot_obj,
            day=snapshot_obj.day,
        )


def load_db_user(database_obj: OracleDatabase, snapshot_obj: Snapshot, user: dict):
    """Load database users"""

    #############################################################
    # User
    #############################################################
    logger.debug("Load database user : %s", user["username"])
    defaults = {
        "account_status": user["account_status"],
        "default_tablespace": user["default_tablespace"],
        "temporary_tablespace": user["temporary_tablespace"],
        "created": make_aware(datetime.strptime(user["created"], DATETIME_FORMAT)),
        "profile": user["profile"],
        "password_versions": user["password_versions"],
        "authentication_type": user["authentication_type"],
        "common": user["common"],
        "snapshot": snapshot_obj,
    }
    if user["lock_date"] != "":
        defaults["lock_date"] = make_aware(
            datetime.strptime(user["lock_date"], DATETIME_FORMAT)
        )
    if user["expiry_date"] != "":
        defaults["expiry_date"] = make_aware(
            datetime.strptime(user["expiry_date"], DATETIME_FORMAT)
        )
    if user["last_login"] != "":
        defaults["last_login"] = make_aware(
            datetime.strptime(user["last_login"], DATETIME_FORMAT)
        )
    if user["password_change_date"] != "":
        defaults["password_change_date"] = make_aware(
            datetime.strptime(user["password_change_date"], DATETIME_FORMAT)
        )

    user_obj, unused_user_created = OracleUser.objects.update_or_create(
        name=user["username"],
        database=database_obj,
        defaults=defaults,
    )

    #############################################################
    # User metrics
    #############################################################
    if "schema_size" in user and user["schema_size"] != "":
        logger.debug("Load database user metric : %s", user["username"])
        UserSpaceMetric.objects.create(
            size=user["schema_size"],
            user=user_obj,
            snapshot=snapshot_obj,
            day=snapshot_obj.day,
        )


def get_db_domain(data: dict) -> str:
    """Retreive db_domain of one instance"""

    for parameter in data["parameters"]:
        logger.debug("get_db_domain # parameter['name'] = %s", parameter["name"])
        if parameter["name"] == "db_domain":
            return parameter["value"]

    # Return default
    return ""


def load_engine(server_obj: Server, instance: dict) -> Engine:
    """Load engine"""

    logger.info("Load engine : %s", instance["oracle_home"])
    (
        engine_obj,
        unused_instance_binary_created,
    ) = Engine.objects.update_or_create(
        path=instance["oracle_home"],
        server=server_obj,
        defaults={"version": instance["version"]},
    )

    return engine_obj


def load_asm_oracle_instance(
    snapshot_obj: Snapshot,
    server_obj: Server,
    vendor_obj: Vendor,
    engine_obj: Engine,
    instance: dict,
) -> AsmOracleInstance:
    """Load ASM instance"""

    logger.info("Load ASM instance : %s", instance["sid"])
    (
        instance_obj,
        unused_instance_created,
    ) = AsmOracleInstance.objects.update_or_create(
        name=instance["sid"],
        server=server_obj,
        defaults={
            "engine": engine_obj,
            "vendor": vendor_obj,
            "snapshot": snapshot_obj,
        },
    )

    return instance_obj


def load_database_oracle_instance(
    snapshot_obj: Snapshot,
    server_obj: Server,
    vendor_obj: Vendor,
    engine_obj: Engine,
    instance: dict,
) -> DatabaseOracleInstance:
    """Load ASM instance"""

    logger.info("Load instance : %s", instance["sid"])
    (
        instance_obj,
        unused_instance_created,
    ) = DatabaseOracleInstance.objects.update_or_create(
        name=instance["sid"],
        server=server_obj,
        defaults={
            "engine": engine_obj,
            "vendor": vendor_obj,
            "snapshot": snapshot_obj,
        },
    )

    return instance_obj


def load_instance_parameter(
    snapshot_obj: Snapshot, instance_obj: Instance, instance: dict
):
    """Load instance parameters"""

    logger.info("Load ASM instance parameter")
    objs = (
        InstanceParameterMetric(
            name=parameter["name"],
            value=parameter["value"],
            instance=instance_obj,
            snapshot=snapshot_obj,
            day=snapshot_obj.day,
        )
        for parameter in instance["parameters"]
    )
    while True:
        batch = list(islice(objs, BATCH_SIZE))
        if not batch:
            break
        InstanceParameterMetric.objects.bulk_create(batch, BATCH_SIZE)


def load_diskgroup(instance_obj: AsmOracleInstance, diskgroup: dict) -> OracleDiskgroup:
    """Load one diskgroup"""

    logger.debug("Load ASM diskgroup : %s", diskgroup["name"])
    (
        diskgroup_obj,
        unused_diskgroup_created,
    ) = OracleDiskgroup.objects.update_or_create(
        name=diskgroup["name"],
        cluster_id=diskgroup["cluster_id"],
        defaults={
            "type": diskgroup["type"],
            "compatibility": diskgroup["compatibility"],
            "database_compatibility": diskgroup["database_compatibility"],
            "voting_files": diskgroup["voting_files"],
        },
    )
    diskgroup_obj.instances.add(instance_obj)

    return diskgroup_obj


def load_oracle_diskgroup_size_metric(
    snapshot_obj: Snapshot, diskgroup_obj: OracleDiskgroup, diskgroup: dict
):
    """Load oracle diskgroup size metric"""

    logger.debug("Load ASM diskgroup metric : %s", diskgroup["name"])
    OracleDiskgroupSizeMetric.objects.update_or_create(
        date=timezone.now().replace(minute=0, second=0, microsecond=0),
        diskgroup=diskgroup_obj,
        defaults={
            "total_mb": diskgroup["total_mb"],
            "used_mb": int(diskgroup["total_mb"]) - int(diskgroup["free_mb"]),
            "snapshot": snapshot_obj,
            "day": snapshot_obj.day,
        },
    )


def load_diskgroups(
    snapshot_obj: Snapshot, instance_obj: AsmOracleInstance, instance: dict
):
    """Load diskgroups"""

    logger.info("Load ASM diskgroup's")
    for diskgroup in instance["diskgroups"]:
        # ASM diskgroup
        #############################################################
        diskgroup_obj = load_diskgroup(instance_obj=instance_obj, diskgroup=diskgroup)

        # ASM diskgroup size metric
        #############################################################
        load_oracle_diskgroup_size_metric(
            snapshot_obj=snapshot_obj, diskgroup_obj=diskgroup_obj, diskgroup=diskgroup
        )


def load_instance_asm(
    data: dict, snapshot_obj: Snapshot, server_obj: Server, vendor_obj: Vendor
):
    """Load ASM instance"""
    instance = data["databases"]["oracle"]["instances"]["asm"]

    if instance:
        # ASM Engine
        #############################################################
        engine_obj = load_engine(server_obj=server_obj, instance=instance)

        # ASM Instance
        #############################################################
        instance_obj = load_asm_oracle_instance(
            snapshot_obj=snapshot_obj,
            server_obj=server_obj,
            vendor_obj=vendor_obj,
            engine_obj=engine_obj,
            instance=instance,
        )

        # ASM Instance parameter
        #############################################################
        load_instance_parameter(
            snapshot_obj=snapshot_obj, instance_obj=instance_obj, instance=instance
        )

        # ASM diskgroup
        #############################################################
        load_diskgroups(
            snapshot_obj=snapshot_obj, instance_obj=instance_obj, instance=instance
        )


def load_database_replication_system(instance: dict) -> DatabaseReplicationSystem:
    """Load database replication system"""

    logger.info("Load replication system")
    (
        replication_system_obj,
        unused_replication_system_created,
    ) = DatabaseReplicationSystem.objects.get_or_create(
        name=instance["database"]["db_name"],
        system_id=instance["database"]["dataguard_database_id"],
    )

    return replication_system_obj


def load_database_size_metric(
    snapshot_obj: Snapshot, database_obj: Database, data: dict
):
    """Load database size metric"""

    if (
        "database_size" in data
        and "datafile_size" in data
        and "redo_size" in data
        and "tempfile_size" in data
    ):
        DatabaseSizeMetric.objects.update_or_create(
            date=timezone.now().replace(minute=0, second=0, microsecond=0),
            database=database_obj,
            defaults={
                "size": data["database_size"],
                "datafile_size": data["datafile_size"],
                "redo_size": data["redo_size"],
                "tempfile_size": data["tempfile_size"],
                "snapshot": snapshot_obj,
                "day": snapshot_obj.day,
            },
        )


def load_container_oracle_fra_metric(
    snapshot_obj: Snapshot, database_obj: Database, data: dict
):
    """Load Oracle container FRA Metric"""

    if data["size"] != "" and data["used"] != "" and data["reclaimable"] != "":
        logger.info(
            "Load container database FRA metric : %s",
            database_obj.name,
        )
        ContainerOracleFraMetric.objects.update_or_create(
            date=timezone.now().replace(minute=0, second=0, microsecond=0),
            database=database_obj,
            defaults={
                "size": data["size"],
                "used": data["used"],
                "reclaimable": data["reclaimable"],
                "snapshot": snapshot_obj,
                "day": snapshot_obj.day,
            },
        )


def load_container_oracle_database(
    snapshot_obj: Snapshot,
    replication_system_obj: DatabaseReplicationSystem,
    instance_obj: DatabaseOracleInstance,
    data: dict,
) -> ContainerOracleDatabase:
    """Load Oracle container database"""

    # Identify database type
    container_type = ContainerOracleDatabase.CDB
    if not data["database"]["is_cdb"]:
        container_type = ContainerOracleDatabase.LEGACY

    logger.info("Load container database : %s", data["database"]["db_unique_name"])
    (
        database_obj,
        unused_database_created,
    ) = ContainerOracleDatabase.objects.update_or_create(
        name=data["database"]["db_unique_name"],
        type=DatabaseType.ORACLE,
        cluster_id=data["database"]["cluster_id"],
        defaults={
            "global_name": data["database"]["db_name"],
            "dbid": data["database"]["dbid"],
            "is_cluster_database": data["database"]["is_cluster_database"],
            "created": make_aware(
                datetime.strptime(data["database"]["created"], DATETIME_FORMAT)
            ),
            "resetlogs_time": make_aware(
                datetime.strptime(data["database"]["resetlogs_time"], DATETIME_FORMAT)
            ),
            "log_mode": data["database"]["log_mode"],
            "database_role": data["database"]["database_role"],
            "force_logging": data["database"]["force_logging"],
            "flashback_on": data["database"]["flashback_on"],
            "replication_system": replication_system_obj,
            "size": data["database"]["database_size"],
            "snapshot": snapshot_obj,
            "container_type": container_type,
        },
    )
    database_obj.instances.add(instance_obj)

    return database_obj


def load_pluggable_oracle_database(
    snapshot_obj: Snapshot,
    instance_obj: DatabaseOracleInstance,
    database_obj: ContainerOracleDatabase,
    data: dict,
) -> PluggableOracleDatabase:
    """Load an oracle pluggable database"""

    logger.debug("Load PDB : %s", data["name"])
    (
        pdb_obj,
        unused_pdb_created,
    ) = PluggableOracleDatabase.objects.update_or_create(
        name=data["name"],
        type=DatabaseType.ORACLE,
        cluster_id=data["cluster_id"],
        defaults={
            "dbid": data["dbid"],
            "created": make_aware(
                datetime.strptime(data["creation_time"], DATETIME_FORMAT)
            ),
            "size": data["database_size"],
            "guid": data["guid"],
            "container": database_obj,
            "snapshot": snapshot_obj,
        },
    )
    pdb_obj.instances.add(instance_obj)

    return pdb_obj


def load_pdb(
    snapshot_obj: Snapshot,
    instance_obj: DatabaseOracleInstance,
    database_obj: ContainerOracleDatabase,
    data: dict,
):
    """Load Oracle PDB"""

    pdb_obj = load_pluggable_oracle_database(
        snapshot_obj=snapshot_obj,
        instance_obj=instance_obj,
        database_obj=database_obj,
        data=data,
    )

    #############################################################
    # Database size metric
    #############################################################
    logger.debug("Load PDB database size metric : %s", data["name"])
    load_database_size_metric(
        snapshot_obj=snapshot_obj, database_obj=pdb_obj, data=data
    )

    #############################################################
    # Tablespace
    #############################################################
    logger.debug("Load PDB tablespaces : %s", data["name"])
    for tablespace in data["tablespaces"]:
        load_tablespace(pdb_obj, snapshot_obj, tablespace)

    #############################################################
    # Users
    #############################################################
    logger.debug("Load PDB users : %s", data["name"])
    for user in data["users"]:
        load_db_user(pdb_obj, snapshot_obj, user)


def load_pdbs(
    snapshot_obj: Snapshot,
    instance_obj: DatabaseOracleInstance,
    database_obj: ContainerOracleDatabase,
    data: dict,
):
    """Load Oracle PDB's"""

    logger.info("Load PDB's")
    for pdb in data["pdbs"]:
        load_pdb(
            snapshot_obj=snapshot_obj,
            instance_obj=instance_obj,
            database_obj=database_obj,
            data=pdb,
        )


def load_database(
    snapshot_obj: Snapshot, instance_obj: DatabaseOracleInstance, data: dict
):
    """Load database"""

    if "database" in data:
        replication_system_obj = load_database_replication_system(instance=data)

        database_obj = load_container_oracle_database(
            snapshot_obj=snapshot_obj,
            replication_system_obj=replication_system_obj,
            instance_obj=instance_obj,
            data=data,
        )

        #############################################################
        # Database size metric
        #############################################################
        logger.info(
            "Load container database size metric : %s",
            data["database"]["db_unique_name"],
        )
        load_database_size_metric(
            snapshot_obj=snapshot_obj, database_obj=database_obj, data=data["database"]
        )

        #############################################################
        # Database FRA metric
        #############################################################
        load_container_oracle_fra_metric(
            snapshot_obj=snapshot_obj,
            database_obj=database_obj,
            data=data["database"]["fra"],
        )

        #############################################################
        # Tablespace
        #############################################################
        logger.info(
            "Load container database tablespaces : %s",
            data["database"]["db_unique_name"],
        )
        for tablespace in data["database"]["tablespaces"]:
            load_tablespace(database_obj, snapshot_obj, tablespace)

        #############################################################
        # Users
        #############################################################
        logger.info(
            "Load container database users : %s",
            data["database"]["db_unique_name"],
        )
        for user in data["database"]["users"]:
            load_db_user(database_obj, snapshot_obj, user)

        #############################################################
        # PDB's
        #############################################################
        load_pdbs(
            snapshot_obj=snapshot_obj,
            instance_obj=instance_obj,
            database_obj=database_obj,
            data=data,
        )


def load_instance_rdbms(
    data: dict, snapshot_obj: Snapshot, server_obj: Server, vendor_obj: Vendor
):
    """Load RDBMS instance"""
    for instance in data["databases"]["oracle"]["instances"]["rdbms"]:
        #############################################################
        # Engine
        #############################################################
        engine_obj = load_engine(server_obj=server_obj, instance=instance)

        #############################################################
        # Instance
        #############################################################
        instance_obj = load_database_oracle_instance(
            snapshot_obj=snapshot_obj,
            server_obj=server_obj,
            vendor_obj=vendor_obj,
            engine_obj=engine_obj,
            instance=instance,
        )

        #############################################################
        # Instance parameter
        #############################################################
        load_instance_parameter(
            snapshot_obj=snapshot_obj, instance_obj=instance_obj, instance=instance
        )

        #############################################################
        # Database
        #############################################################
        load_database(
            snapshot_obj=snapshot_obj, instance_obj=instance_obj, data=instance
        )


def load_listeners(data: dict, cluster_obj: Cluster, server_obj: Server):
    """Load listeners data"""
    logger.info("Load listeners")
    for listener in data["databases"]["oracle"]["listeners"]:
        logger.debug("Listener infos : %s", listener["name"])

        # SCAN Listener
        #############################################################
        if listener["type"] == "scan":
            logger.debug("Load SCAN listener")
            (
                listener_obj,
                unused_listener_created,
            ) = ScanListener.objects.update_or_create(
                name=listener["name"],
                engine=Engine.objects.get(
                    path=listener["oracle_home"], server=server_obj
                ),
                cluster=cluster_obj,
            )

        # LOCAL Listener
        #############################################################
        elif listener["type"] == "local":
            logger.debug("Load LOCAL listener")
            (
                listener_obj,
                unused_listener_created,
            ) = LocalListener.objects.update_or_create(
                name=listener["name"],
                engine=Engine.objects.get(
                    path=listener["oracle_home"], server=server_obj
                ),
                server=server_obj,
            )

        # Endpoints
        #############################################################
        for endpoint in listener["endpoints"]:
            logger.debug(
                "Load endpoint (hostname : %s, port : %s)",
                endpoint["hostname"],
                endpoint["port"],
            )
            Endpoint.objects.update_or_create(
                hostname=endpoint["hostname"],
                port=endpoint["port"],
                listener=listener_obj,
            )


def load_services(data: dict, cluster_obj: Cluster, server_obj: Server):
    """Load services data"""
    # For all databases, search for listener's serving services of this database
    logger.info("Load services")

    # List of listener for each database
    database_listeners = {}

    for instance in data["databases"]["oracle"]["instances"]["rdbms"]:
        logger.debug("Load instance %s", instance["sid"])

        if "db_unique_name" not in instance["database"]:
            logger.debug("DB_UNIQUE_NAME not found")
            continue

        # Retreive db_domain
        db_domain = get_db_domain(data=instance["database"])
        logger.debug('Founded db_domain : "%s"', db_domain)
        if db_domain != "":
            db_domain = f".{db_domain}"

        # Define container database service name. This service will be used to found on which listener
        # this instance will register services
        container_service_name = f"{instance['database']['db_unique_name']}{db_domain}"

        # Init dict for this database
        database_listeners[instance["database"]["db_unique_name"]] = []

        # Iter over listeners to find service of this database
        for listener in data["databases"]["oracle"]["listeners"]:
            for service in listener["services"]:
                if service["name"] == container_service_name:
                    # This listener have the db_unique_name service of database, so record it
                    logger.debug(
                        "Listener %s have service %s",
                        listener["name"],
                        container_service_name,
                    )

                    # SCAN Listener
                    #############################################################
                    if listener["type"] == "scan":
                        logger.debug("Load SCAN listener %s", listener["name"])
                        listener_obj = ScanListener.objects.get(
                            name=listener["name"],
                            engine=Engine.objects.get(
                                path=listener["oracle_home"], server=server_obj
                            ),
                            cluster=cluster_obj,
                        )

                    # LOCAL Listener
                    #############################################################
                    else:
                        logger.debug("Load LOCAL listener %s", listener["name"])
                        listener_obj = LocalListener.objects.get(
                            name=listener["name"],
                            engine=Engine.objects.get(
                                path=listener["oracle_home"], server=server_obj
                            ),
                            server=server_obj,
                        )
                    database_listeners[instance["database"]["db_unique_name"]].append(
                        listener_obj
                    )

    # Associate all services to listener found below
    logger.debug("Associate all services to listener found below")

    # For all instances
    for instance in data["databases"]["oracle"]["instances"]["rdbms"]:
        logger.debug("Load instance %s", instance["sid"])

        if "db_unique_name" not in instance["database"]:
            logger.debug("DB_UNIQUE_NAME not found")
            continue

        database = ContainerOracleDatabase.objects.get(
            name=instance["database"]["db_unique_name"],
            instances__server=server_obj,
        )

        # For all service of this database
        for service in instance["database"]["services"]:
            # Skip service with network name empty
            if service["network_name"] == "":
                logger.debug("Skip service %s, network_name empty", service["name"])
                continue

            # Retreive failover service for this service
            (
                failover_service,
                unused_failover_service_created,
            ) = FailoverService.objects.get_or_create(
                name=service["network_name"],
                replication_system=database.replication_system,
            )

            service_obj, unused_service_created = Service.objects.update_or_create(
                name=service["network_name"],
                database=database,
                failover_service=failover_service,
            )
            for listener_obj in database_listeners[
                instance["database"]["db_unique_name"]
            ]:
                service_obj.listeners.add(listener_obj)

                # Update service hostname & port
                service_updated = False
                if (
                    not service_updated
                    and (
                        # If it's a cluster database and this is a scan listener
                        instance["database"]["is_cluster_database"]
                        and isinstance(listener_obj, ScanListener)
                    )
                    or (
                        # If it's not a cluster database and it's a local listener
                        not instance["database"]["is_cluster_database"]
                        and isinstance(listener_obj, LocalListener)
                    )
                ):
                    # Retreive listener endpoint
                    endpoint = listener_obj.endpoint_set.first()

                    service_obj.hostname = endpoint.hostname
                    service_obj.port = endpoint.port
                    service_obj.save()
                    service_updated = True

        # For all pdbs of this instance
        for pdb in instance["pdbs"]:
            database = PluggableOracleDatabase.objects.get(
                name=pdb["name"],
                container__name=instance["database"]["db_unique_name"],
                instances__server=server_obj,
            )

            # For all service of this pdb
            for service in pdb["services"]:
                # Retreive failover service for this service
                (
                    failover_service,
                    unused_failover_service_created,
                ) = FailoverService.objects.get_or_create(
                    name=service["network_name"],
                    replication_system=database.container.replication_system,
                )

                (
                    service_obj,
                    unused_service_created,
                ) = Service.objects.update_or_create(
                    name=service["network_name"],
                    database=database,
                    failover_service=failover_service,
                )
                for listener_obj in database_listeners[
                    instance["database"]["db_unique_name"]
                ]:
                    service_obj.listeners.add(listener_obj)

                    # Update service hostname & port
                    service_updated = False
                    if (
                        not service_updated
                        and (
                            # If it's a cluster database and this is a scan listener
                            instance["database"]["is_cluster_database"]
                            and isinstance(listener_obj, ScanListener)
                        )
                        or (
                            # If it's not a cluster database and it's a local listener
                            not instance["database"]["is_cluster_database"]
                            and isinstance(listener_obj, LocalListener)
                        )
                    ):
                        # Retreive listener endpoint
                        endpoint = listener_obj.endpoint_set.first()

                        service_obj.hostname = endpoint.hostname
                        service_obj.port = endpoint.port
                        service_obj.save()
                        service_updated = True


@shared_task
def load_facts(server_fqdn: str, snapshot_id: int):
    """Core load facts"""

    try:
        logger.info("Load facts")

        snapshot_obj = Snapshot.objects.get(pk=snapshot_id)

        data = snapshot_obj.payload

        #############################################################
        # Cluster
        #############################################################
        cluster_obj = load_cluster(data)

        #############################################################
        # Server
        #############################################################

        # Server
        #############################################################
        server_obj = load_server(data, server_fqdn, snapshot_obj, cluster_obj)

        # Filesystem's
        #############################################################
        load_filesystems(data, snapshot_obj, server_obj)

        # Device's
        #############################################################
        load_devices(data, server_obj)

        # Network interface
        #############################################################
        load_network_interfaces(data, server_obj)

        #############################################################
        # Vendor
        #############################################################
        vendor_obj = load_vendor("Oracle")

        #############################################################
        # ASM
        #############################################################
        load_instance_asm(data, snapshot_obj, server_obj, vendor_obj)

        #############################################################
        # RDBMS instance
        #############################################################
        load_instance_rdbms(data, snapshot_obj, server_obj, vendor_obj)

        #############################################################
        # Listeners
        #############################################################
        load_listeners(data, cluster_obj, server_obj)

        # Services
        #############################################################
        load_services(data, cluster_obj, server_obj)

        logger.info("Set snapshot load as SUCCESS")
        snapshot_obj.status = Snapshot.StatusChoices.SUCCESS
        snapshot_obj.processed_date = make_aware(datetime.now())
        snapshot_obj.save()

    except Exception as exception:
        logger.error("Snapshot load on error")
        snapshot_obj.status = Snapshot.StatusChoices.FAILED
        snapshot_obj.processed_date = make_aware(datetime.now())
        snapshot_obj.error = traceback.format_exc()
        snapshot_obj.save()

        raise exception
