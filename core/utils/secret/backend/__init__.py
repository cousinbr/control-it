"""Secret backend ABC and dummy implementation"""

import abc


class SecretBackend(metaclass=abc.ABCMeta):
    """Secret backend interface"""

    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'get_secret') and 
                callable(subclass.get_secret) or 
                NotImplemented)


class DummySecretBackend():
    """Dummy secret backen implementation"""

    def __init__(self):
        self.secrets: dict = {'key': "secret"}

    def get_secret(self, key: str, **kwargs) -> str:
        if key in self.secrets:
            return self.secrets[key]
        return ""
