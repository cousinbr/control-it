"""Core api urls"""

from django.urls import path

from core.views.api import GetAnsibleInventoryView, LoadFactsView

app_name = 'core-api'
urlpatterns = [
    path('load_facts/', LoadFactsView.as_view(), name='load-facts'),
    path('ansible_inventory/', GetAnsibleInventoryView.as_view(), name='ansible-inventory'),
]
