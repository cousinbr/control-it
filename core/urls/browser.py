"""Core brownser urls"""

from django.urls import path

from core.views.browser import IndexView, CoreLoginView, CoreLogoutView, SnapshotDetailView, SnapshotListView


app_name = 'core'
urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('login/', CoreLoginView.as_view(), name='login'),
    path('logout/', CoreLogoutView.as_view(next_page='core:login'), name='logout'),
    # Snapshot
    path('snapshots/', SnapshotListView.as_view(), name='snapshot-list'),
    path('snapshots/<str:status>/', SnapshotListView.as_view(), name='filtered-snapshot-list'),
    path('snapshot/<int:pk>/', SnapshotDetailView.as_view(), name='snapshot-detail'),
]
