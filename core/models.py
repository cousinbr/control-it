"""Core app models"""
import datetime
from django.contrib.auth.models import AbstractUser
from django.urls import reverse
from django.utils import timezone
from django.db import models


class User(AbstractUser):
    """Custom Django user"""


class Environment(models.Model):
    """Environment model"""

    name = models.CharField('Name', max_length=255)
    code = models.CharField('Code', max_length=255)
    order_weight = models.PositiveSmallIntegerField('Order weight', default=99)

    def __str__(self):
        return f"{self.name} ({self.code})"

    class Meta:
        ordering = ['order_weight', 'name']
        constraints = [
            models.UniqueConstraint(
                fields=['name'], name='environment_name_unique'),
            models.UniqueConstraint(
                fields=['code'], name='environment_code_unique'),
        ]


class SnapshotManager(models.Manager):
    """Database manager"""

    def nb_by_status(self):
        """Search for database"""

        qs = Snapshot.objects.values('status').order_by('status').annotate(count=models.Count('status'))
        return qs


class Snapshot(models.Model):
    """Snapshot model"""

    class StatusChoices(models.TextChoices):
        """Snapshot status choices"""

        PENDING = "PENDING", "Awaiting celery to process the task"
        SUCCESS = "SUCCESS", "Task done with success"
        FAILED = "FAILED", "Task failed to be processed"

    date = models.DateTimeField('Add date', default=timezone.now)
    day = models.DateField('Add day', default=datetime.date.today)
    payload = models.JSONField('Data')
    status = models.CharField('Status', max_length=10, choices=StatusChoices.choices, default=StatusChoices.PENDING)
    processed_date = models.DateTimeField('Processed date', null=True, blank=True)
    error = models.TextField('Error message', null=True, blank=True)
    target_server = models.ForeignKey('system.Server', on_delete=models.CASCADE, related_name='+', null=True)

    objects = SnapshotManager()

    class Meta:
        ordering = ['-date']

    def __str__(self):
        """Return string representation"""
        return f"{self.id}"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse('core:snapshot-detail', kwargs={'pk': self.pk})


class Metric(models.Model):
    """Metric model
    
    day is used to add partitionment on Metric table
    snapshot is used to easly find last metric associated with model who register his last snapshot_id
    date refer to the metric value date
    """

    date = models.DateTimeField('Metric date', default=timezone.now)
    day = models.DateField('Add day', default=datetime.date.today)
    snapshot = models.ForeignKey(Snapshot, on_delete=models.CASCADE)

    class Meta:
        abstract = True
        ordering = ['date']


class Vendor(models.Model):
    """Vendor model"""

    name = models.CharField('Name', max_length=255)

    def __str__(self):
        return f"{self.name}"
