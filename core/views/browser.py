"""Core brownser views"""

import datetime
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView, ListView, DetailView

from core.models import Snapshot


class IndexView(LoginRequiredMixin, TemplateView):
    """Core index viex"""

    template_name = "core/index.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)

        # Add snapshots context
        date_from = datetime.datetime.now() - datetime.timedelta(days=1)
        context["snapshots_nb_by_status"] = Snapshot.objects.nb_by_status().filter(date__gt=date_from)

        return context


class CoreLoginView(LoginView):
    """Core login view"""


class CoreLogoutView(LoginRequiredMixin, LogoutView):
    """Core logout view"""

    next_page = "core:index"


class SnapshotListView(LoginRequiredMixin, ListView):
    """Snapshot list view"""

    model = Snapshot
    paginate_by = 10

    def get_queryset(self):
        if "status" in self.kwargs:
            status = self.kwargs['status']
            if status in (Snapshot.StatusChoices.FAILED, Snapshot.StatusChoices.PENDING, Snapshot.StatusChoices.SUCCESS):
                return Snapshot.objects.filter(status=status)

        return super().get_queryset()


class SnapshotDetailView(LoginRequiredMixin, DetailView):
    """Snapshot detail view"""

    model = Snapshot
