"""Oject helper for tests"""

import os

from django import setup

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "controlit.settings")
setup()

from core.models import Environment, Snapshot, Vendor


class ModelBuilder:
    """Helper class used in tests to build model objects"""

    # Snapshot
    snapshot_payload = '{"data": ""}'

    # Environment
    environment_name = "Environment"
    environment_code = "ENV"

    # Vendor
    vendor_name = "Vendor"

    def __init__(self):
        # Snapshot
        self.snapshot_obj = Snapshot.objects.create(payload=self.snapshot_payload)

        # Environment
        self.environment_obj, dummy = Environment.objects.get_or_create(
            name=self.environment_name, code=self.environment_code
        )

        # Vendor
        self.vendor_obj, dummy = Vendor.objects.get_or_create(name=self.vendor_name)
