"""Core app tasks Test"""

from unittest.mock import patch

from django.test import TestCase
from django.utils import timezone

from core.models import Vendor
from core.tasks import (
    get_db_domain,
    load_asm_oracle_instance,
    load_cluster,
    load_container_oracle_database,
    load_container_oracle_fra_metric,
    load_database,
    load_database_oracle_instance,
    load_database_replication_system,
    load_database_size_metric,
    load_db_user,
    load_devices,
    load_diskgroup,
    load_diskgroups,
    load_engine,
    load_facts,
    load_filesystems,
    load_instance_asm,
    load_instance_parameter,
    load_instance_rdbms,
    load_listeners,
    load_network_interfaces,
    load_network_interfaces_bonded,
    load_network_interfaces_physical,
    load_oracle_diskgroup_size_metric,
    load_pdb,
    load_pdbs,
    load_pluggable_oracle_database,
    load_server,
    load_services,
    load_tablespace,
    load_vendor,
)
from database.models.database import DatabaseReplicationSystem, DatabaseSizeMetric
from database.models.database.oracle import (
    ContainerOracleDatabase,
    ContainerOracleFraMetric,
    PluggableOracleDatabase,
)
from database.models.diskgroup.oracle import OracleDiskgroup, OracleDiskgroupSizeMetric
from database.models.engine import Engine
from database.models.instance import InstanceParameterMetric
from database.models.instance.oracle import AsmOracleInstance, DatabaseOracleInstance
from database.models.tablespace import Tablespace
from database.models.tns import LocalListener, ScanListener
from database.models.user.oracle import OracleUser
from database.tests.models import ModelBuilder
from system.models import Cluster, Devices, Filesystem, NetworkInterface, Server


class LoadClusterTestCase(TestCase):
    """load_cluster test"""

    def setUp(self):
        """Init data"""

        self.data = {
            "databases": {"oracle": {"is_cluster": None, "cluster_name": "myCluster"}}
        }

    def test_load_cluster_1(self):
        """Test is_cluster = True"""

        self.data["databases"]["oracle"]["is_cluster"] = True
        cluster_obj = load_cluster(data=self.data)

        self.assertIsInstance(cluster_obj, Cluster)

    def test_load_cluster_2(self):
        """Test is_cluster = False"""

        self.data["databases"]["oracle"]["is_cluster"] = False
        cluster_obj = load_cluster(data=self.data)

        self.assertIsNone(cluster_obj)


class LoadServerTestCase(TestCase):
    """load_server test"""

    def setUp(self):
        """Init data"""

        self.data = {
            "processor_cores": 12,
            "memtotal_mb": "4096",
            "server_site": "SITE_A",
        }

        model_builder = ModelBuilder()
        self.server_fqdn = model_builder.system_model_builder.server_name
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.cluster_obj = model_builder.system_model_builder.cluster_obj

    def test_load_server_1(self):
        """Test cluster_obj is a Cluster obj"""

        server_obj = load_server(
            data=self.data,
            server_fqdn=self.server_fqdn,
            snapshot_obj=self.snapshot_obj,
            cluster_obj=self.cluster_obj,
        )

        self.assertIsInstance(server_obj, Server)

    def test_load_server_2(self):
        """Test cluster_obj is None"""

        self.cluster_obj = None
        server_obj = load_server(
            data=self.data,
            server_fqdn=self.server_fqdn,
            snapshot_obj=self.snapshot_obj,
            cluster_obj=self.cluster_obj,
        )

        self.assertIsInstance(server_obj, Server)
        self.assertIsNone(server_obj.cluster)


class LoadFilesystemsTestCase(TestCase):
    """load_filesystems test"""

    def setUp(self):
        """Init data"""

        self.data = {
            "mounts": [
                {
                    "fstype": None,
                    "mount": "/mount_point",
                    "size_total": 100,
                    "size_available": 100,
                }
            ]
        }

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.server_obj = model_builder.system_model_builder.server_obj

    def test_load_filesystems_1(self):
        """Test load ext4 FS"""

        self.data["mounts"][0]["fstype"] = "ext4"
        load_filesystems(
            data=self.data, snapshot_obj=self.snapshot_obj, server_obj=self.server_obj
        )
        filesystem_obj = Filesystem.objects.get(
            mount_point=self.data["mounts"][0]["mount"], server=self.server_obj
        )

        self.assertIsInstance(filesystem_obj, Filesystem)
        self.assertEqual(filesystem_obj.type, self.data["mounts"][0]["fstype"])

    def test_load_filesystems_2(self):
        """Test load nfs FS"""

        self.data["mounts"][0]["fstype"] = "nfs"
        load_filesystems(
            data=self.data, snapshot_obj=self.snapshot_obj, server_obj=self.server_obj
        )
        filesystems = Filesystem.objects.filter(type=self.data["mounts"][0]["fstype"])

        self.assertEqual(len(filesystems), 0)


class LoadDevicesTestCase(TestCase):
    """load_devices test"""

    def setUp(self):
        """Init data"""

        model_builder = ModelBuilder()
        self.server_obj = model_builder.system_model_builder.server_obj

    def test_load_devices_1(self):
        """Test device not stating with sd"""

        data = {"devices": {"xsda": {"vendor": "Vendor", "size": ""}}}
        load_devices(data=data, server_obj=self.server_obj)

        devices = Devices.objects.exclude(name__istartswith="sd")
        self.assertEqual(len(devices), 0)

    def test_load_devices_2(self):
        """Test device with size in GB"""

        device_name = "sda"
        data = {"devices": {device_name: {"vendor": "Vendor", "size": "1 GB"}}}
        load_devices(data=data, server_obj=self.server_obj)

        device_obj = Devices.objects.get(name=device_name, server=self.server_obj)
        self.assertIsInstance(device_obj, Devices)
        self.assertEqual(device_obj.size_gb, 1)

    def test_load_devices_3(self):
        """Test device with size in TB"""

        device_name = "sda"
        data = {"devices": {device_name: {"vendor": "Vendor", "size": "1 TB"}}}
        load_devices(data=data, server_obj=self.server_obj)

        device_obj = Devices.objects.get(name=device_name, server=self.server_obj)
        self.assertIsInstance(device_obj, Devices)
        self.assertEqual(device_obj.size_gb, 1024)

    def test_load_devices_4(self):
        """Test device with size in something not GB or TB"""

        device_name = "sda"
        data = {"devices": {device_name: {"vendor": "Vendor", "size": "1 XX"}}}
        load_devices(data=data, server_obj=self.server_obj)

        device_obj = Devices.objects.get(name=device_name, server=self.server_obj)
        self.assertIsInstance(device_obj, Devices)
        self.assertEqual(device_obj.size_gb, 0)


class LoadNetworkInterfacesTestCase(TestCase):
    """Load network interfaces test"""

    def setUp(self):
        """Init data"""

        self.data = {
            "interfaces": [
                "bond0_2",
                "bond0_1",
                "bond0",
                "bond1",
                "eth1_2",
                "eth3_1",
                "eth5",
                "eth4",
                "eth3",
                "eth2",
                "eth1",
                "eth0",
            ],
            "bond0": {
                "active": True,
                "device": "bond0",
                "ipv4": {
                    "address": "192.168.0.1",
                    "broadcast": "192.168.0.255",
                    "netmask": "255.255.255.0",
                    "network": "192.168.0.0",
                    "prefix": "24",
                },
                "lacp_rate": "fast",
                "miimon": "100",
                "mode": "802.3ad",
                "mtu": 1500,
                "slaves": ["eth0", "eth2"],
                "speed": 20000,
                "type": "bonding",
            },
            "bond0_1": {
                "ipv4_secondaries": [
                    {
                        "address": "192.168.0.182",
                        "broadcast": "192.168.0.255",
                        "netmask": "255.255.255.0",
                        "network": "192.168.0.0",
                        "prefix": "24",
                    }
                ]
            },
            "bond0_2": {
                "ipv4_secondaries": [
                    {
                        "address": "192.168.0.179",
                        "broadcast": "192.168.0.255",
                        "netmask": "255.255.255.0",
                        "network": "192.168.0.0",
                        "prefix": "24",
                    }
                ]
            },
            "bond1": {
                "active": True,
                "device": "bond0",
                "miimon": "100",
                "slaves": ["eth0", "eth2"],
                "type": "bonding",
            },
            "eth0": {
                "active": True,
                "device": "eth0",
                "mtu": 1500,
                "speed": 10000,
                "type": "ether",
            },
            "eth1": {
                "active": True,
                "device": "eth1",
                "ipv4": {
                    "address": "192.168.0.6",
                    "broadcast": "192.168.0.255",
                    "netmask": "255.255.255.0",
                    "network": "192.168.0.0",
                    "prefix": "24",
                },
                "speed": 10000,
                "type": "ether",
            },
            "eth1_2": {
                "ipv4": {
                    "address": "169.254.18.135",
                    "broadcast": "169.254.31.255",
                    "netmask": "255.255.240.0",
                    "network": "169.254.16.0",
                    "prefix": "20",
                }
            },
            "eth2": {
                "active": True,
                "device": "eth2",
                "mtu": 1500,
                "speed": 10000,
                "type": "ether",
            },
            "eth3": {
                "active": True,
                "device": "eth3",
                "ipv4": {
                    "address": "192.168.0.6",
                    "broadcast": "192.168.0.255",
                    "netmask": "255.255.255.0",
                    "network": "192.168.0.0",
                    "prefix": "24",
                },
                "macaddress": "f8:f2:1e:88:79:b1",
                "mtu": 1500,
                "promisc": False,
                "speed": 10000,
                "type": "ether",
            },
            "eth3_1": {
                "ipv4": {
                    "address": "169.254.8.152",
                    "broadcast": "169.254.15.255",
                    "netmask": "255.255.240.0",
                    "network": "169.254.0.0",
                    "prefix": "20",
                }
            },
            "eth4": {"active": False, "device": "eth4", "mtu": 1500, "type": "ether"},
            "eth5": {"active": False, "device": "eth5", "mtu": 1500, "type": "ether"},
        }

        model_builder = ModelBuilder()
        self.server_obj = model_builder.system_model_builder.server_obj

        self.physical_interfaces = load_network_interfaces_physical(
            data=self.data, server_obj=self.server_obj
        )

    def test_load_network_interfaces_physical_1(self):
        """Test physical interface load"""

        physical_interfaces = load_network_interfaces_physical(
            data=self.data, server_obj=self.server_obj
        )

        self.assertNotEqual(len(physical_interfaces), 0)
        self.assertIsInstance(physical_interfaces["eth0"], NetworkInterface)

        eth0 = NetworkInterface.objects.get(name="eth0", server=self.server_obj)
        self.assertIsInstance(eth0, NetworkInterface)

    def test_load_network_interfaces_bonded_1(self):
        """Test bonded interface load"""

        load_network_interfaces_bonded(
            data=self.data,
            server_obj=self.server_obj,
            physical_interfaces=self.physical_interfaces,
        )

        bond0 = NetworkInterface.objects.get(name="bond0", server=self.server_obj)
        self.assertIsInstance(bond0, NetworkInterface)

    @patch("core.tasks.load_network_interfaces_physical", autospec=True)
    @patch("core.tasks.load_network_interfaces_bonded", autospec=True)
    def test_load_network_interfaces_1(self, unused_mock_bonded, unused_mock_physical):
        """Test load network interfaces"""

        load_network_interfaces(data=self.data, server_obj=self.server_obj)


class LoadVendorTestCase(TestCase):
    """Load vendor test"""

    def test_load_vendor_1(self):
        """Test load vendor"""

        vendor = load_vendor(name="TestVendor")
        self.assertIsInstance(vendor, Vendor)


class LoadTablespaceTestCase(TestCase):
    """Load tablespace test case"""

    def setUp(self):
        """Init data"""

        self.tablespace = {
            "bigfile": "YES",
            "block_size": "8192",
            "contents": "PERMANENT",
            "extent_management": "LOCAL",
            "force_logging": "YES",
            "logging": "LOGGING",
            "segment_space_management": "MANUAL",
            "tablespace_name": "TABLESPACE_NAME_TEST",
            "space_size": "34358689792",
            "space_used": "1578565632",
            "space_max_size": "34358689792",
        }

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.database_obj = model_builder.container_oracle_database_cdb_obj

    def test_load_tablespace_1(self):
        """Test load tablespace"""

        load_tablespace(
            database_obj=self.database_obj,
            snapshot_obj=self.snapshot_obj,
            tablespace=self.tablespace,
        )

        tablespace = Tablespace.objects.get(name=self.tablespace["tablespace_name"])
        self.assertIsInstance(tablespace, Tablespace)

    def test_load_tablespace_2(self):
        """Test load tablespace without size"""

        data = self.tablespace.copy()
        data.pop("space_size")
        data.pop("space_used")
        data.pop("space_max_size")
        load_tablespace(
            database_obj=self.database_obj,
            snapshot_obj=self.snapshot_obj,
            tablespace=data,
        )

        tablespace = Tablespace.objects.get(name=self.tablespace["tablespace_name"])
        self.assertIsInstance(tablespace, Tablespace)


class LoadDbUserTestCase(TestCase):
    """Load db user test case"""

    def setUp(self):
        """Init data"""

        self.user = {
            "account_status": "OPEN",
            "authentication_type": "PASSWORD",
            "common": "YES",
            "created": "2017/04/25 14:47:20",
            "default_tablespace": "SYSTEM",
            "expiry_date": "2017/04/25 14:47:20",
            "external_name": "",
            "initial_rsrc_consumer_group": "SYS_GROUP",
            "last_login": "2017/04/25 14:47:20",
            "local_temp_tablespace": "SYSTEM",
            "lock_date": "2017/04/25 14:47:20",
            "password_change_date": "2017/04/25 14:47:20",
            "password_versions": "10G 11G ",
            "profile": "DEFAULT",
            "proxy_only_connect": "N",
            "schema_size": "123456789",
            "temporary_tablespace": "TEMP",
            "username": "USERNAME_TEST",
        }

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.database_obj = model_builder.container_oracle_database_cdb_obj

    def test_load_db_user_1(self):
        """Test db user"""

        load_db_user(
            database_obj=self.database_obj,
            snapshot_obj=self.snapshot_obj,
            user=self.user,
        )

        user = OracleUser.objects.get(name=self.user["username"])
        self.assertIsInstance(user, OracleUser)

    def test_load_db_user_2(self):
        """Test db user"""

        data = self.user.copy()
        data["lock_date"] = ""
        data["expiry_date"] = ""
        data["last_login"] = ""
        data["password_change_date"] = ""
        data["schema_size"] = ""
        load_db_user(
            database_obj=self.database_obj, snapshot_obj=self.snapshot_obj, user=data
        )

        user = OracleUser.objects.get(name=self.user["username"])
        self.assertIsInstance(user, OracleUser)


class GetDbdomainTestCase(TestCase):
    """Get db domain test"""

    def setUp(self):
        """Init data"""

        self.db_domain = "test_domain"
        self.data = {
            "parameters": [
                {"name": "dummy", "value": "dummy"},
                {"name": "db_domain", "value": self.db_domain},
            ]
        }

    def test_get_db_domain_1(self):
        """Test get db_domain"""

        db_domain = get_db_domain(data=self.data)
        self.assertEqual(db_domain, self.db_domain)

    def test_get_db_domain_2(self):
        """Test get db_domain empty"""

        data = {
            "parameters": [
                {"name": "dummy", "value": "dummy"},
            ]
        }
        db_domain = get_db_domain(data=data)
        self.assertEqual(db_domain, "")


class LoadEngineTestCase(TestCase):
    """Load engine test case"""

    def setUp(self):
        """Init data"""

        self.db_domain = "test_domain"
        self.data = {"oracle_home": "/u01/product/rdbms/19.18", "version": "19.18"}

        model_builder = ModelBuilder()
        self.server_obj = model_builder.system_model_builder.server_obj

    def test_load_engine_1(self):
        """Test load_engine"""

        engine = load_engine(server_obj=self.server_obj, instance=self.data)
        self.assertIsInstance(engine, Engine)
        self.assertEqual(engine.version, self.data["version"])


class LoadAsmOracleInstanceTestCase(TestCase):
    """Load ASM Oracle instance test case"""

    def setUp(self):
        """Init data"""

        self.data = {"sid": "+ASM"}

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.server_obj = model_builder.system_model_builder.server_obj
        self.vendor_obj = model_builder.core_model_builder.vendor_obj
        self.engine_obj = model_builder.engine_obj

    def test_load_asm_oracle_instance_1(self):
        """Test load_asm_oracle_instance"""

        instance = load_asm_oracle_instance(
            snapshot_obj=self.snapshot_obj,
            server_obj=self.server_obj,
            vendor_obj=self.vendor_obj,
            engine_obj=self.engine_obj,
            instance=self.data,
        )
        self.assertIsInstance(instance, AsmOracleInstance)


class LoadDatabaseOracleInstanceTestCase(TestCase):
    """Load database Oracle instance test case"""

    def setUp(self):
        """Init data"""

        self.data = {"sid": "ORCL"}

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.server_obj = model_builder.system_model_builder.server_obj
        self.vendor_obj = model_builder.core_model_builder.vendor_obj
        self.engine_obj = model_builder.engine_obj

    def test_load_database_oracle_instance_1(self):
        """Test load_database_oracle_instance"""

        instance = load_database_oracle_instance(
            snapshot_obj=self.snapshot_obj,
            server_obj=self.server_obj,
            vendor_obj=self.vendor_obj,
            engine_obj=self.engine_obj,
            instance=self.data,
        )
        self.assertIsInstance(instance, DatabaseOracleInstance)


class LoadInstanceParameterTestCase(TestCase):
    """Load instance parameter"""

    def setUp(self):
        """Init data"""

        self.data = {
            "parameters": [
                {"name": "large_pool_size", "value": "12M"},
                {"name": "remote_login_passwordfile", "value": "EXCLUSIVE"},
                {"name": "asm_diskstring", "value": "/dev/oracleasm/*"},
                {"name": "asm_diskgroups", "value": "DGFRA"},
                {"name": "asm_power_limit", "value": "1"},
            ]
        }

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.instance_obj = model_builder.database_oracle_instance_obj

    def test_load_instance_parameter_1(self):
        """Test load instance parameter"""

        load_instance_parameter(
            snapshot_obj=self.snapshot_obj,
            instance_obj=self.instance_obj,
            instance=self.data,
        )

        parameters = InstanceParameterMetric.objects.filter(
            instance=self.instance_obj,
            snapshot=self.snapshot_obj,
            day=self.snapshot_obj.day,
        )
        self.assertEqual(len(parameters), len(self.data["parameters"]))


class LoadDiskgroupTestCase(TestCase):
    """Load diskgroup"""

    def setUp(self):
        """Init data"""

        self.data = {
            "allocation_unit_size": "1048576",
            "block_size": "4096",
            "cluster_id": "ba5d0db8c6613f8db2baf2a6463081a48d2b8452942cb64ab8d244kchgzkcjzk",
            "cold_used_mb": "52958003",
            "compatibility": "19.0.0.0.0",
            "database_compatibility": "12.1.0.0.0",
            "free_mb": "30928077",
            "group_number": "1",
            "hot_used_mb": "0",
            "logical_sector_size": "512",
            "name": "DGDATA",
            "offline_disks": "0",
            "required_mirror_free_mb": "0",
            "sector_size": "512",
            "state": "MOUNTED",
            "total_mb": "83886080",
            "type": "EXTERN",
            "usable_file_mb": "30928077",
            "voting_files": "Y",
        }

        model_builder = ModelBuilder()
        self.instance_obj = model_builder.asm_oracle_instance_obj

    def test_load_diskgroup(self):
        """Test load diskgroup"""

        diskgroup = load_diskgroup(instance_obj=self.instance_obj, diskgroup=self.data)
        self.assertIsInstance(diskgroup, OracleDiskgroup)


class LoadOracleDiskgroupSizeMetricTestCase(TestCase):
    """Load diskgroup metric"""

    def setUp(self):
        """Init data"""

        self.data = {
            "free_mb": "30928077",
            "name": "DGDATA",
            "total_mb": "83886080",
        }

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.diskgroup_obj = model_builder.oracle_diskgroup_obj

    def test_load_oracle_diskgroup_size_metric_1(self):
        """Test load oracle diskgroup size metric"""

        load_oracle_diskgroup_size_metric(
            snapshot_obj=self.snapshot_obj,
            diskgroup_obj=self.diskgroup_obj,
            diskgroup=self.data,
        )

        size = OracleDiskgroupSizeMetric.objects.get(
            date=timezone.now().replace(minute=0, second=0, microsecond=0),
            diskgroup=self.diskgroup_obj,
        )
        self.assertIsInstance(size, OracleDiskgroupSizeMetric)


class LoadDiskgroupsTestCase(TestCase):
    """Load diskgroups"""

    def setUp(self):
        """Init data"""

        self.data = {"diskgroups": ["dg1", "dg2"]}

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.instance_obj = model_builder.asm_oracle_instance_obj

    @patch("core.tasks.load_diskgroup", autospec=True)
    @patch("core.tasks.load_oracle_diskgroup_size_metric", autospec=True)
    def test_load_diskgroups_1(
        self, mock_load_oracle_diskgroup_size_metric, mock_load_diskgroup
    ):
        """Test load diskgroups"""

        load_diskgroups(
            snapshot_obj=self.snapshot_obj,
            instance_obj=self.instance_obj,
            instance=self.data,
        )

        self.assertEqual(
            mock_load_oracle_diskgroup_size_metric.call_count,
            len(self.data["diskgroups"]),
        )
        self.assertEqual(mock_load_diskgroup.call_count, len(self.data["diskgroups"]))


class LoadInstanceAsmTestCase(TestCase):
    """Load ASM instance"""

    def setUp(self):
        """Init data"""

        self.data = {"databases": {"oracle": {"instances": {"asm": "Test"}}}}

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.server_obj = model_builder.system_model_builder.server_obj
        self.vendor_obj = model_builder.core_model_builder.vendor_obj

    @patch("core.tasks.load_engine", autospec=True)
    @patch("core.tasks.load_asm_oracle_instance", autospec=True)
    @patch("core.tasks.load_instance_parameter", autospec=True)
    @patch("core.tasks.load_diskgroups", autospec=True)
    def test_load_instance_asm_1(
        self,
        mock_load_diskgroups,
        mock_load_instance_parameter,
        mock_load_asm_oracle_instance,
        mock_load_engine,
    ):
        """Test test_load_instance_asm"""

        load_instance_asm(
            snapshot_obj=self.snapshot_obj,
            server_obj=self.server_obj,
            vendor_obj=self.vendor_obj,
            data=self.data,
        )
        self.assertEqual(mock_load_diskgroups.call_count, 1)
        self.assertEqual(mock_load_instance_parameter.call_count, 1)
        self.assertEqual(mock_load_asm_oracle_instance.call_count, 1)
        self.assertEqual(mock_load_engine.call_count, 1)

    @patch("core.tasks.load_engine", autospec=True)
    @patch("core.tasks.load_asm_oracle_instance", autospec=True)
    @patch("core.tasks.load_instance_parameter", autospec=True)
    @patch("core.tasks.load_diskgroups", autospec=True)
    def test_load_instance_asm_2(
        self,
        mock_load_diskgroups,
        mock_load_instance_parameter,
        mock_load_asm_oracle_instance,
        mock_load_engine,
    ):
        """Test test_load_instance_asm without ASM instance"""

        data = self.data.copy()
        data["databases"]["oracle"]["instances"]["asm"] = {}

        load_instance_asm(
            snapshot_obj=self.snapshot_obj,
            server_obj=self.server_obj,
            vendor_obj=self.vendor_obj,
            data=self.data,
        )
        self.assertEqual(mock_load_diskgroups.call_count, 0)
        self.assertEqual(mock_load_instance_parameter.call_count, 0)
        self.assertEqual(mock_load_asm_oracle_instance.call_count, 0)
        self.assertEqual(mock_load_engine.call_count, 0)


class LoadDatabaseReplicationSystemTestCase(TestCase):
    """Load database replication system"""

    def setUp(self):
        """Init data"""

        self.data = {
            "database": {
                "db_name": "ORCL",
                "dataguard_database_id": "80f043635a071457c76cffcb956c18ed76f6331d686476fbfa82580b9b4aaaaa",
            }
        }

    def test_load_database_replication_system_1(self):
        """Test load_database_replication_system"""

        replication_system_obj = load_database_replication_system(instance=self.data)
        self.assertIsInstance(replication_system_obj, DatabaseReplicationSystem)


class LoadDatabaseSizeMetricTestCase(TestCase):
    """Load database size metric"""

    def setUp(self):
        """Init data"""

        self.data = {
            "database_size": 1,
            "datafile_size": 2,
            "redo_size": 3,
            "tempfile_size": 4,
        }

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.database_obj = model_builder.container_oracle_database_cdb_obj

    def test_load_database_size_metric_1(self):
        """Test load_database_size_metric"""

        load_database_size_metric(
            snapshot_obj=self.snapshot_obj,
            database_obj=self.database_obj,
            data=self.data,
        )
        metric = DatabaseSizeMetric.objects.get(
            date=timezone.now().replace(minute=0, second=0, microsecond=0),
            database=self.database_obj,
        )
        self.assertIsInstance(metric, DatabaseSizeMetric)

    def test_load_database_size_metric_2(self):
        """Test load_database_size_metric size empty"""

        data = self.data.copy()
        data.pop("database_size")

        load_database_size_metric(
            snapshot_obj=self.snapshot_obj, database_obj=self.database_obj, data=data
        )
        metrics = DatabaseSizeMetric.objects.filter(
            date=timezone.now().replace(minute=0, second=0, microsecond=0),
            database=self.database_obj,
        )
        self.assertEqual(len(metrics), 0)


class LoadContainerOracleFraMetricTestCase(TestCase):
    """Load container oracle fra metric"""

    def setUp(self):
        """Init data"""

        self.data = {"size": 1, "used": 2, "reclaimable": 3}

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.database_obj = model_builder.container_oracle_database_cdb_obj

    def test_load_container_oracle_fra_metric_1(self):
        """Test load_container_oracle_fra_metric"""

        load_container_oracle_fra_metric(
            snapshot_obj=self.snapshot_obj,
            database_obj=self.database_obj,
            data=self.data,
        )

        metric = ContainerOracleFraMetric.objects.get(
            date=timezone.now().replace(minute=0, second=0, microsecond=0),
            database=self.database_obj,
        )
        self.assertIsInstance(metric, ContainerOracleFraMetric)

    def test_load_container_oracle_fra_metric_2(self):
        """Test load_container_oracle_fra_metric size empty"""

        data = self.data.copy()
        data["size"] = ""

        load_container_oracle_fra_metric(
            snapshot_obj=self.snapshot_obj, database_obj=self.database_obj, data=data
        )

        metrics = ContainerOracleFraMetric.objects.filter(
            date=timezone.now().replace(minute=0, second=0, microsecond=0),
            database=self.database_obj,
        )
        self.assertEqual(len(metrics), 0)


class LoadContainerOracleDatabaseTestCase(TestCase):
    """Load Oracle container database"""

    def setUp(self):
        """Init data"""

        self.data = {
            "database": {
                "cluster_id": "0fea50887867f4953c05903ff062306a04ac04179c3216c1e1aea869b81aaaaa",
                "created": "2017/04/25 14:47:09",
                "database_role": "PRIMARY",
                "database_size": "123456789",
                "datafile_size": "123456789",
                "redo_size": "123456789",
                "tempfile_size": "123456789",
                "dataguard_database_id": "80f043635a071457c76cffcb956c18ed76f6331d686476fbfa82580b9b4aaaaa",
                "db_name": "ORCL",
                "db_unique_name": "ORCL",
                "dbid": "12345",
                "flashback_on": "NO",
                "force_logging": "YES",
                "fra": {
                    "reclaimable": "142119796736",
                    "size": "1099511627776",
                    "used": "142121893888",
                },
                "is_cdb": None,
                "is_cluster_database": True,
                "log_mode": "ARCHIVELOG",
                "resetlogs_time": "2017/04/25 14:47:09",
            },
        }

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.replication_system_obj = model_builder.replication_system_obj
        self.instance_obj = model_builder.database_oracle_instance_obj

    def test_load_container_oracle_database_1(self):
        """Test load_container_oracle_database Legacy"""

        self.data["database"]["is_cdb"] = False

        database_obj = load_container_oracle_database(
            snapshot_obj=self.snapshot_obj,
            replication_system_obj=self.replication_system_obj,
            instance_obj=self.instance_obj,
            data=self.data,
        )
        self.assertIsInstance(database_obj, ContainerOracleDatabase)
        self.assertEqual(database_obj.container_type, ContainerOracleDatabase.LEGACY)

    def test_load_container_oracle_database_2(self):
        """Test load_container_oracle_database Legacy"""

        self.data["database"]["is_cdb"] = True

        database_obj = load_container_oracle_database(
            snapshot_obj=self.snapshot_obj,
            replication_system_obj=self.replication_system_obj,
            instance_obj=self.instance_obj,
            data=self.data,
        )
        self.assertIsInstance(database_obj, ContainerOracleDatabase)
        self.assertEqual(database_obj.container_type, ContainerOracleDatabase.CDB)


class LoadPluggableOracleDatabaseTestCase(TestCase):
    """Load pluggable database"""

    def setUp(self):
        """Init data"""

        self.data = {
            "cluster_id": "f38e65c54d52bef2993878c2093254f9d9e07ff599906eea9f89a7b0f7aaaaaa",
            "creation_time": "2020/09/22 17:53:53",
            "database_size": "123456789",
            "datafile_size": "123456789",
            "redo_size": "123456789",
            "tempfile_size": "123456789",
            "dbid": "12345",
            "guid": "f38e65c54d52bef2993878c2093254f9",
            "is_cluster_database": True,
            "name": "PDB1",
            "open_mode": "READ WRITE",
            "open_time": "2022/06/28 18:17:33",
            "recovery_status": "ENABLED",
            "restricted": "NO",
        }

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.instance_obj = model_builder.database_oracle_instance_obj
        self.database_obj = model_builder.container_oracle_database_cdb_obj

    def test_load_pluggable_oracle_database_1(self):
        """Test load_pluggable_oracle_database"""

        database_obj = load_pluggable_oracle_database(
            snapshot_obj=self.snapshot_obj,
            instance_obj=self.instance_obj,
            database_obj=self.database_obj,
            data=self.data,
        )
        self.assertIsInstance(database_obj, PluggableOracleDatabase)


class LoadPdbTestCase(TestCase):
    """Load one PDB"""

    def setUp(self):
        """Init data"""

        self.data = {
            "name": "PDB",
            "tablespaces": ["TBS1", "TBS2"],
            "users": ["USER1", "USER2"],
        }

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.instance_obj = model_builder.database_oracle_instance_obj
        self.database_obj = model_builder.container_oracle_database_cdb_obj

    @patch("core.tasks.load_pluggable_oracle_database", autospec=True)
    @patch("core.tasks.load_database_size_metric", autospec=True)
    @patch("core.tasks.load_tablespace", autospec=True)
    @patch("core.tasks.load_db_user", autospec=True)
    def test_load_pdb_1(
        self,
        mock_load_db_user,
        mock_load_tablespace,
        mock_load_database_size_metric,
        mock_load_pluggable_oracle_database,
    ):
        """Test load_pdb"""

        load_pdb(
            snapshot_obj=self.snapshot_obj,
            instance_obj=self.instance_obj,
            database_obj=self.database_obj,
            data=self.data,
        )
        self.assertEqual(mock_load_db_user.call_count, 2)
        self.assertEqual(mock_load_tablespace.call_count, 2)
        self.assertEqual(mock_load_database_size_metric.call_count, 1)
        self.assertEqual(mock_load_pluggable_oracle_database.call_count, 1)


class LoadPdbsTEstCase(TestCase):
    """Load pdbs"""

    def setUp(self):
        """Init data"""

        self.data = {"pdbs": ["PDB1", "PDB2"]}

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.instance_obj = model_builder.database_oracle_instance_obj
        self.database_obj = model_builder.container_oracle_database_cdb_obj

    @patch("core.tasks.load_pdb", autospec=True)
    def test_load_pdbs_1(self, mock_load_pdb):
        """Test load_pdbs"""

        load_pdbs(
            snapshot_obj=self.snapshot_obj,
            instance_obj=self.instance_obj,
            database_obj=self.database_obj,
            data=self.data,
        )
        self.assertEqual(mock_load_pdb.call_count, 2)


class LoadDatabaseTestCase(TestCase):
    """Load database"""

    def setUp(self):
        """Init data"""

        self.data = {
            "database": {
                "db_unique_name": "ORCL",
                "tablespaces": ["TBS1", "TBS2"],
                "fra": "",
                "users": ["USER1", "USER2"],
            }
        }

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.instance_obj = model_builder.database_oracle_instance_obj

    @patch("core.tasks.load_database_replication_system", autospec=True)
    @patch("core.tasks.load_container_oracle_database", autospec=True)
    @patch("core.tasks.load_database_size_metric", autospec=True)
    @patch("core.tasks.load_container_oracle_fra_metric", autospec=True)
    @patch("core.tasks.load_tablespace", autospec=True)
    @patch("core.tasks.load_db_user", autospec=True)
    @patch("core.tasks.load_pdbs", autospec=True)
    def test_load_database_1(
        self,
        mock_load_pdbs,
        mock_load_db_user,
        mock_load_tablespace,
        mock_load_container_oracle_fra_metric,
        mock_load_database_size_metric,
        mock_load_container_oracle_database,
        mock_load_database_replication_system,
    ):
        """Test load_database"""

        load_database(
            snapshot_obj=self.snapshot_obj,
            instance_obj=self.instance_obj,
            data=self.data,
        )
        self.assertEqual(mock_load_pdbs.call_count, 1)
        self.assertEqual(mock_load_db_user.call_count, 2)
        self.assertEqual(mock_load_tablespace.call_count, 2)
        self.assertEqual(mock_load_container_oracle_fra_metric.call_count, 1)
        self.assertEqual(mock_load_database_size_metric.call_count, 1)
        self.assertEqual(mock_load_container_oracle_database.call_count, 1)
        self.assertEqual(mock_load_database_replication_system.call_count, 1)

    @patch("core.tasks.load_database_replication_system", autospec=True)
    @patch("core.tasks.load_container_oracle_database", autospec=True)
    @patch("core.tasks.load_database_size_metric", autospec=True)
    @patch("core.tasks.load_container_oracle_fra_metric", autospec=True)
    @patch("core.tasks.load_tablespace", autospec=True)
    @patch("core.tasks.load_db_user", autospec=True)
    @patch("core.tasks.load_pdbs", autospec=True)
    def test_load_database_2(
        self,
        mock_load_pdbs,
        mock_load_db_user,
        mock_load_tablespace,
        mock_load_container_oracle_fra_metric,
        mock_load_database_size_metric,
        mock_load_container_oracle_database,
        mock_load_database_replication_system,
    ):
        """Test load_database"""

        data = {}

        load_database(
            snapshot_obj=self.snapshot_obj, instance_obj=self.instance_obj, data=data
        )
        self.assertEqual(mock_load_pdbs.call_count, 0)
        self.assertEqual(mock_load_db_user.call_count, 0)
        self.assertEqual(mock_load_tablespace.call_count, 0)
        self.assertEqual(mock_load_container_oracle_fra_metric.call_count, 0)
        self.assertEqual(mock_load_database_size_metric.call_count, 0)
        self.assertEqual(mock_load_container_oracle_database.call_count, 0)
        self.assertEqual(mock_load_database_replication_system.call_count, 0)


class LoadInstanceRdbmsTestCase(TestCase):
    """Load instance rdbms"""

    def setUp(self):
        """Init data"""

        self.data = {
            "databases": {
                "oracle": {"instances": {"rdbms": ["INSTANCE1", "INSTANCE2"]}}
            }
        }

        model_builder = ModelBuilder()
        self.snapshot_obj = model_builder.core_model_builder.snapshot_obj
        self.server_obj = model_builder.system_model_builder.server_obj
        self.vendor_obj = model_builder.core_model_builder.vendor_obj

    @patch("core.tasks.load_engine", autospec=True)
    @patch("core.tasks.load_database_oracle_instance", autospec=True)
    @patch("core.tasks.load_instance_parameter", autospec=True)
    @patch("core.tasks.load_database", autospec=True)
    def test_load_instance_rdbms_1(
        self,
        mock_load_database,
        mock_load_instance_parameter,
        mock_load_database_oracle_instance,
        mock_load_engine,
    ):
        """Test load_instance_rdbms"""

        load_instance_rdbms(
            snapshot_obj=self.snapshot_obj,
            server_obj=self.server_obj,
            vendor_obj=self.vendor_obj,
            data=self.data,
        )
        self.assertEqual(mock_load_database.call_count, 2)
        self.assertEqual(mock_load_instance_parameter.call_count, 2)
        self.assertEqual(mock_load_database_oracle_instance.call_count, 2)
        self.assertEqual(mock_load_engine.call_count, 2)


class LoadListenersTestCase(TestCase):
    """Load listeners"""

    def setUp(self):
        """Init data"""

        model_builder = ModelBuilder()
        self.cluster_obj = model_builder.system_model_builder.cluster_obj
        self.server_obj = model_builder.system_model_builder.server_obj

        self.scan_name = "LISTENER_SCAN"
        self.local_name = "LISTENER"
        self.engine_obj = model_builder.engine_obj

        self.data = {
            "databases": {
                "oracle": {
                    "listeners": [
                        {
                            "endpoints": [
                                {"hostname": "myscan.domain", "port": "1521"}
                            ],
                            "name": self.scan_name,
                            "oracle_home": self.engine_obj.path,
                            "services": [
                                {"instances": ["ORCL"], "name": "myservice1.domain"},
                                {"instances": ["ORCL"], "name": "PDB1.domain"},
                            ],
                            "type": "scan",
                        },
                        {
                            "endpoints": [
                                {"hostname": "myserver.domain", "port": "1521"},
                                {"hostname": "myserver-vip.domain", "port": "1521"},
                            ],
                            "name": self.local_name,
                            "oracle_home": self.engine_obj.path,
                            "services": [
                                {"instances": ["ORCL"], "name": "myservice1.domain"},
                                {"instances": ["ORCL"], "name": "PDB1.domain"},
                            ],
                            "type": "local",
                        },
                        {
                            "endpoints": [
                                {"hostname": "myserver.domain", "port": "1521"},
                                {"hostname": "myserver-vip.domain", "port": "1521"},
                            ],
                            "name": "LISTENER",
                            "oracle_home": self.engine_obj.path,
                            "services": [
                                {"instances": ["ORCL"], "name": "myservice1.domain"},
                                {"instances": ["ORCL"], "name": "PDB1.domain"},
                            ],
                            "type": "dummy",
                        },
                    ],
                }
            }
        }

    def test_load_listeners_1(self):
        """Test load_listeners"""

        load_listeners(
            cluster_obj=self.cluster_obj, server_obj=self.server_obj, data=self.data
        )

        scan_listener = ScanListener.objects.get(
            name=self.scan_name,
            engine=self.engine_obj,
            cluster=self.cluster_obj,
        )
        self.assertIsInstance(scan_listener, ScanListener)

        local_listener = LocalListener.objects.get(
            name=self.local_name,
            engine=self.engine_obj,
            server=self.server_obj,
        )
        self.assertIsInstance(local_listener, LocalListener)


class LoadServicesTestCase(TestCase):
    """Load services"""

    def setUp(self):
        """Init data"""

        model_builder = ModelBuilder()
        self.cluster_obj = model_builder.system_model_builder.cluster_obj
        self.server_obj = model_builder.system_model_builder.server_obj

        self.container_database_obj = model_builder.container_oracle_database_cdb_obj
        self.pdb_database_obj = model_builder.pluggable_oracle_database_obj
        self.engine_obj = model_builder.engine_obj
        self.scan_listener_obj = model_builder.scan_oracle_listener_obj
        self.local_listener_obj = model_builder.local_oracle_listener_obj

        self.data = {
            "databases": {
                "oracle": {
                    "is_cluster": True,
                    "cluster_name": "test-cluster",
                    "instances": {
                        "rdbms": [
                            {
                                "database": {},
                                "oracle_home": "/u01/oracle/19.0.0.0/dbhome_1",
                                "parameters": [],
                                "pdbs": [],
                                "sid": "ORCL1",
                                "version": "19.6.0.0.200114",
                            },
                            {
                                "database": {
                                    "cluster_id": "f38e65c54d52bef2993878c2093254f907ff599906eea9f89a7b0f7aaaaaa",
                                    "created": "2017/04/25 14:47:09",
                                    "database_role": "PRIMARY",
                                    "database_size": "123456789",
                                    "datafile_size": "123456789",
                                    "redo_size": "123456789",
                                    "tempfile_size": "123456789",
                                    "dataguard_database_id": "80f043635a071457c76cffcb956c11d686476fbfa82580b9b4aaaaa",
                                    "db_name": "ORCL",
                                    "db_unique_name": self.container_database_obj.name,
                                    "dbid": "12345",
                                    "flashback_on": "NO",
                                    "force_logging": "YES",
                                    "fra": {
                                        "reclaimable": "142119796736",
                                        "size": "1099511627776",
                                        "used": "142121893888",
                                    },
                                    "is_cdb": False,
                                    "is_cluster_database": True,
                                    "log_mode": "ARCHIVELOG",
                                    "parameters": [
                                        {
                                            "name": "long_module_action",
                                            "value": "FALSE",
                                        },
                                        {"name": "sga_max_size", "value": "20G"},
                                        {"name": "use_large_pages", "value": "TRUE"},
                                        {"name": "pre_page_sga", "value": "FALSE"},
                                        {"name": "streams_pool_size", "value": "256M"},
                                        {"name": "_kghdsidx_count", "value": "2"},
                                        {
                                            "name": "nls_length_semantics",
                                            "value": "CHAR",
                                        },
                                        {
                                            "name": "_disk_sector_size_override",
                                            "value": "TRUE",
                                        },
                                        {"name": "sga_target", "value": "20G"},
                                        {"name": "db_block_size", "value": "8192"},
                                        {"name": "compatible", "value": "19.0.0.0"},
                                        {
                                            "name": "log_archive_dest_1",
                                            "value": "location=USE_DB_RECOVERY_FILE_DEST",
                                        },
                                        {
                                            "name": "log_archive_dest_state_1",
                                            "value": "ENABLE",
                                        },
                                        {"name": "db_domain", "value": "domain"},
                                    ],
                                    "resetlogs_time": "2017/04/25 14:47:09",
                                    "services": [
                                        {
                                            "name": "myservice1",
                                            "network_name": "myservice1",
                                        },
                                        {"name": "myservice1", "network_name": ""},
                                    ],
                                },
                                "oracle_home": "/u01/oracle/product/19.0.0.0/dbhome_1",
                                "pdbs": [
                                    {
                                        "cluster_id": "f38e65c54d52bef2993878c2093254f907ff599906eea9f89a7b0f7aaaaaa",
                                        "creation_time": "2020/09/22 17:53:53",
                                        "database_size": "123456789",
                                        "datafile_size": "123456789",
                                        "redo_size": "123456789",
                                        "tempfile_size": "123456789",
                                        "dbid": "12345",
                                        "guid": "f38e65c54d52bef2993878c2093254f9",
                                        "is_cluster_database": True,
                                        "name": self.pdb_database_obj.name,
                                        "open_mode": "READ WRITE",
                                        "open_time": "2022/06/28 18:17:33",
                                        "recovery_status": "ENABLED",
                                        "restricted": "NO",
                                        "services": [
                                            {"name": "PDB1", "network_name": "PDB1"}
                                        ],
                                    }
                                ],
                                "sid": "ORCL",
                                "version": "19.6.0.0.200114",
                            },
                        ]
                    },
                    "listeners": [
                        {
                            "endpoints": [
                                {"hostname": "myscan.domain", "port": "1521"}
                            ],
                            "name": self.scan_listener_obj.name,
                            "oracle_home": self.engine_obj.path,
                            "services": [
                                {
                                    "instances": ["ORCL"],
                                    "name": f"{self.container_database_obj.name}.domain",
                                },
                                {"instances": ["ORCL"], "name": "PDB1.domain"},
                            ],
                            "type": "scan",
                        },
                        {
                            "endpoints": [
                                {"hostname": "myserver.domain", "port": "1521"},
                                {"hostname": "myserver-vip.domain", "port": "1521"},
                            ],
                            "name": self.local_listener_obj.name,
                            "oracle_home": self.engine_obj.path,
                            "services": [
                                {
                                    "instances": ["ORCL"],
                                    "name": f"{self.container_database_obj.name}.domain",
                                },
                                {"instances": ["ORCL"], "name": "PDB1.domain"},
                            ],
                            "type": "local",
                        },
                    ],
                    "skipped_instances": [],
                }
            },
            "server_site": "site1",
        }

        load_listeners(
            cluster_obj=self.cluster_obj, server_obj=self.server_obj, data=self.data
        )

    def test_load_services_1(self):
        """Test load_services"""

        load_services(
            cluster_obj=self.cluster_obj, server_obj=self.server_obj, data=self.data
        )

    def test_load_services_2(self):
        """Test load_services without db_domain"""

        data = self.data.copy()
        data["databases"]["oracle"]["instances"]["rdbms"][1]["database"][
            "parameters"
        ] = [{"name": "db_domain", "value": ""}]
        load_services(
            cluster_obj=self.cluster_obj, server_obj=self.server_obj, data=self.data
        )


class LoadFactsTestCase(TestCase):
    """Load facts"""

    def setUp(self):
        """Init data"""

        model_builder = ModelBuilder()
        self.server_fqdn = model_builder.system_model_builder.server_name
        self.snapshot_id = model_builder.core_model_builder.snapshot_obj.id

    @patch("core.tasks.load_cluster", autospec=True)
    @patch("core.tasks.load_server", autospec=True)
    @patch("core.tasks.load_filesystems", autospec=True)
    @patch("core.tasks.load_devices", autospec=True)
    @patch("core.tasks.load_network_interfaces", autospec=True)
    @patch("core.tasks.load_vendor", autospec=True)
    @patch("core.tasks.load_instance_asm", autospec=True)
    @patch("core.tasks.load_instance_rdbms", autospec=True)
    @patch("core.tasks.load_listeners", autospec=True)
    @patch("core.tasks.load_services", autospec=True)
    def test_load_facts_1(
        self,
        unused_mock_load_services,
        unused_mock_load_listeners,
        unused_mock_load_instance_rdbms,
        unused_mock_load_instance_asm,
        unused_mock_load_vendor,
        unused_mock_load_network_interfaces,
        unused_mock_load_devices,
        unused_mock_load_filesystems,
        unused_mock_load_server,
        unused_mock_load_cluster,
    ):
        """Test load_facts"""

        load_facts(server_fqdn=self.server_fqdn, snapshot_id=self.snapshot_id)

    @patch("core.tasks.load_cluster", autospec=True)
    def test_load_facts_2(self, mock_load_cluster):
        """Test load_facts error"""

        mock_load_cluster.side_effect = Exception
        with self.assertRaises(Exception):
            load_facts(server_fqdn=self.server_fqdn, snapshot_id=self.snapshot_id)
