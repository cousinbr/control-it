"""Core app utils.secret Test"""
from django.test import TestCase

from core.utils.secret import SecretEngine


class SecretEngineTestCase(TestCase):
    """SecretEngine test"""

    def test_is_static(self):
        """Test that SecretEngine could not be instancied"""
        with self.assertRaises(TypeError):
            SecretEngine()
