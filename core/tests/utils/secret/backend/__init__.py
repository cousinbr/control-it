"""Core app utils.secret.backend Test"""
from django.test import TestCase

from core.utils.secret.backend import DummySecretBackend


class DummySecretBackendTestCase(TestCase):
    """DummySecretBackend test"""

    def test_get_password(self):
        """Test get engine"""
        backend = DummySecretBackend()

        secret = backend.get_secret(key="key")
        self.assertEqual(secret, "secret")

        secret = backend.get_secret(key="mickey")
        self.assertEqual(secret, "")
