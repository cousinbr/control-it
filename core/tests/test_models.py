"""Core app models Test"""

from django.test import TestCase

from core.models import Environment, Vendor
from core.tests import ModelBuilder


class EnvironmentTestCase(TestCase):
    """Environment model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str(self):
        """Environment string representation"""
        env = Environment.objects.get(name=self.model_builder.environment_name)
        self.assertEqual(
            str(env),
            f"{self.model_builder.environment_name} ({self.model_builder.environment_code})",
        )


class VendorTestCase(TestCase):
    """Vendor model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str(self):
        """Vendor string representation"""
        vendor = Vendor.objects.get(name=self.model_builder.vendor_name)
        self.assertEqual(str(vendor), f"{self.model_builder.vendor_name}")
