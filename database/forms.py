"""System forms"""

from django.forms import ModelForm

from database.models.tns import FailoverNetworkAlias, StandaloneNetworkAlias


class StandaloneNetworkAliasForm(ModelForm):
    """StandaloneNetworkAlias form"""

    class Meta:
        model = StandaloneNetworkAlias
        fields = "__all__"

    def __init__(self, *args, service: int = None, **kwargs):
        super().__init__(*args, **kwargs)
        self.initial['service'] = service

class FailoverNetworkAliasForm(ModelForm):
    """FailoverNetworkAlias form"""

    class Meta:
        model = FailoverNetworkAlias
        fields = "__all__"

    def __init__(self, *args, service: int = None, **kwargs):
        super().__init__(*args, **kwargs)
        self.initial['service'] = service
