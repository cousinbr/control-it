"""Database tasks"""

import logging

import oracledb
from oracledb.connection import Connection

from celery import shared_task

from django.utils import timezone
from django.utils.timezone import make_aware

from controlit import settings
from core.models import Snapshot
from database.models.database import DatabaseSizeMetric
from database.models.database.oracle import OracleDatabase, ContainerOracleDatabase, PluggableOracleDatabase
from database.models.diskgroup.oracle import OracleDiskgroup, OracleDiskgroupSizeMetric
from database.models.instance.oracle import AsmOracleInstance
from database.models.tablespace import TablespaceSpaceMetric
from database.models.tablespace.oracle import OracleTablespace
from system.models import FilesystemSizeMetric, Server

logger = logging.getLogger(__name__)
BATCH_SIZE = 100

def get_database_target_guid(connection: Connection, database: OracleDatabase) -> str:
    """Get OEM target_guid for this database"""

    target_guid = None

    if isinstance(database, ContainerOracleDatabase):

        # Non PDB RAC database
        if database.is_cluster_database:
            logger.debug("Retreive \"Non PDB RAC database\" data from OEM")
            with connection.cursor() as cursor:
                sql = """select
                            target_guid
                        from
                            mgmt$target target
                        where
                            target_type = 'rac_database'
                            and lower(target_name) like lower(:database_name)"""
                rows = cursor.execute(sql, database_name=f"{database.name}%")

                for row in rows:
                    logger.debug("%s", row)
                    target_guid = row[0]

        # Non PDB non RAC database
        elif not database.is_cluster_database:
            logger.debug("Retreive \"Non PDB non RAC database\" data from OEM")
            with connection.cursor() as cursor:
                sql = """select
                            target.target_guid
                        from
                            mgmt$target target
                        where
                            target_type = 'oracle_database'
                            and type_qualifier3 = 'DB'
                            and lower(target_name) like lower(:database_name)"""
                rows = cursor.execute(sql, database_name=f"{database.name}%")

                for row in rows:
                    target_guid = row[0]

    elif isinstance(database, PluggableOracleDatabase):

        # PDB database
        logger.debug("Retreive \"PDB database\" data from OEM")
        with connection.cursor() as cursor:
            sql = """select
                        target_guid
                    from
                        mgmt$target
                    where
                        target_type = 'oracle_pdb'
                        and lower(target_name) like lower(:database_name)"""
            rows = cursor.execute(sql, database_name=f"{database.container.name}_%{database.name}")

            for row in rows:
                target_guid = row[0]

    # Raise exeception if target not found
    if not target_guid:
        raise Exception('OEM Target GUID not found')

    return target_guid


def get_asm_target_guid(connection: Connection, instance: AsmOracleInstance) -> str:
    """Get OEM target_guid for this instance"""

    target_guid = None

    # If this is a cluster member
    if instance.server.cluster:
        target_name = f"+ASM_{instance.server.cluster.name}%"
    else:
        target_name = f"+ASM_{instance.server.name}%"

    logger.debug("Retreive ASM target_guid from OEM for %s", target_name)
    with connection.cursor() as cursor:
        sql = """select
                    target_guid
                from
                    mgmt$target
                where
                    target_type in ('osm_cluster', 'osm_instance')
                    and type_qualifier2 = 'ASM'
                    and lower(target_name) like lower(:target_name)"""
        rows = cursor.execute(sql, target_name=target_name)

        for row in rows:
            logger.debug("%s", row)
            target_guid = row[0]

    # Raise exeception if target not found
    if not target_guid:
        raise Exception('OEM Target GUID not found')

    return target_guid


def get_host_target_guid(connection: Connection, server: Server) -> str:
    """Get OEM target_guid for this instance"""

    target_guid = None

    logger.debug("Retreive host target_guid from OEM for %s", server.name)
    with connection.cursor() as cursor:
        sql = """select
                    target_guid
                from
                    mgmt$target
                where
                    target_type = 'host'
                    and lower(target_name) like lower(:target_name)"""
        rows = cursor.execute(sql, target_name=server.name)

        for row in rows:
            logger.debug("%s", row)
            target_guid = row[0]

    # Raise exeception if target not found
    if not target_guid:
        raise Exception('OEM Target GUID not found')

    return target_guid


def load_filesystem_size_history(connection: Connection, server: Server):
    """Load tablespace size from OEM"""
    logger.info("Load tablespace size history for %s", server.name)

    target_guid = get_host_target_guid(connection, server)
    logger.debug("target_guid=%s", target_guid)

    # Create snapshot
    snapshot = Snapshot.objects.create(
        payload='{"OEM data load for '+server.name+'"}'
    )

    # Iterate over server filesystems
    for filesystem in server.filesystem_set.all():

        logger.debug("Load filesystem size history for %s:%s", server.name, filesystem.mount_point)

        # Retreive older metric in inventory
        older_metric = filesystem.filesystemsizemetric_set.all().first()
        if older_metric:
            older_metric_date = older_metric.date
        else:
            older_metric_date = timezone.now()

        logger.debug("Retreive data from OEM older than %s", older_metric_date)
        with connection.cursor() as cursor:
            sql = """select
                        key_value,
                        rollup_timestamp,
                        metric_column,
                        average * 1024 * 1024
                    from
                        mgmt$metric_daily
                    where
                        target_guid = :target_guid 
                        and metric_name='Filesystems'
                        and key_value = :mount_point
                        and rollup_timestamp < :rollup_timestamp
                    order by
                        rollup_timestamp,
                        metric_column"""
            cursor.execute(sql,
                target_guid=target_guid,
                rollup_timestamp=older_metric_date,
                mount_point=filesystem.mount_point
            )

            # Init variable
            space_available = None
            space_size = None

            while True:
                rows = cursor.fetchmany(BATCH_SIZE)
                logger.debug("%s rows fetched from OEM", len(rows))
                objs = []
                if not rows:
                    break

                logger.debug("Creating models objects")
                for row in rows:
                    logger.debug(row)
                    if row[2] == "available":
                        space_available = row[3]
                        continue
                    elif row[2] == "size":
                        space_size = row[3]

                        metric = FilesystemSizeMetric(
                            size=int(space_size),
                            used=int(space_size - space_available),
                            date=make_aware(row[1]),
                            filesystem=filesystem,
                            snapshot=snapshot,
                            day=snapshot.day)

                        objs.append(metric)
                        space_available = None
                        space_size = None

                logger.debug("%d objects to insert", len(objs))
                FilesystemSizeMetric.objects.bulk_create(objs)


def load_database_size_history(connection: Connection, database: OracleDatabase):
    """Load database size from OEM"""
    logger.info("Load database size history for %s", database.name)

    target_guid = get_database_target_guid(connection, database)
    logger.debug("target_guid=%s", target_guid)

    # Retreive older metric in inventory
    older_metric = database.databasesizemetric_set.first()
    if older_metric:
        older_metric_date = older_metric.date
    else:
        older_metric_date = timezone.now()

    logger.info("Retreive data from OEM")
    with connection.cursor() as cursor:
        sql = """select
                    metric_dbsize.rollup_timestamp,
                    metric_dbsize.average * 1024 * 1024 * 1024 dbsize,
                    metric_tempsize.average * 1024 * 1024 tempsize,
                    redo_size.logsize
                from
                    mgmt$metric_daily metric_dbsize
                join
                    mgmt$metric_daily metric_tempsize
                    on metric_dbsize.target_guid = metric_tempsize.target_guid
                        and metric_dbsize.rollup_timestamp = metric_tempsize.rollup_timestamp
                        and metric_tempsize.metric_name='tbspAllocation'
                        and metric_tempsize.metric_column = 'spaceAllocated'
                        and metric_tempsize.key_value = 'TEMP'
                cross join
                    (select
                        sum(logsize) logsize
                    from
                        mgmt$db_redologs
                    where
                        target_guid = :target_guid) redo_size
                where
                    metric_dbsize.target_guid = :target_guid
                    and metric_dbsize.metric_name = 'DATABASE_SIZE'
                    and metric_dbsize.metric_column = 'ALLOCATED_GB'
                    and metric_dbsize.rollup_timestamp < :rollup_timestamp
                order by metric_dbsize.rollup_timestamp"""

        cursor.execute(sql, target_guid=target_guid, rollup_timestamp=older_metric_date)

        # Create snapshot
        snapshot = Snapshot.objects.create(
            payload='{"OEM data load for '+database.name+'"}'
        )

        while True:
            rows = cursor.fetchmany(BATCH_SIZE)
            logger.debug("%d rows fetched from OEM", len(rows))
            objs = []
            if not rows:
                break

            logger.debug("Creating models objects")
            for row in rows:

                metric = DatabaseSizeMetric(
                    size=int(row[1]),
                    datafile_size=int(row[1])-int(row[2]),
                    redo_size=int(row[3]),
                    tempfile_size=int(row[2]),
                    date=make_aware(row[0]),
                    database=database,
                    snapshot=snapshot,
                    day=snapshot.day)

                objs.append(metric)

            logger.debug("%d objects to insert", len(objs))
            DatabaseSizeMetric.objects.bulk_create(objs)


def load_tablespace_size_history(connection: Connection, database: OracleDatabase):
    """Load tablespace size from OEM"""
    logger.info("Load tablespace size history for %s", database.name)

    target_guid = get_database_target_guid(connection, database)
    logger.debug("target_guid=%s", target_guid)

    # Create snapshot
    snapshot = Snapshot.objects.create(
        payload='{"OEM data load for '+database.name+'"}'
    )

    # Iterate over database tablespaces
    for tablespace in OracleTablespace.objects.filter(database_id=database.id).all():

        logger.debug("Load tablespace size history for %s:%s", database.name, tablespace.name)

        # Retreive older metric in inventory
        older_metric = tablespace.tablespacespacemetric_set.all().first()
        if older_metric:
            older_metric_date = older_metric.date
        else:
            older_metric_date = timezone.now()

        logger.debug("Retreive data from OEM older than %s", older_metric_date)
        with connection.cursor() as cursor:
            sql = """select
                        key_value,
                        rollup_timestamp,
                        metric_column,
                        average * 1024 * 1024
                    from
                        mgmt$metric_daily
                    where
                        target_guid = :target_guid 
                        and metric_name='tbspAllocation'
                        and key_value = :tablespace_name
                        and rollup_timestamp < :rollup_timestamp
                    order by
                        rollup_timestamp,
                        metric_column"""
            cursor.execute(sql,
                target_guid=target_guid,
                rollup_timestamp=older_metric_date,
                tablespace_name=tablespace.name
            )

            # Init variable
            space_allocated = None
            space_used = None

            while True:
                rows = cursor.fetchmany(BATCH_SIZE)
                logger.debug("%d rows fetched from OEM", len(rows))
                objs = []
                if not rows:
                    break

                logger.debug("Creating models objects")
                for row in rows:
                    logger.debug(row)
                    if row[2] == "spaceAllocated":
                        space_allocated = row[3]
                        continue
                    elif row[2] == "spaceUsed":
                        space_used = row[3]

                        metric = TablespaceSpaceMetric(
                            size=int(space_allocated),
                            used=int(space_used),
                            max_size=0,
                            date=make_aware(row[1]),
                            tablespace=tablespace,
                            snapshot=snapshot,
                            day=snapshot.day)

                        objs.append(metric)
                        space_allocated = None
                        space_used = None

                logger.debug("%d objects to insert", len(objs))
                TablespaceSpaceMetric.objects.bulk_create(objs)


def load_diskgroup_size_history(connection: Connection, instance: AsmOracleInstance):
    """Load diskgroup space history for one asm instance"""

    logger.info("Load diskgroup size history for %s", instance)

    target_guid = get_asm_target_guid(connection, instance)
    logger.debug("target_guid=%s", target_guid)

    # Retreive older metric in inventory
    diskgroup = instance.oraclediskgroup_set.first()
    older_metric = diskgroup.oraclediskgroupsizemetric_set.first()
    if older_metric:
        older_metric_date = older_metric.date
    else:
        older_metric_date = timezone.now()

    logger.debug("Retreive data from OEM older than %s", older_metric_date)
    with connection.cursor() as cursor:
        sql = """select
                    dg.name,
                    total.rollup_timestamp,
                    round(total.average) total,
                    round(total.average - free.average) used,
                    round(usable.average) average
                from (
                    select
                        target1.target_guid,
                        metric1.key_value name
                    from
                        mgmt$target target1
                    join
                        mgmt$metric_current metric1
                        on target1.target_guid = metric1.target_guid and metric1.metric_name = 'Database_DiskGroup_Usage'
                    where
                        target1.target_type in ('osm_cluster', 'osm_instance')
                        and target1.type_qualifier2 = 'ASM'
                        and target1.target_guid = :target_guid
                    group by
                        target1.target_guid,
                        metric1.key_value) dg
                left join
                    mgmt$metric_daily total
                    on dg.target_guid = total.target_guid 
                    and dg.name = total.key_value 
                    and total.metric_name = 'DiskGroup_Usage' 
                    and total.metric_column = 'total_mb'
                left join
                    mgmt$metric_daily free
                    on dg.target_guid = free.target_guid 
                    and dg.name = free.key_value 
                    and total.rollup_timestamp = free.rollup_timestamp 
                    and free.metric_name = 'DiskGroup_Usage' 
                    and free.metric_column = 'free_mb'
                left join
                    mgmt$metric_daily usable
                    on dg.target_guid = usable.target_guid 
                    and dg.name = usable.key_value 
                    and total.rollup_timestamp = usable.rollup_timestamp 
                    and usable.metric_name = 'DiskGroup_Usage' 
                    and usable.metric_column = 'usable_total_mb'
                where
                    total.rollup_timestamp < :rollup_timestamp
                order by total.rollup_timestamp"""
        cursor.execute(sql, target_guid=target_guid, rollup_timestamp=older_metric_date)

        # Create snapshot
        snapshot = Snapshot.objects.create(
            payload='{"OEM data load for '+instance.name+'"}'
        )

        # Diskgroup dict to cache diskgroup object
        diskgroups = {}

        while True:
            rows = cursor.fetchmany(BATCH_SIZE)
            logger.debug("%d rows fetched from OEM", len(rows))
            objs = []
            if not rows:
                break

            logger.debug("Creating models objects")
            for row in rows:

                # Retrieve diskgroup object
                if row[0] not in diskgroups:
                    diskgroups[row[0]] = OracleDiskgroup.objects.filter(name=row[0], instances=instance).first()

                metric = OracleDiskgroupSizeMetric(
                    total_mb=int(row[2]),
                    used_mb=int(row[3]),
                    date=make_aware(row[1]),
                    diskgroup=diskgroups[row[0]],
                    snapshot=snapshot,
                    day=snapshot.day)

                objs.append(metric)

            logger.debug("%d objects to insert", len(objs))
            OracleDiskgroupSizeMetric.objects.bulk_create(objs)


@shared_task
def load_server_space_history_from_oem(server_id: int):
    """Load space history for one server from OEM"""

    server = Server.objects.get(pk=server_id)

    # Connect to OEM
    with oracledb.connect(
        user=settings.OEM_CONFIG['user'],
        password=settings.OEM_CONFIG['password'],
        dsn=settings.OEM_CONFIG['connect_string']) as connection:

        load_filesystem_size_history(connection=connection, server=server)


@shared_task
def load_database_space_history_from_oem(database_id: int):
    """Load space history for one database from OEM"""

    database = OracleDatabase.objects.get(pk=database_id)

    if database.database_type == OracleDatabase.CDB:
        database = ContainerOracleDatabase.objects.get(pk=database_id)
    else:
        database = PluggableOracleDatabase.objects.get(pk=database_id)

    # Connect to OEM
    with oracledb.connect(
        user=settings.OEM_CONFIG['user'],
        password=settings.OEM_CONFIG['password'],
        dsn=settings.OEM_CONFIG['connect_string']) as connection:

        load_database_size_history(connection=connection, database=database)
        load_tablespace_size_history(connection=connection, database=database)


@shared_task
def load_asm_space_history_from_oem(instance_id: int):
    """Load space history for one ASM instance from OEM"""

    instance = AsmOracleInstance.objects.get(pk=instance_id)

    # Connect to OEM
    with oracledb.connect(
        user=settings.OEM_CONFIG['user'],
        password=settings.OEM_CONFIG['password'],
        dsn=settings.OEM_CONFIG['connect_string']) as connection:

        load_diskgroup_size_history(connection=connection, instance=instance)


def load_all_history_from_oem():
    """Load all history from OEM"""

    # Databases
    for database in OracleDatabase.objects.all():
        load_database_space_history_from_oem.delay(database_id=database.pk)

    # ASM
    for instance in AsmOracleInstance.objects.all():
        load_asm_space_history_from_oem.delay(instance_id=instance.pk)

    # Server
    for server in Server.objects.all():
        load_server_space_history_from_oem.delay(server_id=server.pk)
