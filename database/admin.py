"""Database module Admin"""
from django.contrib import admin

from database.models.database import Database
from database.models.database.oracle import ContainerOracleDatabase
from database.models.instance import Instance

admin.site.register(Database)
admin.site.register(ContainerOracleDatabase)
admin.site.register(Instance)
