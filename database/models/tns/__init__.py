"""Tns base models"""

from django.db import models
from django.urls import reverse

from database.models.database import Database, DatabaseReplicationSystem
from database.models.database.oracle import ContainerOracleDatabase, OracleDatabase
from database.models.engine import Engine
from database.models.instance import InstanceParameterMetric
from system.models import Cluster, Server


class Listener(models.Model):
    """Listener model"""

    LOCAL = "LOCAL"
    SCAN = "SCAN"

    ORACLE_LISTENER_TYPE = [
        (LOCAL, "Local Listener"),
        (SCAN, "Scan Listener"),
    ]

    name = models.CharField("Name", max_length=30)
    type = models.CharField(max_length=5, choices=ORACLE_LISTENER_TYPE)
    engine = models.ForeignKey(Engine, on_delete=models.CASCADE)

    class Meta:
        db_table = "database_listener"

    def __str__(self):
        """Return string representation"""
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""

        if self.type == self.LOCAL:
            return self.locallistener.get_absolute_url()  # pylint: disable=no-member
        else:
            return self.scanlistener.get_absolute_url()  # pylint: disable=no-member


class LocalListener(Listener):
    """LocalListener model"""

    server = models.ForeignKey(Server, on_delete=models.CASCADE)

    class Meta:
        db_table = "database_listener_local"

    def save(self, *args, **kwargs):
        self.type = Listener.LOCAL
        super().save(*args, **kwargs)

    def __str__(self):
        """Return string representation"""
        return f"{self.name} (Local Listener)"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse("database:listener-local-detail", kwargs={"pk": self.pk})


class ScanListener(Listener):
    """ScanListener model"""

    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE)

    class Meta:
        db_table = "database_listener_scan"

    def save(self, *args, **kwargs):
        self.type = Listener.SCAN
        super().save(*args, **kwargs)

    def __str__(self):
        """Return string representation"""
        return f"{self.name} (SCAN Listener)"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse("database:listener-scan-detail", kwargs={"pk": self.pk})


class Endpoint(models.Model):
    """Endpoint model"""

    hostname = models.CharField("Hostname", max_length=255)
    port = models.PositiveSmallIntegerField("Port")

    listener = models.ForeignKey(Listener, on_delete=models.CASCADE)

    class Meta:
        db_table = "database_endpoint"


# pylint: disable=R0903
class ServiceManager(models.Manager):
    """Service manager"""

    def search(self, query=None):
        """Search for service"""

        qs = self.get_queryset()
        if query is not None:
            or_lookup = models.Q(name__icontains=query)
            # distinct() is often necessary with Q lookups
            qs = qs.filter(or_lookup).order_by("name")
        return qs


class FailoverService(models.Model):
    """Failover Service Model"""

    name = models.CharField("Name", max_length=512)

    replication_system = models.ForeignKey(
        DatabaseReplicationSystem, on_delete=models.CASCADE
    )

    objects = ServiceManager()

    class Meta:
        db_table = "database_service_failover"
        ordering = ["name"]
        constraints = [
            models.UniqueConstraint(
                fields=["name", "replication_system"],
                name="databaseservicefailover_set_unique",
            )
        ]

    def __str__(self):
        """Return string representation"""
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse("database:failover-service-detail", kwargs={"pk": self.pk})

    @property
    def name_without_domain(self):
        """Return service name without db_domain"""
        return self.name.split(".")[0]

    @property
    def tns_name(self):
        """Unique tns name for this service"""
        sep = "-"
        return f"{self.replication_system.name}{sep}{self.name_without_domain}{sep}{self.replication_system.id}".replace(
            ".", "_"
        )

    @property
    def tns_string(self):
        """Return TNS string for this service"""
        connect_string = "(DESCRIPTION=(TRANSPORT_CONNECT_TIMEOUT=32)(CONNECT_TIMEOUT=60)(RETRY_COUNT=1)(ADDRESS_LIST=(FAILOVER=ON)"
        for service in self.service_set.all():
            connect_string += f"{service.tns_address}"
        connect_string += f"){self.tns_connect_data})"

        return connect_string

    @property
    def tns_connect_data(self):
        """Tns description for this service"""
        return f"(CONNECT_DATA=(SERVICE_NAME={self.name}))"


class Service(models.Model):
    """Service Model"""

    name = models.CharField("Name", max_length=512)
    hostname = models.CharField(
        "Hostname", max_length=255, null=True, blank=True, editable=False
    )
    port = models.PositiveSmallIntegerField(
        "Port", null=True, blank=True, editable=False
    )

    listeners = models.ManyToManyField(Listener)
    database = models.ForeignKey(Database, on_delete=models.CASCADE)

    failover_service = models.ForeignKey(
        FailoverService, on_delete=models.SET_NULL, null=True, blank=True
    )

    objects = ServiceManager()

    class Meta:
        db_table = "database_service"
        ordering = ["name"]
        constraints = [
            models.UniqueConstraint(
                fields=["name", "database"], name="databaseservice_set_unique"
            )
        ]

    def __str__(self):
        """Return string representation"""
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse("database:service-detail", kwargs={"pk": self.pk})

    @property
    def name_without_domain(self):
        """Return service name without db_domain"""
        return self.name.split(".")[0]

    @property
    def tns_name(self):
        """Unique tns name for this service"""
        sep = "-"
        return f"{self.database.name}{sep}{self.name_without_domain}{sep}{self.database.id}".replace(
            ".", "_"
        )

    @property
    def tns_string(self):
        """Return TNS string for this service"""
        return f"(DESCRIPTION={self.tns_address}{self.tns_connect_data})"

    @property
    def tns_address(self):
        """Tns description for this service"""
        return f"(ADDRESS=(PROTOCOL=TCP)(HOST={self.hostname})(PORT={self.port}))"

    @property
    def tns_connect_data(self):
        """Tns description for this service"""
        return f"(CONNECT_DATA=(SERVICE_NAME={self.name_without_domain}{self.db_domain}))"

    @property
    def db_domain(self):
        """Return database domain"""
        instance = self.database.instances.first()
        db_domain = InstanceParameterMetric.objects.filter(
            instance=instance.id, name="db_domain", snapshot=instance.snapshot_id
        )

        if len(db_domain) == 1:
            return f".{db_domain[0].value}"
        return ""


class NetworkAliasManager(models.Manager):
    """Service manager"""

    def search(self, query=None):
        """Search for service"""

        qs = self.get_queryset()
        if query is not None:
            or_lookup = models.Q(name__icontains=query)
            # distinct() is often necessary with Q lookups
            qs = qs.filter(or_lookup).order_by("name")
        return qs


class NetworkAlias(models.Model):
    """Base Network alias model"""

    name = models.CharField("Name", max_length=512)

    objects = NetworkAliasManager()

    class Meta:
        db_table = "database_alias_network"
        ordering = ["name"]
        constraints = [
            models.UniqueConstraint(
                fields=["name"], name="databasenetworkalias_set_unique"
            )
        ]


class StandaloneNetworkAlias(NetworkAlias):
    """Network alias Model"""

    service = models.ForeignKey(Service, on_delete=models.CASCADE)

    class Meta:
        db_table = "database_alias_network_standalone"

    def __str__(self):
        """Return string representation"""
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse(
            "database:standalone-network-alias-detail", kwargs={"pk": self.pk}
        )


class FailoverNetworkAlias(NetworkAlias):
    """Failover network alias Model"""

    service = models.ForeignKey(FailoverService, on_delete=models.CASCADE)

    class Meta:
        db_table = "database_alias_network_failover"

    def __str__(self):
        """Return string representation"""
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse("database:failover-network-alias-detail", kwargs={"pk": self.pk})
