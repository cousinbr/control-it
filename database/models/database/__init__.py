"""Database base models"""

from django.db import models
from django.urls import reverse

from core.models import Metric, Snapshot
from database.models import DatabaseType
from database.models.instance import Instance


class DatabaseReplicationSystem(models.Model):
    """Database repliacation system model"""

    name = models.CharField("Name", max_length=9)
    system_id = models.CharField("Unique replication system identifier", max_length=64)

    class Meta:
        db_table = "database_database_replication_system"
        constraints = [
            models.UniqueConstraint(
                fields=["name", "system_id"],
                name="databasereplicationsystem_set_unique",
            )
        ]


# pylint: disable=R0903
class DatabaseManager(models.Manager):
    """Database manager"""

    def search(self, query=None):
        """Search for database"""

        qs = self.get_queryset()
        if query is not None:
            or_lookup = models.Q(name__icontains=query)
            # distinct() is often necessary with Q lookups
            qs = qs.filter(or_lookup).order_by("name")
        return qs


class Database(models.Model):
    """Base database model"""

    name = models.CharField("Name of this database", max_length=128)
    type = models.CharField(max_length=3, choices=DatabaseType.TYPE)
    cluster_id = models.CharField("Unique cluster identifier", max_length=64)

    instances = models.ManyToManyField(Instance)
    snapshot = models.ForeignKey(Snapshot, on_delete=models.CASCADE)

    objects = DatabaseManager()

    class Meta:
        db_table = "database_database"
        ordering = ["name"]
        constraints = [
            models.UniqueConstraint(
                fields=["name", "cluster_id"], name="database_set_unique"
            )
        ]

    def __str__(self):
        """Return string representation"""
        return f"{self.name} ({self.get_type_display()})"

    def get_absolute_url(self):
        """Return object absolute url"""

        if self.type == DatabaseType.ORACLE:
            return self.oracledatabase.get_absolute_url()  # pylint: disable=no-member

        return reverse("database:database-detail", kwargs={"pk": self.pk})

    def get_sites(self):
        """Return the site of servers hosting this database"""

        sites = []
        for instance in self.instances.all().select_related("server"):
            if instance.server.site not in sites:
                sites.append(instance.server.site)

        return sites

    def get_host(self):
        """Return a tuple of host type and host name"""

        server = self.instances.first().server

        # Don't want the FQDN, so split on "." and return first element
        if server.cluster:
            return ("cluster", server.cluster.name.split(".")[0])

        return ("cluster", server.name.split(".")[0])


class DatabaseSizeMetric(Metric):
    """Database size metric"""

    database = models.ForeignKey(Database, on_delete=models.CASCADE)
    size = models.PositiveBigIntegerField("Total database size")
    datafile_size = models.PositiveBigIntegerField("Datafiles size")
    redo_size = models.PositiveBigIntegerField("Redo size")
    tempfile_size = models.PositiveBigIntegerField("Tempfiles size")

    class Meta(Metric.Meta):
        db_table = "database_database_size_metric"
