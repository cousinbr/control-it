"""Database type definition"""

class DatabaseType:
    """List of database type"""

    ORACLE = 'ORA'
    SQLSERVER = 'SQL'
    POSTGRES = 'PGS'

    TYPE = [
        (ORACLE, 'Oracle'),
        (SQLSERVER, 'SQLServer'),
        (POSTGRES, 'PostgreSQL'),
    ]
