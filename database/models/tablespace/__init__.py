"""Tablespace base models"""

from django.db import models
from django.urls import reverse

from core.models import Metric, Snapshot
from database.models import DatabaseType
from database.models.database import Database


# pylint: disable=R0903
class TablespaceManager(models.Manager):
    """Tablepsace manager"""

    def search(self, query=None):
        """Search for tablespace"""

        qs = self.get_queryset()
        if query is not None:
            or_lookup = (
                models.Q(name__icontains=query)
            )
            # distinct() is often necessary with Q lookups
            qs = qs.filter(or_lookup).order_by('name')
        return qs


class Tablespace(models.Model):
    """Tablespace model"""

    name = models.CharField('Name', max_length=30)
    type = models.CharField(max_length=3, choices=DatabaseType.TYPE)

    database = models.ForeignKey(Database, on_delete=models.CASCADE)
    snapshot = models.ForeignKey(Snapshot, on_delete=models.CASCADE)

    objects = TablespaceManager()

    class Meta:
        db_table = 'database_tablespace'
        ordering = ['name']
        constraints = [
            models.UniqueConstraint(
                fields=['name', 'database'], name='tablespace_set_unique')
        ]

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        """Return object absolute url"""

        if self.type == DatabaseType.ORACLE:
            return self.oracletablespace.get_absolute_url()  # pylint: disable=no-member


class TablespaceSpaceMetric(Metric):
    """Tablespace space metric"""

    tablespace = models.ForeignKey(Tablespace, on_delete=models.CASCADE)
    size = models.PositiveBigIntegerField('Size')
    used = models.PositiveBigIntegerField('Used')
    max_size = models.PositiveBigIntegerField('Max Size')

    class Meta(Metric.Meta):
        db_table = 'database_tablespace_space_metric'
