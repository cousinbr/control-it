"""Oracle Tablespace models"""

from django.db import models
from django.urls import reverse

from database.models import DatabaseType
from database.models.tablespace import Tablespace


class OracleTablespace(Tablespace):
    """Oracle tablespace model"""

    bigfile = models.CharField('Bigfile', max_length=3)
    block_size = models.PositiveSmallIntegerField('Block size')
    extent_management = models.CharField('Extent management', max_length=10)
    force_logging = models.CharField('Force logging', max_length=3)
    segment_space_management = models.CharField('Segment space management', max_length=6)
    contents = models.CharField('Contents', max_length=21)

    class Meta:
        db_table = 'database_tablespace_oracle'

    def save(self, *args, **kwargs):
        self.type = DatabaseType.ORACLE
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse('database:tablespace-oracle-detail', kwargs={'pk': self.pk})
