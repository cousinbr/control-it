"""Oracle User models"""

from django.db import models
from django.urls import reverse

from database.models import DatabaseType
from database.models.user import User


class OracleUser(User):
    """Oracle user model"""

    account_status = models.CharField('Account status', max_length=32)
    lock_date = models.DateTimeField('Lock date', blank=True, null=True)
    expiry_date = models.DateTimeField('Expiry date', blank=True, null=True)
    default_tablespace = models.CharField('Default tablespace', max_length=30)
    temporary_tablespace = models.CharField('Temporary tablespace', max_length=30)
    created = models.DateTimeField('Creation date')
    profile = models.CharField('Profile', max_length=128)
    password_versions = models.CharField('Password version', max_length=17)
    authentication_type = models.CharField('Authentication type', max_length=8)
    common = models.CharField('Common', max_length=3)
    last_login = models.DateTimeField('Last login', blank=True, null=True)
    password_change_date = models.DateTimeField('Password change date', blank=True, null=True)

    class Meta:
        db_table = 'database_user_oracle'

    def save(self, *args, **kwargs):
        self.type = DatabaseType.ORACLE
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        """Return object absolute url"""
        return reverse('database:user-oracle-detail', kwargs={'pk': self.pk})
