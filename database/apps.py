"""Database module apps"""
from django.apps import AppConfig


class DatabaseConfig(AppConfig):
    """Database app config"""

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'database'
