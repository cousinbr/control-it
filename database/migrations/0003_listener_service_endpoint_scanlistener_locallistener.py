# Generated by Django 4.1.3 on 2023-03-21 08:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0002_alter_filesystemsizemetric_table'),
        ('database', '0002_alter_containeroracleframetric_options_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Listener',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30, verbose_name='Name')),
                ('type', models.CharField(choices=[('LOCAL', 'Local Listener'), ('SCAN', 'Scan Listener')], max_length=5)),
                ('engine', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='database.engine')),
            ],
            options={
                'db_table': 'database_listener',
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=512, verbose_name='Name')),
                ('database', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='database.database')),
                ('listeners', models.ManyToManyField(to='database.listener')),
            ],
            options={
                'db_table': 'database_service',
            },
        ),
        migrations.CreateModel(
            name='Endpoint',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hostname', models.CharField(max_length=255, verbose_name='Hostname')),
                ('port', models.PositiveSmallIntegerField(verbose_name='Port')),
                ('listener', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='database.listener')),
            ],
            options={
                'db_table': 'database_endpoint',
            },
        ),
        migrations.CreateModel(
            name='ScanListener',
            fields=[
                ('listener_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='database.listener')),
                ('cluster', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='system.cluster')),
            ],
            options={
                'db_table': 'database_listener_scan',
            },
            bases=('database.listener',),
        ),
        migrations.CreateModel(
            name='LocalListener',
            fields=[
                ('listener_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='database.listener')),
                ('server', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='system.server')),
            ],
            options={
                'db_table': 'database_listener_local',
            },
            bases=('database.listener',),
        ),
    ]
