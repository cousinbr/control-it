# Generated by Django 4.2.2 on 2023-07-06 12:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('database', '0005_containeroracledatabase_part_of_dr'),
    ]

    operations = [
        migrations.CreateModel(
            name='FailoverService',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=512, verbose_name='Name')),
            ],
            options={
                'db_table': 'database_service_failover',
            },
        ),
        migrations.CreateModel(
            name='NetworkAlias',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=512, verbose_name='Name')),
            ],
            options={
                'db_table': 'database_alias_network',
            },
        ),
        migrations.AlterModelOptions(
            name='service',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='service',
            name='hostname',
            field=models.CharField(blank=True, editable=False, max_length=255, null=True, verbose_name='Hostname'),
        ),
        migrations.AddField(
            model_name='service',
            name='port',
            field=models.PositiveSmallIntegerField(blank=True, editable=False, null=True, verbose_name='Port'),
        ),
        migrations.AddConstraint(
            model_name='service',
            constraint=models.UniqueConstraint(fields=('name', 'database'), name='databaseservice_set_unique'),
        ),
        migrations.CreateModel(
            name='FailoverNetworkAlias',
            fields=[
                ('networkalias_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='database.networkalias')),
            ],
            options={
                'db_table': 'database_alias_network_failover',
            },
            bases=('database.networkalias',),
        ),
        migrations.CreateModel(
            name='StandaloneNetworkAlias',
            fields=[
                ('networkalias_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='database.networkalias')),
            ],
            options={
                'db_table': 'database_alias_network_standalone',
            },
            bases=('database.networkalias',),
        ),
        migrations.AddConstraint(
            model_name='networkalias',
            constraint=models.UniqueConstraint(fields=('name',), name='databasenetworkalias_set_unique'),
        ),
        migrations.AddField(
            model_name='failoverservice',
            name='replication_system',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='database.databasereplicationsystem'),
        ),
        migrations.AddField(
            model_name='service',
            name='failover_service',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='database.failoverservice'),
        ),
        migrations.AddField(
            model_name='standalonenetworkalias',
            name='service',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='database.service'),
        ),
        migrations.AddConstraint(
            model_name='failoverservice',
            constraint=models.UniqueConstraint(fields=('name', 'replication_system'), name='databaseservicefailover_set_unique'),
        ),
        migrations.AddField(
            model_name='failovernetworkalias',
            name='service',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='database.failoverservice'),
        ),
    ]
