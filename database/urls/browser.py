"""Database brownser urls"""

from django.urls import path

from database.views.browser import (AsmOracleInstanceDetailView,
                                    ContainerOracleDatabaseCDBDetailView,
                                    ContainerOracleDatabaseLEGDetailView,
                                    DatabaseDetailView, DatabaseExportView,
                                    DatabaseListView,
                                    DatabaseOracleInstanceDetailView,
                                    EngineDetailView, EngineListView, FailoverNetworkAliasCreateView, FailoverNetworkAliasDetailView, FailoverNetworkAliasListView, FailoverServiceDetailView,
                                    InstanceDetailView, InstanceExportView,
                                    InstanceListView, LocalListenerDetailView,
                                    StandaloneNetworkAliasCreateView, StandaloneNetworkAliasDetailView, StandaloneNetworkAliasListView,
                                    OracleDiskgroupDetailView,
                                    OracleTablespaceDetailView,
                                    OracleUserDetailView,
                                    PluggableOracleDatabaseDetailView,
                                    ScanListenerDetailView, ServiceDetailView,
                                    TablespaceDetailView, TablespaceExportView,
                                    TablespaceListView, UserDetailView,
                                    UserExportView, UserListView)

app_name = 'database'
urlpatterns = [
    # Instance
    path('instances/', InstanceListView.as_view(), name='instance-list'),
    path('instances/export/', InstanceExportView.as_view(), name='instance-export'),
    path('instance/<int:pk>/', InstanceDetailView.as_view(), name='instance-detail'),
    path('instance/oracle/rdbms/<int:pk>/', DatabaseOracleInstanceDetailView.as_view(), name='instance-oracle-database-detail'),
    path('instance/oracle/asm/<int:pk>/', AsmOracleInstanceDetailView.as_view(), name='instance-oracle-asm-detail'),
    # Database
    path('databases/', DatabaseListView.as_view(), name='database-list'),
    path('databases/export/', DatabaseExportView.as_view(), name='database-export'),
    path('database/<int:pk>/', DatabaseDetailView.as_view(), name='database-detail'),
    path('database/oracle/legacy/<int:pk>/', ContainerOracleDatabaseLEGDetailView.as_view(), name='database-oracle-leg-detail'),
    path('database/oracle/cdb/<int:pk>/', ContainerOracleDatabaseCDBDetailView.as_view(), name='database-oracle-cdb-detail'),
    path('database/oracle/pdb/<int:pk>/', PluggableOracleDatabaseDetailView.as_view(), name='database-oracle-pdb-detail'),
    # Tablespace
    path('tablespaces/', TablespaceListView.as_view(), name='tablespace-list'),
    path('tablespaces/export/', TablespaceExportView.as_view(), name='tablespace-export'),
    path('tablespace/<int:pk>/', TablespaceDetailView.as_view(), name='tablespace-detail'),
    path('tablespace/oracle/<int:pk>/', OracleTablespaceDetailView.as_view(), name='tablespace-oracle-detail'),
    # Users
    path('users/', UserListView.as_view(), name='user-list'),
    path('users/export/', UserExportView.as_view(), name='user-export'),
    path('user/<int:pk>/', UserDetailView.as_view(), name='user-detail'),
    path('user/oracle/<int:pk>/', OracleUserDetailView.as_view(), name='user-oracle-detail'),
    # Instance Binary
    path('instance-binarys/', EngineListView.as_view(), name='instancebinary-list'),
    path('instance-binary/<int:pk>/', EngineDetailView.as_view(), name='instancebinary-detail'),
    # Diskgroup
    path('diskgroup/oracle/<int:pk>/', OracleDiskgroupDetailView.as_view(), name='diskgroup-oracle-detail'),
    # Service
    path('service/<int:pk>/', ServiceDetailView.as_view(), name='service-detail'),
    # Failover Service
    path('failover-service/<int:pk>/', FailoverServiceDetailView.as_view(), name='failover-service-detail'),
    # Network Alias
    path('network-aliases/', StandaloneNetworkAliasListView.as_view(), name='standalone-network-alias-list'),
    path('network-alias/add/<int:service>/', StandaloneNetworkAliasCreateView.as_view(), name='standalone-network-alias-add'),
    path('network-alias/<int:pk>/', StandaloneNetworkAliasDetailView.as_view(), name='standalone-network-alias-detail'),
    # FailoverNetwork Alias
    path('failover-network-aliases/', FailoverNetworkAliasListView.as_view(), name='failover-network-alias-list'),
    path('failover-network-alias/add/<int:service>/', FailoverNetworkAliasCreateView.as_view(), name='failover-network-alias-add'),
    path('failover-network-alias/<int:pk>/', FailoverNetworkAliasDetailView.as_view(), name='failover-network-alias-detail'),
    # Listener
    path('listener/oracle/local/<int:pk>/', LocalListenerDetailView.as_view(), name='listener-local-detail'),
    path('listener/oracle/scan/<int:pk>/', ScanListenerDetailView.as_view(), name='listener-scan-detail'),
]
