"""Oject helper for tests"""

from core.tests import ModelBuilder as CoreModelBuilder
from database.models import DatabaseType
from database.models.database import DatabaseReplicationSystem, DatabaseSizeMetric
from database.models.database.oracle import ContainerOracleDatabase, PluggableOracleDatabase
from database.models.diskgroup.oracle import OracleDiskgroup
from database.models.engine import Engine
from database.models.instance.oracle import AsmOracleInstance, DatabaseOracleInstance
from database.models.tablespace import TablespaceSpaceMetric
from database.models.tablespace.oracle import OracleTablespace
from database.models.tns import Endpoint, FailoverNetworkAlias, FailoverService, LocalListener, StandaloneNetworkAlias, ScanListener, Service
from database.models.user.oracle import OracleUser
from system.tests import ModelBuilder as SystemModelBuilder


class ModelBuilder:
    """Helper class used in tests to build model objects"""

    # Engine
    engine_path = "/u01/oracle"
    engine_version = "19.18.0.0"

    # AsmOracleInstance
    asm_oracle_instance_name = "+ASM"

    # OracleDiskgroup
    oracle_diskgroup_name = "DG01"
    oracle_diskgroup_cluster_id = "5567a4f221b68748a77ebd24c567ea4b51674962e47941bd5a557acf02029681"
    oracle_diskgroup_type = "EXTERN"
    oracle_diskgroup_compatibility = "19.0.0.0.0"
    oracle_diskgroup_database_compatibility = "19.0.0.0.0"
    oracle_diskgroup_voting_files = "N"

    # DatabaseOracleInstance
    database_oracle_instance_name = "ORCL"

    # DatabaseReplicationSystem
    replication_system_name = "ORCLDB"
    replication_system_system_id = "5567a4f221b68748a77ebd24c567ea4b51674962e47941bd5a557acf02029681"

    # ContainerOracleDatabase CDB
    container_oracle_database_cdb_name = "ORCLCDB"
    container_oracle_database_cdb_cluster_id = "5567a4f221b68748a77ebd24c567ea4b51674962e47941bd5a557acf02029681"
    container_oracle_database_cdb_global_name = "ORCL"
    container_oracle_database_cdb_dbid = 666
    container_oracle_database_cdb_is_cluster_database = True
    container_oracle_database_cdb_created = "2011-09-01T13:20:30+03:00"
    container_oracle_database_cdb_resetlogs_time = "2011-09-01T13:20:30+03:00"
    container_oracle_database_cdb_log_mode = "ARCHIVELOG"
    container_oracle_database_cdb_database_role = "PRIMARY"
    container_oracle_database_cdb_force_logging = "YES"
    container_oracle_database_cdb_flashback_on = "NO"
    container_oracle_database_cdb_size = 666
    container_oracle_database_cdb_container_type = ContainerOracleDatabase.CDB

    # ContainerOracleDatabase LEGACY
    container_oracle_database_legacy_name = "ORCLLEG"
    container_oracle_database_legacy_cluster_id = "5567a4f221b68748a77ebd24c567ea4b51674962e47941bd5a557acf02029681"
    container_oracle_database_legacy_global_name = "ORCL"
    container_oracle_database_legacy_dbid = 666
    container_oracle_database_legacy_is_cluster_database = True
    container_oracle_database_legacy_created = "2011-09-01T13:20:30+03:00"
    container_oracle_database_legacy_resetlogs_time = "2011-09-01T13:20:30+03:00"
    container_oracle_database_legacy_log_mode = "ARCHIVELOG"
    container_oracle_database_legacy_database_role = "PRIMARY"
    container_oracle_database_legacy_force_logging = "YES"
    container_oracle_database_legacy_flashback_on = "NO"
    container_oracle_database_legacy_size = 666
    container_oracle_database_legacy_container_type = ContainerOracleDatabase.LEGACY

    # PluggableOracleDatabase
    pluggable_oracle_database_name = "ORCLPDB"
    pluggable_oracle_database_cluster_id = "5567a4f221b68748a77ebd24c567ea4b51674962e47941bd5a557acf02029681"
    pluggable_oracle_database_dbid = 666
    pluggable_oracle_database_created = "2011-09-01T13:20:30+03:00"
    pluggable_oracle_database_size = 666
    pluggable_oracle_database_guid = "E4A25D48BD6BA0D8E0530418460A3537"

    # DatabaseSizeMetric
    database_size_metric_size = 666
    database_datafile_size_metric_size = 666
    database_redo_size_metric_size = 666
    database_tempfile_size_metric_size = 666

    # OracleTablespace
    oracle_tablespace_name = "TBS1"
    oracle_tablespace_bigfile = "YES"
    oracle_tablespace_block_size = 8192
    oracle_tablespace_extent_management = "LOCAL"
    oracle_tablespace_force_logging = "NO"
    oracle_tablespace_segment_space_management = "AUTO"
    oracle_tablespace_contents = "PERMANENT"

    # TablespaceSpaceMetric
    tablespace_space_metric_size = 666
    tablespace_space_metric_used = 666
    tablespace_space_metric_max_size = 666

    # OracleUser
    oracle_user_name = "SCOTT"
    oracle_user_account_status = "OPEN"
    oracle_user_default_tablespace = "USERS"
    oracle_user_temporary_tablespace = "TEMP"
    oracle_user_created = "2011-09-01T13:20:30+03:00"
    oracle_user_profile = "DEFAULT"
    oracle_user_password_versions = ""
    oracle_user_authentication_type = "PASSWORD"
    oracle_user_common = "NO"

    # LocalOracleListener
    local_oracle_listener_name = "LISTENER"
    local_oracle_listener_oracle_home = "/u01/oracle"

    # ScanOracleListener
    scan_oracle_listener_name = "SCAN_LISTENER"
    scan_oracle_listener_oracle_home = "/u01/oracle"

    # Endpoint
    endpoint_hostname = "endpoint.fqdn"
    endpoint_port = 1521

    # Service
    service_name = "db.service"

    # Failover Service
    failover_service_name = "failoverdb.service"

    # NetworkAlias
    network_alias_name = "ALIAS"

    # FailoverNetworkAlias
    failover_network_alias_name = "ALIAS_FAILOVER"

    def __init__(self):

        self.core_model_builder = CoreModelBuilder()
        self.system_model_builder = SystemModelBuilder()

        # Engine
        self.engine_obj = Engine.objects.create(
            path = self.engine_path,
            server = self.system_model_builder.server_obj,
            version = self.engine_version
        )

        # AsmOracleInstance
        self.asm_oracle_instance_obj = AsmOracleInstance.objects.create(
            name = self.asm_oracle_instance_name,
            server = self.system_model_builder.server_obj,
            engine = self.engine_obj,
            vendor = self.core_model_builder.vendor_obj,
            snapshot = self.core_model_builder.snapshot_obj
        )

        # OracleDiskgroup
        self.oracle_diskgroup_obj = OracleDiskgroup.objects.create(
            name = self.oracle_diskgroup_name,
            cluster_id = self.oracle_diskgroup_cluster_id,
            type = self.oracle_diskgroup_type,
            compatibility = self.oracle_diskgroup_compatibility,
            database_compatibility = self.oracle_diskgroup_database_compatibility,
            voting_files = self.oracle_diskgroup_voting_files
        )
        self.oracle_diskgroup_obj.instances.add(self.asm_oracle_instance_obj)

        # DatabaseOracleInstance
        self.database_oracle_instance_obj = DatabaseOracleInstance.objects.create(
            name = self.database_oracle_instance_name,
            server = self.system_model_builder.server_obj,
            engine = self.engine_obj,
            vendor = self.core_model_builder.vendor_obj,
            snapshot = self.core_model_builder.snapshot_obj
        )

        # DatabaseReplicationSystem
        self.replication_system_obj = DatabaseReplicationSystem.objects.create(
            name = self.replication_system_name,
            system_id = self.replication_system_system_id,
        )

        # ContainerOracleDatabase CDB
        self.container_oracle_database_cdb_obj = ContainerOracleDatabase.objects.create(
            name = self.container_oracle_database_cdb_name,
            type = DatabaseType.ORACLE,
            cluster_id = self.container_oracle_database_cdb_cluster_id,
            global_name = self.container_oracle_database_cdb_global_name,
            dbid = self.container_oracle_database_cdb_dbid,
            is_cluster_database = self.container_oracle_database_cdb_is_cluster_database,
            created = self.container_oracle_database_cdb_created,
            resetlogs_time = self.container_oracle_database_cdb_resetlogs_time,
            log_mode = self.container_oracle_database_cdb_log_mode,
            database_role = self.container_oracle_database_cdb_database_role,
            force_logging = self.container_oracle_database_cdb_force_logging,
            flashback_on = self.container_oracle_database_cdb_flashback_on,
            replication_system = self.replication_system_obj,
            size = self.container_oracle_database_cdb_size,
            snapshot = self.core_model_builder.snapshot_obj,
            container_type = self.container_oracle_database_cdb_container_type,
        )
        self.container_oracle_database_cdb_obj.instances.add(self.database_oracle_instance_obj)

        # ContainerOracleDatabase CDB
        self.container_oracle_database_legacy_obj = ContainerOracleDatabase.objects.create(
            name = self.container_oracle_database_legacy_name,
            type = DatabaseType.ORACLE,
            cluster_id = self.container_oracle_database_legacy_cluster_id,
            global_name = self.container_oracle_database_legacy_global_name,
            dbid = self.container_oracle_database_legacy_dbid,
            is_cluster_database = self.container_oracle_database_legacy_is_cluster_database,
            created = self.container_oracle_database_legacy_created,
            resetlogs_time = self.container_oracle_database_legacy_resetlogs_time,
            log_mode = self.container_oracle_database_legacy_log_mode,
            database_role = self.container_oracle_database_legacy_database_role,
            force_logging = self.container_oracle_database_legacy_force_logging,
            flashback_on = self.container_oracle_database_legacy_flashback_on,
            replication_system = self.replication_system_obj,
            size = self.container_oracle_database_legacy_size,
            snapshot = self.core_model_builder.snapshot_obj,
            container_type = self.container_oracle_database_legacy_container_type,
        )
        self.container_oracle_database_legacy_obj.instances.add(self.database_oracle_instance_obj)

        # PluggableOracleDatabase
        self.pluggable_oracle_database_obj = PluggableOracleDatabase.objects.create(
            name = self.pluggable_oracle_database_name,
            type = DatabaseType.ORACLE,
            cluster_id = self.pluggable_oracle_database_cluster_id,
            dbid = self.pluggable_oracle_database_dbid,
            created = self.pluggable_oracle_database_created,
            size = self.pluggable_oracle_database_size,
            guid = self.pluggable_oracle_database_guid,
            container = self.container_oracle_database_cdb_obj,
            snapshot = self.core_model_builder.snapshot_obj,
        )
        self.pluggable_oracle_database_obj.instances.add(self.database_oracle_instance_obj)

        # DatabaseSizeMetric
        DatabaseSizeMetric.objects.create(
            size = self.database_size_metric_size,
            datafile_size = self.database_datafile_size_metric_size,
            redo_size = self.database_redo_size_metric_size,
            tempfile_size = self.database_tempfile_size_metric_size,
            database = self.container_oracle_database_cdb_obj,
            snapshot = self.core_model_builder.snapshot_obj,
            day = self.core_model_builder.snapshot_obj.day,
            date = "2011-09-01T13:20:30+03:00"
        )
        DatabaseSizeMetric.objects.create(
            size = self.database_size_metric_size,
            datafile_size = self.database_datafile_size_metric_size,
            redo_size = self.database_redo_size_metric_size,
            tempfile_size = self.database_tempfile_size_metric_size,
            database = self.container_oracle_database_legacy_obj,
            snapshot = self.core_model_builder.snapshot_obj,
            day = self.core_model_builder.snapshot_obj.day,
            date = "2011-08-01T13:20:30+03:00"
        )
        DatabaseSizeMetric.objects.create(
            size = self.database_size_metric_size,
            datafile_size = self.database_datafile_size_metric_size,
            redo_size = self.database_redo_size_metric_size,
            tempfile_size = self.database_tempfile_size_metric_size,
            database = self.pluggable_oracle_database_obj,
            snapshot = self.core_model_builder.snapshot_obj,
            day = self.core_model_builder.snapshot_obj.day,
            date = "2011-10-01T13:20:30+03:00"
        )

        # OracleTablespace
        self.oracle_tablespace_obj = OracleTablespace.objects.create(
            name = self.oracle_tablespace_name,
            database = self.container_oracle_database_legacy_obj,
            bigfile = self.oracle_tablespace_bigfile,
            block_size = self.oracle_tablespace_block_size,
            extent_management = self.oracle_tablespace_extent_management,
            force_logging = self.oracle_tablespace_force_logging,
            segment_space_management = self.oracle_tablespace_segment_space_management,
            contents = self.oracle_tablespace_contents,
            snapshot = self.core_model_builder.snapshot_obj,
        )

        # TablespaceSpaceMetric
        TablespaceSpaceMetric.objects.create(
            size = self.tablespace_space_metric_size,
            used = self.tablespace_space_metric_used,
            max_size = self.tablespace_space_metric_max_size,
            tablespace = self.oracle_tablespace_obj,
            snapshot = self.core_model_builder.snapshot_obj,
            day = self.core_model_builder.snapshot_obj.day,
            date = "2011-09-01T13:20:30+03:00"
        )
        TablespaceSpaceMetric.objects.create(
            size = self.tablespace_space_metric_size,
            used = self.tablespace_space_metric_used,
            max_size = self.tablespace_space_metric_max_size,
            tablespace = self.oracle_tablespace_obj,
            snapshot = self.core_model_builder.snapshot_obj,
            day = self.core_model_builder.snapshot_obj.day,
            date = "2011-08-01T13:20:30+03:00"
        )
        TablespaceSpaceMetric.objects.create(
            size = self.tablespace_space_metric_size,
            used = self.tablespace_space_metric_used,
            max_size = self.tablespace_space_metric_max_size,
            tablespace = self.oracle_tablespace_obj,
            snapshot = self.core_model_builder.snapshot_obj,
            day = self.core_model_builder.snapshot_obj.day,
            date = "2011-10-01T13:20:30+03:00"
        )

        # OracleUser
        self.oracle_user_obj = OracleUser.objects.create(
            name = self.oracle_user_name,
            database = self.container_oracle_database_legacy_obj,
            account_status = self.oracle_user_account_status,
            default_tablespace = self.oracle_user_default_tablespace,
            temporary_tablespace = self.oracle_user_temporary_tablespace,
            created = self.oracle_user_created,
            profile = self.oracle_user_profile,
            password_versions = self.oracle_user_password_versions,
            authentication_type = self.oracle_user_authentication_type,
            common = self.oracle_user_common,
            snapshot = self.core_model_builder.snapshot_obj,
        )

        # LocalOracleListener
        self.local_oracle_listener_obj = LocalListener.objects.create(
            name = self.local_oracle_listener_name,
            engine = self.engine_obj,
            server = self.system_model_builder.server_obj,
        )

        # ScanOracleListener
        self.scan_oracle_listener_obj = ScanListener.objects.create(
            name = self.scan_oracle_listener_name,
            engine = self.engine_obj,
            cluster = self.system_model_builder.cluster_obj,
        )

        # Endpoint
        self.endpoint_obj = Endpoint.objects.create(
            hostname = self.endpoint_hostname,
            port = self.endpoint_port,
            listener = self.local_oracle_listener_obj,
        )

        # Service
        self.service_obj = Service.objects.create(
            name = self.service_name,
            database = self.container_oracle_database_legacy_obj
        )
        self.service_obj.listeners.add(self.local_oracle_listener_obj)
        self.service_obj.listeners.add(self.scan_oracle_listener_obj)

        # FailoverService
        self.failover_service_obj = FailoverService.objects.create(
            name = self.failover_service_name,
            replication_system = self.replication_system_obj
        )

        # NetworkAlias
        self.network_alias_obj = StandaloneNetworkAlias.objects.create(
            name = self.network_alias_name,
            service = self.service_obj
        )

        # FailoverNetworkAlias
        self.failover_network_alias_obj = FailoverNetworkAlias.objects.create(
            name = self.failover_network_alias_name,
            service = self.failover_service_obj
        )
