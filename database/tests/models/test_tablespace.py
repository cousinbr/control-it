"""Database module models Test"""

from datetime import datetime
from django.db.models import Q
from django.test import TestCase
from django.utils.timezone import make_aware

from database.models.tablespace import Tablespace, TablespaceSpaceMetric
from database.models.tablespace.oracle import OracleTablespace
from database.tests.models import ModelBuilder


class TablespaceManagerTestCase(TestCase):
    """TablespaceManager model test"""

    def test_search(self):
        """TablespaceManager search"""
        qs = Tablespace.objects.get_queryset()
        or_lookup = Q(name__icontains="ORCL")
        qs = qs.filter(or_lookup).distinct()
        self.assertQuerysetEqual(Tablespace.objects.search("ORCL"), qs)

        qs = Tablespace.objects.get_queryset()
        self.assertQuerysetEqual(Tablespace.objects.search(), qs)


class TablespaceTestCase(TestCase):
    """Tablespace model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str(self):
        """Tablespace string representation"""
        obj = OracleTablespace.objects.get(name=self.model_builder.oracle_tablespace_name)
        self.assertEqual(str(obj), f"{self.model_builder.oracle_tablespace_name}")

    def test_get_absolute_url(self):
        """Tablespace get_absolute_url"""
        obj = Tablespace.objects.get(name=self.model_builder.oracle_tablespace_name)
        self.assertEqual(obj.get_absolute_url(), f"/database/tablespace/oracle/{obj.id}/")

    def test_metric_asc_order(self):
        """Test that metric is order on date ASC"""
        date = make_aware(datetime(1900, 1, 1, 0, 0))
        for metric in TablespaceSpaceMetric.objects.all():
            self.assertGreater(metric.date, date)
            date = metric.date
