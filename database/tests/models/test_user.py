"""Database module models Test"""

from django.db.models import Q
from django.test import TestCase

from database.models.user import User
from database.models.user.oracle import OracleUser
from database.tests.models import ModelBuilder


class UserManagerTestCase(TestCase):
    """UserManager model test"""

    def test_search(self):
        """UserManager search"""
        qs = User.objects.get_queryset()
        or_lookup = Q(name__icontains="SCOTT")
        qs = qs.filter(or_lookup).distinct()
        self.assertQuerysetEqual(User.objects.search("SCOTT"), qs)

        qs = User.objects.get_queryset()
        self.assertQuerysetEqual(User.objects.search(), qs)


class UserTestCase(TestCase):
    """User model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str(self):
        """User string representation"""
        obj = OracleUser.objects.get(name=self.model_builder.oracle_user_name)
        self.assertEqual(str(obj), f"{self.model_builder.oracle_user_name}")

    def test_get_absolute_url(self):
        """User get_absolute_url"""
        obj = User.objects.get(name=self.model_builder.oracle_user_name)
        self.assertEqual(obj.get_absolute_url(), f"/database/user/oracle/{obj.id}/")
