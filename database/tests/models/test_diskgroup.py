"""Diskgroup module models Test"""

from django.test import TestCase
from database.models.diskgroup.oracle import OracleDiskgroup

from database.tests.models import ModelBuilder


class OracleDiskgroupTestCase(TestCase):
    """Database model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str_legacy(self):
        """Database string representation"""
        obj = OracleDiskgroup.objects.get(name=self.model_builder.oracle_diskgroup_name)
        self.assertEqual(str(obj), f"{self.model_builder.oracle_diskgroup_name}")

    def test_get_absolute_url_legacy(self):
        """Database get_absolute_url"""
        obj = OracleDiskgroup.objects.get(name=self.model_builder.oracle_diskgroup_name)
        self.assertEqual(obj.get_absolute_url(), f"/database/diskgroup/oracle/{obj.id}/")