"""Database module models Test"""

from datetime import datetime
from django.db.models import Q
from django.test import TestCase
from django.utils.timezone import make_aware

from database.models.database import Database, DatabaseSizeMetric
from database.models.database.oracle import ContainerOracleDatabase, PluggableOracleDatabase
from database.tests.models import ModelBuilder


class DatabaseManagerTestCase(TestCase):
    """DatabaseManager model test"""

    def test_search(self):
        """DatabaseManager search"""
        qs = Database.objects.get_queryset()
        or_lookup = Q(name__icontains="ORCL")
        qs = qs.filter(or_lookup).distinct()
        self.assertQuerysetEqual(Database.objects.search("ORCL"), qs)

        qs = Database.objects.get_queryset()
        self.assertQuerysetEqual(Database.objects.search(), qs)


class DatabaseTestCase(TestCase):
    """Database model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str_legacy(self):
        """Database string representation"""
        obj = ContainerOracleDatabase.objects.get(name=self.model_builder.container_oracle_database_legacy_name)
        self.assertEqual(str(obj), f"{self.model_builder.container_oracle_database_legacy_name} (Legacy Database)")

    def test_str_container(self):
        """Database string representation"""
        obj = ContainerOracleDatabase.objects.get(name=self.model_builder.container_oracle_database_cdb_name)
        self.assertEqual(str(obj), f"{self.model_builder.container_oracle_database_cdb_name} (Multitenant Container Database)")

    def test_str_pdb(self):
        """Database string representation"""
        obj = PluggableOracleDatabase.objects.get(name=self.model_builder.pluggable_oracle_database_name)
        self.assertEqual(str(obj), f"{self.model_builder.pluggable_oracle_database_name} (Pluggable Database)")

    def test_get_absolute_url_legacy(self):
        """Database get_absolute_url"""
        obj = Database.objects.get(name=self.model_builder.container_oracle_database_legacy_name)
        self.assertEqual(obj.get_absolute_url(), f"/database/database/oracle/legacy/{obj.id}/")

    def test_get_absolute_url_container(self):
        """Database get_absolute_url"""
        obj = Database.objects.get(name=self.model_builder.container_oracle_database_cdb_name)
        self.assertEqual(obj.get_absolute_url(), f"/database/database/oracle/cdb/{obj.id}/")

    def test_get_absolute_url_pdb(self):
        """Database get_absolute_url"""
        obj = Database.objects.get(name=self.model_builder.pluggable_oracle_database_name)
        self.assertEqual(obj.get_absolute_url(), f"/database/database/oracle/pdb/{obj.id}/")

    def test_metric_asc_order(self):
        """Test that metric is order on date ASC"""
        date = make_aware(datetime(1900, 1, 1, 0, 0))
        for metric in DatabaseSizeMetric.objects.all():
            self.assertGreater(metric.date, date)
            date = metric.date
