"""Database module models Test"""

from django.test import TestCase

from core.models import Environment, Snapshot
from database.models.engine import Engine
from database.tests.models import ModelBuilder
from system.models import Server


class EngineTestCase(TestCase):
    """Engine model test"""

    def setUp(self):
        self.model_builder = ModelBuilder()

    def test_str(self):
        """Engine string representation"""
        obj = Engine.objects.get(path=self.model_builder.engine_path)
        self.assertEqual(str(obj), f"{self.model_builder.engine_path}")

    def test_get_absolute_url(self):
        """Engine get_absolute_url"""
        obj = Engine.objects.get(path=self.model_builder.engine_path)
        self.assertEqual(obj.get_absolute_url(), f"/database/instance-binary/{obj.id}/")
